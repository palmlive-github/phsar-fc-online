<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('express_deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('from');
            $table->char('to');
            $table->text('name')->nullable();
            $table->bigInteger('sender_id')->nullable();
            $table->text('sender_info')->nullable();
            $table->bigInteger('receiver_id')->nullable();
            $table->text('receiver_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('express_deliveries');
    }
}
