<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVIPSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_i_p_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('fee');
            $table->decimal('revenue_from_register');
            $table->decimal('can_withdrawal');
            $table->longText('payments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_i_p_settings');
    }
}
