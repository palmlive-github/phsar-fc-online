<?php

namespace App\CPU;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageManager
{
    public static function upload(string $dir, string $format, $image = null)
    {
        if ($image != null) {
            $imageName = Carbon::now()->toDateString() . "-" . uniqid() . "." . $format;
            if (!Storage::disk('public')->exists($dir)) {
                Storage::disk('public')->makeDirectory($dir);
            }
            Storage::disk('public')->put($dir . $imageName, file_get_contents($image));
        } else {
            $imageName = 'def.png';
        }

        return $imageName;
    }

    public static function update(string $dir, $old_image, string $format, $image = null)
    {
        if (Storage::disk('public')->exists($dir . $old_image)) {
            Storage::disk('public')->delete($dir . $old_image);
        }
        $imageName = ImageManager::upload($dir, $format, $image);
        return $imageName;
    }

    public static function move($from_full_path,$to_path, $file)
    {
       self::deleteold();
       $file = collect(explode('/',$file))->last();
        if (Storage::disk('public')->exists($from_full_path) ){

                try {
                    Storage::disk('public')->copy($from_full_path,$to_path.$file);
                    return $file;
                } catch (\Throwable $th) {
                    return $file;
                }
                // if(Storage::disk('public')->move($from_full_path,$to_path.$file)){
                //     return $file;
                // }
                //Storage::disk('public')->delete($from_full_path);
        }
        return null;
    }

    public static function temp(object $image)
    {
        if($image){
           try {
            $file = ImageManager::upload('temp/','png',$image);
            return [
                'status' => true,
                'file' => $file,
                'url' => asset('storage/temp/'.$file),
            ];
           } catch (\Throwable $th) {
            return [
                'status' => false,
                'message' => translate('Image size must be less than 1MB'),
                'url' => asset('assets/back-end/img/400x400/img2.jpg'),
            ];
           }
        }
        return [
            'status' => false,
            'url' => asset('assets/back-end/img/400x400/img2.jpg'),
        ];
    }
    public static function deleteold()
    {
        $files = collect();
        foreach (Storage::disk('public')->allFiles('temp') as  $file) {
            $filename = public_path('storage/'.$file);

            if (file_exists($filename)) {
                $data = File::lastModified($filename);

                if (now() > Carbon::parse($data)->addDays(2)) {
                    unlink($filename);
                }else{
                    $files->add([
                        'date' => Carbon::parse($data)->format('Y-m-d'),
                        'file' => $filename,
                        'can_delete' => now() > Carbon::parse($data)->addDays(2)
                    ]);
                }
            }
        }
        return $files;
    }
    public static function delete($full_path)
    {
        if (Storage::disk('public')->exists($full_path)) {
            Storage::disk('public')->delete($full_path);
        }

        return [
            'success' => 1,
            'message' => translate('Removed successfully !')
        ];

    }
}
