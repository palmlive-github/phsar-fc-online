<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use App\Model\MarketingLevel;
use App\Model\Translation;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;

use function App\CPU\translate;

class MarketingLevelController extends Controller
{
    public function index(Request $request)
    {
        $marketingLevel = new MarketingLevel;
        $query_param = [];
        $search = $request['search'];
        if($request->has('search'))
        {
            $key = explode(' ', $request['search']);
            $marketingLevel = MarketingLevel::where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('name', 'like', "%{$value}%");
                }
            });
            $query_param = ['search' => $request['search']];
        }

        $marketingLevel = $marketingLevel->orderBy('name','asc')->paginate(Helpers::pagination_limit())->appends($query_param);
        return view('admin-views.marketing-level.index', compact('marketingLevel','search'));
    }

    public function create(Request $request)
    {
        $maxCommission  = (20 - MarketingLevel::sum('commission'))?? 20;
        if($maxCommission){
            return view('admin-views.marketing-level.create',compact('maxCommission'));
        }else{
            return $this->index($request);
        }

    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'commission' => 'required'
        ], [
            'name.required' => 'Name is required!',
            'commission.required' => 'Commission is required!',
        ]);

        $marketingLevel = new MarketingLevel;
        $marketingLevel->name = $request->name[array_search('en', $request->lang)];
        $marketingLevel->commission = $request->commission;
        $marketingLevel->save();

        $data = [];
        foreach ($request->lang as $index => $key) {
            if ($request->name[$index] && $key != 'en') {
                array_push($data, array(
                    'translationable_type' => 'App\Model\MarketingLevel',
                    'translationable_id' => $marketingLevel->id,
                    'locale' => $key,
                    'key' => 'name',
                    'value' => $request->name[$index],
                ));
            }
        }
        if (count($data)) {
            Translation::insert($data);
        }

        Toastr::success(translate('Marketing Level added successfully!'));
        return redirect()->route('admin.marketing-level.index');
    }

    public function edit(Request $request, $id)
    {
        $marketingLevel = MarketingLevel::withoutGlobalScopes()->find($id);
        $maxCommission  = (20 - MarketingLevel::sum('commission'))?? 20;
        if(($maxCommission + $marketingLevel->commission) == 20){
            $maxCommission = 20;
        }else{
            $maxCommission += $marketingLevel->commission;
        }
        return view('admin-views.marketing-level.edit', compact('marketingLevel','maxCommission'));
    }

    public function update(Request $request,$id)
    {

        $marketingLevel = MarketingLevel::find($id);
        $marketingLevel->name = $request->name[array_search('en', $request->lang)];
        $marketingLevel->commission = $request->commission;
        $marketingLevel->save();

        foreach ($request->lang as $index => $key) {
            if ($request->name[$index] && $key != 'en') {
                Translation::updateOrInsert(
                    ['translationable_type' => 'App\Model\MarketingLevel',
                        'translationable_id' => $marketingLevel->id,
                        'locale' => $key,
                        'key' => 'name'],
                    ['value' => $request->name[$index]]
                );
            }
        }

        Toastr::success(translate('Marketing Level updated successfully!'));
        return redirect()->route('admin.marketing-level.index');
    }

    public function destroy($id)
    {

        $translation = Translation::where('translationable_type','App\Model\MarketingLevel')
                                    ->where('translationable_id',$id);
        $translation->delete();
        MarketingLevel::destroy($id);

        return response()->json();
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = MarketingLevel::where('position', 0)->orderBy('id', 'desc')->get();
            return response()->json($data);
        }
    }


}
