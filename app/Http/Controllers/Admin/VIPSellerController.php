<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\CPU\ImageManager;
use App\Http\Controllers\Controller;
use App\Model\Seller;
use App\Model\Shop;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function App\CPU\translate;

class VIPSellerController extends Controller
{
    public function index(Request $request)
    {
        $query_param = [];
        $search = $request['search'];
        if ($request->has('search')) {
            $key = explode(' ', $request['search']);
            $sellers = Seller::with(['orders', 'product'])
                ->where(function ($q) use ($key) {
                    foreach ($key as $value) {
                        $q->orWhere('f_name', 'like', "%{$value}%")
                            ->orWhere('l_name', 'like', "%{$value}%")
                            ->orWhere('phone', 'like', "%{$value}%")
                            ->orWhere('email', 'like', "%{$value}%");
                    }
                });
            $query_param = ['search' => $request['search']];
        } else {
            $sellers = Seller::with(['orders', 'product']);
        }
        $sellers = $sellers->latest()->where('vip', true)->paginate(Helpers::pagination_limit())->appends($query_param);
        return view('admin-views.seller.index', compact('sellers', 'search'));
    }


    public function create()
    {
        $seller = new Seller();
        return view('admin-views.seller.create', compact('seller'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:sellers',
            'password' => 'required|min:8',
            'bank_name' => 'required',
            'holder_name' => 'required',
            'account_no' => 'required',
        ]);
        $full_number = $request['phone']['full'];
        $user = Seller::query()->where('email', $request->email)->orWhere('phone', $full_number)->first();
        if ($user) {
            Toastr::success(translate('Seller is already exist'));
            return redirect()->route('seller.auth.login');
        }
        DB::transaction(function ($r) use ($request, &$user) {
            $seller = new Seller();
            $seller->f_name = $request->f_name;
            $seller->l_name = $request->l_name;
            $seller->phone = $request['phone']['full'];
            $seller->email = $request->email;
            $seller->phone_verify_at = null;
            $seller->image = ImageManager::upload('seller/', 'png', $request->file('image'));
            $seller->passport_image = ImageManager::upload('seller/', 'png', $request->file('passport_image'));
            $seller->password = bcrypt($request->password);
            $seller->status = "approved";  // pending , suspended
            $seller->bank_name = $request->bank_name;
            $seller->holder_name = $request->holder_name;
            $seller->account_no = $request->account_no;
            $seller->phone_verify_at = now();
            $seller->vip = isset($request->vip);
            $seller->save();

            $shop = new Shop();
            $shop->seller_id = $seller->id;
            $shop->name = $request->shop_name;
            $shop->address = $request->shop_address;
            $shop->sales_commission_percentage = $request->sales_commission_percentage;
            $shop->contact = $request['phone']['full'];
            $shop->logo = ImageManager::upload('shop/', 'png', $request->file('logo'));
            $shop->banner = ImageManager::upload('shop/banner/', 'png', $request->file('banner'));
            $shop->save();

            DB::table('seller_wallets')->insert([
                'seller_id' => $seller['id'],
                'withdrawn' => 0,
                'commission_given' => 0,
                'total_earning' => 0,
                'pending_withdraw' => 0,
                'delivery_charge_earned' => 0,
                'collected_cash' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $user = $seller;
        });

        Toastr::success(translate('seller created successfully!'));
        return redirect()->route('admin.sellers.seller-list.vip');
    }

    public function edit($id)
    {
        $seller = Seller::findOrFail($id);
        return view('admin-views.seller.edit', compact('seller'));
    }

    public function update(Request $request, $id)
    {
        $seller = Seller::findOrFail($id);
        if ($request->phone) {
            $seller->update(['phone' => $request['phone']['full']]);
        }

        $seller->update([
            'f_name' => $request->f_name,
            'l_name' => $request->lname,
        ]);

        if ($request->password) {
            $seller->update(['password' => bcrypt($request->password)]);
        }

        dd($seller->shop->image);
//        if ($request->file('image')) {
//            $seller->update(['image' => ImageManager::update($seller->image, $request->file('image'))]);
//        }
//        if ($request->file('passport_image')) {
//            $seller->update(['passport_image' => ImageManager::update($seller->passport_image, $request->file('passport_image'))]);
//        }
//
//        if ($request->file('logo')) {
//            $seller->shop->update(['logo' => ImageManager::update($seller->shop->logo, $request->file('logo'))]);
//        }
//        if ($request->file('banner')) {
//            $seller->shop->update(['banner' => ImageManager::update($seller->shop->banner, $request->file('banner'))]);
//        }

        Toastr::success(translate('seller updated successfully!'));
        return redirect()->route('admin.sellers.seller-list.vip');
    }
}
