<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use App\Model\Color;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;

use function App\CPU\translate;

class ColorController extends Controller
{
    public function index(Request $request)
    {
        $colors = Color::query()
            ->when($request->input('q'), function ($query, $q) {
                return $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('id', 'DESC')
            ->paginate(Helpers::pagination_limit());
        return view('admin-views.color.view', compact('colors'));
    }

    public function store(Request $request)
    {
        Color::create([
            'name' => $request->name,
            'code' => $request->code
        ]);
        Toastr::success(translate('Color added successfully!'));
        return back();
    }

    public function show($id)
    {
        return redirect()->route('admin.colors.index');
    }

    public function edit($id)
    {
        $color = Color::findOrFail($id);
        return view('admin-views.color.edit', compact('color'));
    }

    public function update(Request $request, $id)
    {
        $color = Color::findOrFail($id);
        $color->update([
            'name' => $request->name,
            'code' => $request->code,
        ]);

        Toastr::success(translate('Color updated successfully!'));
        return redirect()->route('admin.colors.index');
    }

    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $color->delete();
        return response()->json();
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $data = Color::orderBy('id', 'desc')->get();
            return response()->json($data);
        }
    }
}
