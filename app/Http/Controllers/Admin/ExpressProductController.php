<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ExpressProduct;
use Brian2694\Toastr\Facades\Toastr;
use App\CPU\ImageManager;
use App\Model\Express;
use App\Model\UserNotify;
use App\User;

use function App\CPU\translate;

class ExpressProductController extends Controller
{
    public $products = [];
    public function __construct()
    {
        if (request('products')) {

            $this->products = [];

            foreach (request('products') as $value) {
                if (!empty(array_filter($value))) {
                    $this->products[] = $value;
                }
            }
        }
    }
    public function index(Request $request)
    {
        $expressProduct = ExpressProduct::
        whereHas('sender',function($sender){
          //  $sender->where('id',request('sender'));
        })->
        latest('id')
        ->when(request('search'),function($query){
                $query->where('name','like','%'.request('search').'%');
                $query->orWhere('length','like','%'.request('search').'%');
                $query->orWhere('height','like','%'.request('search').'%');
                $query->orWhere('width','like','%'.request('search').'%');
                $query->orWhere('unit','like','%'.request('search').'%');
            })->when(request('status'),function($query){
                $query->where('status',request('status'));
            })
            ->orderBy('created_at','desc')
        ->paginate(Helpers::pagination_limit());

        return view('admin-views.express-product.index', compact('expressProduct'));
    }
    public function moreUser()
    {
        return User::when(request('search'),function($query){
            $query->where('name','like','%'.request('search').'%');
            $query->orWhere('f_name','like','%'.request('search').'%');
            $query->orWhere('l_name','like','%'.request('search').'%');
            $query->orWhere('refcode','like','%'.request('search').'%');
        })
        ->latest('id')->paginate(Helpers::pagination_limit());
    }
    public function create(Request $request)
    {
        return view('admin-views.express-product.create');
    }

    public function updateOrCreate(Request $request, $update = false)
    {


        $request->validate([
            'sender_id'=> 'required',
            //'products.*.name' => 'required',
            'products.*.weight' => 'required',
            'products.*.unit' => 'required',
        ], [], [
            'sender_id' => 'Sender',
            'products.*.weight' => 'Product weight',
            'products.*.unit' => 'Product unit',
        ]);
        $products = collect();
        if ($this->products) {
            foreach ($this->products as $product) {
                if (@$product['image']) {

                    $image = collect(explode('/', $product['image']))->last();
                    $file  = ImageManager::move('temp/' . $image, 'express-product-image/', $image);
                    if ($file) {
                        $product['image'] = asset('storage/express-product-image/' . $file);
                    }
                }
                $product['sender_id'] = $request->sender_id;
                $product = ExpressProduct::updateOrCreate(['id' => @$product['id']], $product);
                $products->add($product);
            }
        }

        if ($update) {
            if (count($this->products) > 1) {
                Toastr::success(translate('Express product create successfully!'));
            }
            Toastr::success(translate('Express product updated successfully!'));
            return redirect()->route('admin.express-product.index');
        } else {
            foreach ($products as $key => $product) {
                UserNotify::create([
                    'user_id' =>  $request->sender_id,
                    'message' => translate('Create Express Products'),
                    'model_id' => $product->id,
                    'model' => ExpressProduct::class,
                ]);
            }

            Toastr::success(translate('Express product create successfully!'));
            return redirect()->route('admin.express-product.index');
        }
    }
    public function store(Request $request)
    {
        return $this->updateOrCreate($request);
    }

    public function edit(Request $request, $id)
    {

        $expressProduct = ExpressProduct::whereIn('id',explode(',',$id))->get();

        $users = User::where('id',$expressProduct->first()->sender_id)->get();

        return view('admin-views.express-product.edit', compact('expressProduct', 'users'));
    }

    public function productBySender(Request $request)
    {
        $express = Express::find($request->express_id);
        $products = ExpressProduct::where('sender_id',$request->sender_id)
        ->where('status','warehousing')
        ->get();
        if($express){
            $products = ExpressProduct::whereIn('id',$express->express_products->merge($products->pluck('id')))->latest('id')->get();
        }
        $express_products = $request->express_products??[];

        return view('admin-views.express-delivery.templates.express_product_by_sender', compact('products', 'express_products'));
    }

    public function update(Request $request, $id)
    {
        return $this->updateOrCreate($request, true);
    }

    public function destroy($id)
    {
        // $expressProduct = ExpressProduct::findOrFail($id);
        // if ($expressProduct->image) {
        //     $image = str_replace(asset('storage/'), '', $expressProduct->image);
        //     ImageManager::delete($image);
        // }

        // $expressProduct->delete();
        // Toastr::success(translate('Express product delete successfully!');
    }
}
