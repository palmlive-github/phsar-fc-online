<?php

namespace App\Http\Controllers\Admin;

use App\CPU\ImageManager;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\PaymentMethod;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

use function App\CPU\translate;

class ProfileController extends Controller
{

    public function view()
    {
        $data = Admin::where('id', auth('admin')->id())->first();
        return view('admin-views.profile.view', compact('data'));
    }

    public function edit($id)
    {
        $data = Admin::where('id', $id)->first();
        $payment_methods = PaymentMethod::get();
        return view('admin-views.profile.edit', compact('data','payment_methods'));
    }

    public function update(Request $request, $id)
    {
        $admin = Admin::find($id);
        $admin->name = $request->name;
        $admin->phone = $request->phone;
        $admin->email = $request->email;
        if ($request->image) {
            $admin->image = ImageManager::update('admin/', $admin->image, 'png', $request->file('image'));
        }
        $admin->save();
        Toastr::info('Profile updated successfully!');
        return back();
    }

    public function settings_password_update(Request $request)
    {
        $request->validate([
            'password' => 'required|same:confirm_password|min:8',
            'confirm_password' => 'required',
        ]);

        $admin = Admin::find(auth('admin')->id());
        $admin->password = bcrypt($request['password']);
        $admin->save();
        Toastr::success(translate('Admin password updated successfully!'));
        return back();
    }

    public function bank_update(Request $request)
    {
        $request->validate([
            'payment_method_id' => 'required',
            'holder_name' => 'required',
            'account_no' => 'required',
        ]);
        $bank = Admin::findOrFail(1);
        $bank->payment_method_id = $request->payment_method_id;
//        $bank->branch = $request->branch;
        $bank->holder_name = $request->holder_name;
        $bank->account_no = $request->account_no;
        $bank->save();
        Toastr::success(translate('Bank Info updated successfully!'));
        return back();
    }

}
