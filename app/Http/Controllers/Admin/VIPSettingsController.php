<?php

namespace App\Http\Controllers\Admin;

use App\CPU\ImageManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VIPSettings;
use Brian2694\Toastr\Facades\Toastr;

use function App\CPU\translate;

class VIPSettingsController extends Controller
{
    public $payments = [];
    public function __construct()
    {
        if(request('payments')){

            $this->payments = [];

            foreach (request('payments') as $value) {
                if (!empty(array_filter($value))) {
                    $this->payments[] = $value;
                }
            }
        }
    }
    public function index(Request $request)
    {
        $vip = VIPSettings::first()??new VIPSettings();
        return view('admin-views.vip-settings.index', compact('vip'));
    }

    public function create(Request $request)
    {


    }

    public function store(Request $request)
    {
        $request->merge([
            'revenue_from_register'=> request('fee') ,
            'show_vip_request_reject_button'=> request('show_vip_request_reject_button',0) ,
        ]);

        $request->validate([
            'fee' => 'required',
            'revenue_from_register' => 'required',
            'payments.*.name' => 'required',
            'payments.*.account_name' => 'required',
            'payments.*.account_number' => 'required',
            'show_vip_request_reject_button'=> 'required',
        ],[],[
            'payments.*.name' => 'Bank name',
            'payments.*.account_name' => 'Account name',
            'payments.*.account_number' => 'Account number',
        ]);

        $oldpayments = VIPSettings::first()->payments;
        $payments = collect();
        foreach ($this->payments as $key => $payment) {
            if (@$payment['image']) {

                $image = collect(explode('/', $payment['image']))->last();
                $file  = ImageManager::move('temp/' . $image, 'vip-payment-image/', $image);
                if ($file) {
                    $payment['image'] = asset('storage/vip-payment-image/' . $file);
                    if($oldpayments->get($key) && $oldimage = @$oldpayments->get($key)['image']){
                        $image = str_replace(asset('storage/'), '', $oldimage);
                        ImageManager::delete($image);
                    }
                }
            }
            $payments->add($payment);
        }


        foreach ($oldpayments->pluck('image') as $key => $value) {
            if(!in_array($value, array_column($this->payments,'image'))){
                $image = str_replace(asset('storage/'), '', $value);
                ImageManager::delete($image);
            }
        }

        VIPSettings::updateOrCreate([
            'id' => $request->id
        ],[
            'fee' => $request->fee,
            'revenue_from_register' => $request->revenue_from_register,
            'can_withdrawal' => $request->can_withdrawal,
            'base_refcode' => $request->base_refcode,
            'payments' => $payments,
            'show_vip_request_reject_button' => $request->show_vip_request_reject_button,
        ]);

        Toastr::success(translate('Udated successfully!'));
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {

    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {

    }



}
