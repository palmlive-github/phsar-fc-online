<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\Model\Translation;
use App\Model\PaymentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VIPRequest;
use App\Model\VIPSettings;
use Brian2694\Toastr\Facades\Toastr;

use function App\CPU\commission_level;
use function App\CPU\commission_level_remove;
use function App\CPU\translate;

class VIPRequestController extends Controller
{
    public function index(Request $request)
    {
        $vip_requests = VIPRequest::whereHas('user',function($user){
            $value = request('search');
            if($value ){
                $user->where('f_name', 'like', "%{$value}%")
                ->orWhere('l_name', 'like', "%{$value}%")
                ->orWhere('phone', 'like', "%{$value}%")
                ->orWhere('email', 'like', "%{$value}%")
                ->orWhere('refcode', 'like', "%{$value}%");
            }
        })
        ->when(request('status'),function($query){
            $query->where('status',request('status'));
        })->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
        $vip_settings = VIPSettings::first();

        return view('admin-views.vip-request.index', compact('vip_requests','vip_settings'));
    }

    public function show($id)
    {
        $vip_request = VIPRequest::findOrFail($id);
        return view('email-templates.becomevip', compact('vip_request'));
    }

    public function create(Request $request)
    {


    }

    public function store(Request $request)
    {

    }

    public function edit(Request $request, $id)
    {

    }

    public function update(Request $request,$id)
    {
        if($vip_request = VIPRequest::find($id)){
            $vip_request->update([
                'status' => $request->status,
            ]);
            $is_vip = false;
            switch ($vip_request->status) {
                case 'requesting':
                case 'canceled':
                    $is_vip = false;
                    commission_level_remove($vip_request->user);
                    break;
                case 'approved':
                    $is_vip = true;
                    commission_level($vip_request->user);
                    break;
            }
            $vip_request->user->update([
                'is_vip'=> $is_vip,
            ]);

            Toastr::success(translate('Udated successfully!'));
        }
        return redirect()->back();
    }

    public function destroy($id)
    {

    }



}
