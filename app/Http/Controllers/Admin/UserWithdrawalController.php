<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\CPU\ImageManager;
use App\Model\Translation;
use App\Model\PaymentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserWithdrawal;
use App\User;
use Brian2694\Toastr\Facades\Toastr;

use function App\CPU\translate;
use function App\CPU\truncate_number;

class UserWithdrawalController extends Controller
{
    public function index(Request $request)
    {
        $user_withdrawals = UserWithdrawal::whereHas('user',function($user){
            $value = request('search');
            if($value ){
                $user->where('f_name', 'like', "%{$value}%")
                ->orWhere('l_name', 'like', "%{$value}%")
                ->orWhere('phone', 'like', "%{$value}%")
                ->orWhere('email', 'like', "%{$value}%")
                ->orWhere('refcode', 'like', "%{$value}%");
            }



        })->paginate(Helpers::pagination_limit());
        return view('admin-views.user-withdrawal.index', compact('user_withdrawals'));
    }
    public function userList()
    {
        $users = User::where('is_vip',true)->where(function($user){
            $value = request('search');
            if($value ){
                $user->where('f_name', 'like', "%{$value}%")
                ->orWhere('l_name', 'like', "%{$value}%")
                ->orWhere('phone', 'like', "%{$value}%")
                ->orWhere('email', 'like', "%{$value}%")
                ->orWhere('refcode', 'like', "%{$value}%");
            }
        })->paginate(Helpers::pagination_limit());
        return view('admin-views.user-withdrawal.userlist', compact('users'));
    }

    public function userHistory($id)
    {
        $user = User::find($id);
        $history_types = collect();
            foreach ($user->balances()->groupBy('model')->get() as $row) {
            $history_types->add( collect(explode('\\', $row->model))->last());
        }

        $history_balance = $user->balances()->where(function($history){
            if(request('date')){

                    $history->whereBetween('created_at',explode(' - ',request('date')));

            }

        })->paginate(Helpers::pagination_limit());
        return view('admin-views.user-withdrawal.userhistory', compact('user','history_balance','history_types'));
    }

    public function userClear(Request $request ,$id)
    {
        $user = User::find($id);

        if($request->method() == 'POST'){
            $request->validate([
                'balance' => 'required',
                'noted' => 'required',
                'image' => 'required',
            ]);
            $balance = truncate_number($user->balances()->sum('balance') - $user->withdrawals()->sum('balance'));

            if($request->balance >  $balance){
                Toastr::error(translate('Clear commission unsuccessfully!'));
                return back()->withInput();
            }
            $image_url = '';

            $image = collect(explode('/', $request->image))->last();
            $file  = ImageManager::move('temp/' . $image, 'transenction-image/', $image);
            if ($file) {
                $image_url = asset('storage/transenction-image/' . $file);

            }
            $user->withdrawals()->create([
                'balance' => $request->balance,
                'noted' => $request->noted,
                'image' => $image_url,
            ]);
            Toastr::success(translate('Clear commission successfully!'));
        }
        return view('admin-views.user-withdrawal.userclear', compact('user'));
    }
    public function editUserClear(Request $request ,$id)
    {
        $userWithdrawal = UserWithdrawal::find($id);
        $user = $userWithdrawal->user;

        if($request->method() == 'POST'){
            $request->validate([
                'balance' => 'required',
                'noted' => 'required',
                'image' => 'required',
            ]);
            $balance = truncate_number($user->balances()->sum('balance') - $user->withdrawals()->whereNotIn('id',[$userWithdrawal->id])->sum('balance'));

            if($request->balance > $balance){
                Toastr::error(translate('Update clear commission unsuccessfully!'));
                return back()->withInput();
            }
            $image_url = '';
            $image = collect(explode('/', $request->image))->last();
            $file  = ImageManager::move('temp/' . $image, 'transenction-image/', $image);
            if ($file) {
                $image_url = asset('storage/transenction-image/' . $file);

            }
            $userWithdrawal->update([
                'balance' => $request->balance,
                'noted' => $request->noted,
                'image' => $image_url,
            ]);
            Toastr::success(translate('Update clear commission successfully!'));
            return redirect(route('admin.user-withdrawal.index'));
        }
        return view('admin-views.user-withdrawal.edituserclear', compact('user','userWithdrawal'));
    }


    public function create(Request $request)
    {


    }

    public function store(Request $request)
    {

    }

    public function edit(Request $request, $id)
    {

    }

    public function update(Request $request,$id)
    {
        if($user_withdrawal = UserWithdrawal::find($id)){
            $user_withdrawal->update([
                'approved_by' => auth('admin')->id(),
                'status' => $request->status,
            ]);
            Toastr::success(translate('Udated successfully!'));
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        if($user_withdrawal = UserWithdrawal::find($id)){
            $user_withdrawal->delete();
            Toastr::success(translate('Cancel successfully!'));
        }
    }



}
