<?php

namespace App\Http\Controllers\Admin;

use App\CPU\ImageManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Express;
use App\Model\ExpressDelivery;
use App\Model\ExpressProduct;
use App\Model\UserNotify;
use Brian2694\Toastr\Facades\Toastr;
use App\User;
use Illuminate\Support\Facades\Mail;

use function App\CPU\check_refcode;
use function App\CPU\commission_level;
use function App\CPU\commission_level_remove;
use function App\CPU\generate_refcode;
use function App\CPU\translate;

class ExpressDeliveryController extends Controller
{
    public $products = [];
    public function __construct()
    {
        if (request('products')) {

            $this->products = [];

            foreach (request('products') as $value) {
                $unit = $value['unit'];
                    unset($value['unit']);
                if (!empty(array_filter($value))) {
                    $value['unit'] = $unit;
                    $this->products[] = $value;
                }
            }
        }
    }
    public function index(Request $request)
    {
        session()->remove('users');

        // $expressDelivery = ExpressDelivery::latest('id')
        //     ->when(request('search'), function ($query) {

        //         $query->orWhere('from', 'like', '%' . request('search') . '%');
        //         $query->orWhere('to', 'like', '%' . request('search') . '%');
        //         $query->orWhere('sender_info', 'like', '%' . request('search') . '%');
        //         $query->orWhere('receiver_info', 'like', '%' . request('search') . '%');
        //     })->paginate(Helpers::pagination_limit());
        // return view('admin-views.express-delivery.index', compact('expressDelivery'));

        $express = Express::latest('id')
            ->whereHas('delivery', function ($delivery) {
                if (request('search')) {

                    $delivery->where('from', 'like', '%' . request('search') . '%');
                    $delivery->orWhere('to', 'like', '%' . request('search') . '%');
                    $delivery->orWhere('sender_info', 'like', '%' . request('search') . '%');
                    $delivery->orWhere('receiver_info', 'like', '%' . request('search') . '%');
                }
            })->when(request('search'), function ($query) {

                $query->orWhere('price', 'like', '%' . request('search') . '%');
            })->when(request('status'), function ($query) {
                $query->where('status', request('status'));
            })
            ->paginate(5);
        return view('admin-views.express-delivery.index', compact('express'));
    }

    public function create(Request $request)
    {

        return view('admin-views.express-delivery.create');
    }

    public function updateOrCreate(Request $request)
    {
        $rules = [
            'from' => 'required',
            'to' => 'required',
            'sender.name' => 'required',
            'sender.phone.main' => 'required',
            'sender.address' => 'required',
            'receiver.name' => 'required',
            'receiver.phone.main' => 'required',
            'receiver.address' => 'required',
        ];
        if ($request->id) {
           // $rules['payment_image'] = 'required';
        }


        if($request->sender['id'] || $request->receiver['id']){
            $ids =  array_filter([@$request->sender['id'], @$request->receiver['id']]);
            $users = User::with('shippings')-> whereIn('id', $ids)->get();
            session()->put('users',$users);

        }



        $request->validate($rules, [], [
            'sender.name' => translate('Sender name'),
            'sender.phone.main' => translate('Sender phone'),
            'sender.address' => translate('Sender address'),
            'receiver.name' => translate('Receiver name'),
            'receiver.phone.main' => translate('Receiver phone'),
            'receiver.address' => translate('Receiver address'),
        ]);

        $refcode_to = substr($request->refcode_to, 0, 6);

        // if ($request->id) {
        //     if ($request->without_refcode) {
        //         $refcode_to = null;
        //     } else {
        //         $refcode_user = check_refcode($refcode_to);
        //         if (!$refcode_user) {
        //             Toastr::error(translate('Refcode not found'));
        //             return back()->withInput();
        //         }
        //         if ($refcode_user->is_vip == 0) {
        //             Toastr::error(translate('Referral not VIP'));
        //             return back()->withInput();
        //         }

        //         if ($refcode_user->is_active == 0) {
        //             Toastr::error(translate('Referral is Disabled'));
        //             return back()->withInput();
        //         }
        //     }
        // }



        $sender_info = $request->sender;

        if (@$request->sender['id']) {
            $sender = User::find($request->sender['id']);
        } else {
            $sender = User::firstOrCreate([
                'phone' => $request->sender['phone']['full'],
            ], $sender_info);
        }

        $refcode_to = $sender->refcode;


        $receiver_info = $request->receiver;
        $receiver_info['refcode'] = generate_refcode();

        if (@$request->receiver['id']) {
            $receiver = User::find($request->receiver['id']);
        } else {
            $receiver = User::firstOrCreate([
                'phone' => $request->receiver['phone']['full'],
            ], $receiver_info);
        }

        $address_image = null;
        if($request->address_image){

            $image = collect(explode('/',$request->address_image))->last();
            $file  = ImageManager::move('temp/' . $image, 'address-image/', $image);

            if($file){
                $address_image = asset('storage/address-image/' . $file);
            }

        }
        $delivery = ExpressDelivery::updateOrCreate(['id' => $request->id], [

            'from' => $request->from,
            'to' => $request->to,
            'sender_id' => $sender->id,
            'sender_info' => $sender_info,
            'receiver_id' => $receiver->id,
            'receiver_info' => $receiver_info,
        ]);
        if($address_image ){
            $delivery->update([
                'address_image' => $address_image
            ]);
        }


        $products = collect();
        if ($this->products) {

            foreach ($this->products as $product) {

                if (@$product['image']) {
                    $image = collect(explode('/',$product['image']))->last();
                    $file  = ImageManager::move('temp/' . $image, 'express-product-image/', $image);
                    if ($file) {
                        $product['image'] = asset('storage/express-product-image/' . $file);
                    }
                }
                $product['sender_id'] = $sender->id;
                $product['status'] = 'deliver';
                $product = ExpressProduct::updateOrCreate(['id' => @$product['id']], $product);
                $products->add($product);
            }
        }

        $products = $products->pluck('id')->merge(request('express_products', []));
        ExpressProduct::whereIn('id', $products)->update(['status' => 'deliver']);
        $products =  ExpressProduct::whereIn('id',$products)->get();

        $payment_image = null;
        if($request->payment_image){

            $image = collect(explode('/',$request->payment_image))->last();
            $file  = ImageManager::move('temp/' . $image, 'payment-image/', $image);

            if($file){
                $payment_image = asset('storage/payment-image/' . $file);
            }
        }

        $model =  Express::updateOrCreate(
            [
                'express_delivery_id' => $delivery->id,
            ],
            [
                'express_delivery_id' => $delivery->id,
                'express_products' => $products->pluck('id'),
                'price' => $products->sum('price'),
                'status' => $request->status??'pending',
                'noted' => $request->noted,
                'refcode_to' => $refcode_to,
            ],
        );
        if($payment_image ){
            $model->update([
                'payment_image' => $payment_image
            ]);
        }

        $sender->notify()->where('model' , Express::class)
        ->where('model_id',$model->id)->delete();
        commission_level_remove($model);
        if($request->status == 'received'){
            Mail::to($sender->email)->send(new \App\Mail\ExpressDelivery($model->id));
            $sender->notify()->create([
                'message' => translate('Express received'),
                'model_id' =>  $model->id,
                'model' => Express::class,
            ]);
            commission_level($model);
        }

        if ($request->id) {
            Toastr::success(translate('Express delivery updated successfully!'));
            return back();
        } else {
            Toastr::success(translate('Express delivery create successfully!'));
            return redirect()->route('admin.express-delivery.index');
        }
    }
    public function store(Request $request)
    {

        return $this->updateOrCreate($request);
    }

    public function edit(Request $request, $id)
    {

        $expressDelivery = ExpressDelivery::findOrFail($id);
        $products = ExpressProduct::where('sender_id', $expressDelivery->sender_id)
            ->where('status', 'warehousing')
            ->get();
        $express = Express::where('express_delivery_id', $id)->first();
        if ($express) {
            $products = ExpressProduct::whereIn('id', array_merge($express->express_products->toArray(), $products->pluck('id')->toArray()))->latest('id')->get();
        }
        $products = ExpressProduct::whereIn('id', array_merge($express->express_products->toArray(), $products->pluck('id')->toArray()))->latest('id')->get();


        $users = User::with('shippings')-> whereIn('id', [$expressDelivery->sender_id, $expressDelivery->receiver_id])->get();

        return view('admin-views.express-delivery.edit', compact('express', 'expressDelivery', 'users', 'products'));
    }

    public function update(Request $request, $id)
    {

        $request->merge(['id' => $id]);
        if( $express = Express::where('express_delivery_id',$id)->first()){
            if($express->express_products->count()){
                ExpressProduct::whereIn('id',$express->express_products)->update(['status' => 'warehousing']);
             }
        }

        return $this->updateOrCreate($request);
    }
    public function show(Request $request, $id)
    {

        $express = Express::findOrFail($id);
       //return view('email-templates.express-delivery',compact('express'));
        return view('admin-views.express-delivery.show',compact('express'));
    }

    public function updateStatus(Request $request)
    {
        $express = Express::findOrFail($request->id);
        $sender = $express->delivery->sender;
        $update  = $express->update([
            'status' => $request->status,
        ]);
        if($request->status == 'received'){

            commission_level($express);
            Mail::to($sender->email)->send(new \App\Mail\ExpressDelivery($express->id));
            $sender->notify()->create([
                'message' => translate('Express received'),
                'model_id' =>  $express->id,
                'model' => Express::class,
            ]);
        }else{

         commission_level_remove($express);
         $sender->notify()->where('model' , Express::class)
         ->where('model_id',$express->id)->delete();
        }
        ExpressProduct::whereIn('id', $express->express_products)->update([
            'status' => $request->status,
        ]);
        if ($update) {
            return [
                'status' => true,
                'message' => translate('Update Successfully'),
            ];
        }
        return [
            'status' => false,
            'message' => translate('Update unsuccessful'),
        ];
    }

    public function destroy($id)
    {
        $expressDelivery = ExpressDelivery::findOrFail($id);
        Express::where('express_delivery_id', $id)->delete();
        $expressDelivery->delete();
        Toastr::success(translate('Express delivery delete successfully!'));
    }
}
