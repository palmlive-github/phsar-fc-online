<?php

namespace App\Http\Controllers\Web;

use App\User;
use Carbon\Carbon;
use App\CPU\Helpers;
use App\Model\Order;
use App\Model\Wishlist;
use App\CPU\ImageManager;
use App\CPU\OrderManager;
use App\Model\VIPSettings;
use App\CPU\CustomerManager;
use App\Model\PaymentMethod;
use App\Model\SupportTicket;
use Illuminate\Http\Request;
use App\Model\ShippingAddress;

use function App\CPU\check_refcode;
use function App\CPU\commission_level;
use function App\CPU\generate_refcode;
use function App\CPU\translate;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\Express;
use App\Model\ExpressDelivery;
use App\Model\ExpressProduct;
use App\Model\VIPRequest;
use Brian2694\Toastr\Facades\Toastr;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class UserProfileController extends Controller
{
    public function user_account(Request $request)
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            return view('web-views.users-profile.account-profile', compact('customerDetail'));
        } else {
            return redirect()->route('home');
        }
    }

    public function user_update(Request $request)
    {

        $image = $request->file('image');

        if ($image != null) {
            $imageName = ImageManager::update('profile/', auth('customer')->user()->image, 'png', $request->file('image'));
        } else {
            $imageName = auth('customer')->user()->image;
        }

        User::where('id', auth('customer')->id())->update([
            'image' => $imageName,
        ]);

        if ($request['password'] != $request['con_password']) {
            Toastr::error(translate('Password did not match.'));
            return back();
        }

        $userDetails = [
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
//            'phone' => $request->phone,
            'password' => strlen($request->password) > 5 ? bcrypt($request->password) : auth('customer')->user()->password,
        ];
        if (auth('customer')->check()) {
            User::where(['id' => auth('customer')->id()])->update($userDetails);
            Toastr::info(translate('updated_successfully'));
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function account_address()
    {
        if (auth('customer')->check()) {
            $shippingAddresses = \App\Model\ShippingAddress::where('customer_id', auth('customer')->id())->get();
            return view('web-views.users-profile.account-address', compact('shippingAddresses'));
        } else {
            return redirect()->route('home');
        }
    }

    public function address_store(Request $request)
    {
        $address = [
            'customer_id' => auth('customer')->check() ? auth('customer')->id() : null,
            'contact_person_name' => $request->name,
            'address_type' => $request->addressAs,
            'address' => $request->address,
            'city' => $request->city,
            'zip' => $request->zip,
            'phone' => $request->phone,
            'state' => $request->state,
            'country' => $request->country,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        DB::table('shipping_addresses')->insert($address);
        return back();
    }

    public function address_update(Request $request)
    {
        $updateAddress = [
            'contact_person_name' => $request->name,
            'address_type' => $request->addressAs,
            'address' => $request->address,
            'city' => $request->city,
            'zip' => $request->zip,
            'phone' => $request->phone,
            'state' => $request->state,
            'country' => $request->country,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        if (auth('customer')->check()) {
            ShippingAddress::where('id', $request->id)->update($updateAddress);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function address_delete(Request $request)
    {
        if (auth('customer')->check()) {
            ShippingAddress::destroy($request->id);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function account_payment()
    {
        if (auth('customer')->check()) {
            return view('web-views.users-profile.account-payment');

        } else {
            return redirect()->route('home');
        }

    }

    public function account_oder()
    {
        $orders = Order::where('customer_id', auth('customer')->id())->orderBy('id','DESC')->get();
        return view('web-views.users-profile.account-orders', compact('orders'));
    }
    public function account_balance()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            $history_balance = $customerDetail->balances()->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
            return view('web-views.users-profile.account-balance', compact('customerDetail','history_balance'));
        } else {
            return redirect()->route('home');
        }
    }
    public function account_withdrawal()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){
                $vip = VIPSettings::first();
                $amount = request('amount');
                $balance = $customerDetail->balances->sum('balance') -  $customerDetail->withdrawals->sum('balance');
                if(request('amount')> $balance ){
                    Toastr::error(translate('request_unsuccessful'));
                }elseif(request('amount') <  $vip->can_withdrawal){
                    Toastr::error(translate('Withdrawal up to 100$'));
                    Toastr::error(translate('request_unsuccessful'));
                }else{
                    $withdrawal = $customerDetail->withdrawals->where('status','requesting')->first();
                    if($withdrawal){
                        $amount+=$withdrawal->balance;
                        $withdrawal->update([
                            'balance' =>$amount,
                            'created_at' => now(),
                        ]);
                    }else{
                        $customerDetail->withdrawals()->create([
                            'balance' =>$amount,
                            'status' => 'requesting',
                        ]);
                    }
                    Toastr::info(translate('request_successfully'));
                    return redirect()->back();
                }

            }
            $withdrawals = $customerDetail->withdrawals()->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
            return view('web-views.users-profile.account-withdrawal', compact('customerDetail','withdrawals'));
        } else {
            return redirect()->route('home');
        }
    }


    public function account_withdrawal_cancel($id)
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            $customerDetail->withdrawals->find($id)->delete();
            Toastr::info(translate('cancel_successfully'));
            return redirect()->back();
        }

        return redirect()->route('home');
    }
    public function account_bankinfo()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){

                request()->validate([
                    'payment_method_id' => 'required',
                    'holder_name' => 'required',
                    'account_no' => 'required',
                ]);

                $customerDetail->update([
                    'payment_method_id' =>  request()->payment_method_id,
                    'holder_name' =>  request()->holder_name,
                    'account_no' =>  request()->account_no,
                ]);
                Toastr::info(translate('updated_successfully'));
            }
            $payment_method = PaymentMethod::get();
            return view('web-views.users-profile.account-bankinfo', compact('customerDetail','payment_method'));
        } else {
            return redirect()->route('home');
        }
    }

    public function account_refcode()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){
                $refcode = random_int(100000, 999999);
                if($customerDetail->refcode == null){
                    $refcode = substr(request('refcode',$refcode), 0, 6);
                    if(User::where('refcode',$refcode)->exists()){
                        return back()->withErrors(['refcode' => 'Referral code is exists.']);
                    }else{
                        $customerDetail->update([
                            'refcode' => $refcode,
                        ]);
                        Toastr::info(translate('updated_successfully'));
                    }
                }
            }
            $myrefs  = $customerDetail->myrefs()->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
            return view('web-views.users-profile.account-refcode', compact('customerDetail','myrefs'));
        } else {
            return redirect()->route('home');
        }
    }

    public function account_period()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){
                if (request()->period) {
                    $period = explode(' - ',request()->period);
                    $start = $period[0];
                    $end =$period[1];

                    $customerDetail->update([
                        'period_start' =>  $start,
                        'period_end' =>  $end,
                    ]);
                    Toastr::info(translate('updated_successfully'));
                }else{
                    return back()->withErrors(['refcode' => 'Referral period is required.']);
                }
            }
            return view('web-views.users-profile.account-period', compact('customerDetail'));
        } else {
            return redirect()->route('home');
        }
    }
    public function account_become_vip()
    {

        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            $vip = PaymentMethod::get();
            if(request()->method() == 'POST'){
                $customerDetail->update([
                    'payment_method_id' => request()->payment_method_id,
                    //'bank_name' => PaymentMethod::find($request->payment_method_id)->name,
                    'holder_name' => request()->holder_name,
                    'account_no' => request()->account_no,
                    'id_card_image' => ImageManager::upload('nationid-card/', 'png', request()->file('id_card_image')),
                ]);

                $customerDetail->vipRequests()->create([
                    'status' => 'requesting',
                    'total_fee' => $vip->fee,
                    'payment' => PaymentMethod::find(request('payment')),
                    'upload_payment' => ImageManager::upload('upload-payment/', 'png', request()->file('upload_payment')),
                ]);
                Toastr::info(translate('request_successfully'));
            }
            $payment_method = PaymentMethod::get();

            return view('web-views.users-profile.account-become-vip', compact('customerDetail','payment_method','vip'));
        } else {
            return redirect()->route('home');
        }
    }

    public function account_order_details(Request $request)
    {
        $order = Order::find($request->id);
        return view('web-views.users-profile.account-order-details', compact('order'));
    }
    public function account_received_order(Request $request,$id)
    {
        $order = Order::find($request->id);
        if(in_array( $order->order_status,['confirmed','processing','out_for_delivery'])){
            $order->update(['order_status' => 'delivered']);
            commission_level($order);
            return response()->json(true);
        }
        return response()->json(false);

    }

    public function account_wishlist()
    {
        if (auth('customer')->check()) {
            $wishlists = Wishlist::where('customer_id', auth('customer')->id())->get();
            return view('web-views.products.wishlist', compact('wishlists'));
        } else {
            return redirect()->route('home');
        }
    }

    public function account_tickets()
    {
        if (auth('customer')->check()) {
            $supportTickets = SupportTicket::where('customer_id', auth('customer')->id())->get();
            return view('web-views.users-profile.account-tickets', compact('supportTickets'));
        } else {
            return redirect()->route('home');
        }
    }

    public function ticket_submit(Request $request)
    {
        $ticket = [
            'subject' => $request['ticket_subject'],
            'type' => $request['ticket_type'],
            'customer_id' => auth('customer')->check() ? auth('customer')->id() : null,
            'priority' => $request['ticket_priority'],
            'description' => $request['ticket_description'],
            'created_at' => now(),
            'updated_at' => now(),
        ];
        DB::table('support_tickets')->insert($ticket);
        return back();
    }

    public function single_ticket(Request $request)
    {
        $ticket = SupportTicket::where('id', $request->id)->first();
        return view('web-views.users-profile.ticket-view', compact('ticket'));
    }

    public function comment_submit(Request $request, $id)
    {
        DB::table('support_tickets')->where(['id' => $id])->update([
            'status' => 'open',
            'updated_at' => now(),
        ]);

        DB::table('support_ticket_convs')->insert([
            'customer_message' => $request->comment,
            'support_ticket_id' => $id,
            'position' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        return back();
    }

    public function support_ticket_close($id)
    {
        DB::table('support_tickets')->where(['id' => $id])->update([
            'status' => 'close',
            'updated_at' => now(),
        ]);
        Toastr::success(translate('Ticket closed!'));
        return redirect('/account-tickets');
    }

    public function account_transaction()
    {
        $customer_id = auth('customer')->id();
        $customer_type = 'customer';
        if (auth('customer')->check()) {
            $transactionHistory = CustomerManager::user_transactions($customer_id, $customer_type);
            return view('web-views.users-profile.account-transaction', compact('transactionHistory'));
        } else {
            return redirect()->route('home');
        }
    }

    public function support_ticket_delete(Request $request)
    {

        if (auth('customer')->check()) {
            $support = SupportTicket::find($request->id);
            $support->delete();
            return redirect()->back();
        } else {
            return redirect()->back();
        }

    }

    public function account_wallet_history($user_id, $user_type = 'customer')
    {
        $customer_id = auth('customer')->id();
        if (auth('customer')->check()) {
            $wallerHistory = CustomerManager::user_wallet_histories($customer_id);
            return view('web-views.users-profile.account-wallet', compact('wallerHistory'));
        } else {
            return redirect()->route('home');
        }

    }

    public function track_order()
    {
        return view('web-views.order-tracking-page');
    }

    public function track_order_result(Request $request)
    {
        $orderDetails = Order::where('id',$request['order_id'])->whereHas('details',function ($query){
            $query->where('customer_id',auth('customer')->id());
        })->first();

        if (isset($orderDetails)){
            return view('web-views.order-tracking', compact('orderDetails'));
        }
        return redirect()->route('track-order.index')->with('Error', 'Invalid Order Id');
    }

    public function track_last_order()
    {
        $orderDetails = OrderManager::track_order(Order::where('customer_id', auth('customer')->id())->latest()->first()->id);

        if ($orderDetails != null) {
            return view('web-views.order-tracking', compact('orderDetails'));
        } else {
            return redirect()->route('track-order.index')->with('Error', 'Invalid Order Id or Phone Number');
        }

    }

    public function order_cancel($id)
    {
        $order = Order::where(['id' => $id])->first();
        if ($order['payment_method'] == 'cash_on_delivery' && $order['order_status'] == 'pending') {
            OrderManager::stock_update_on_order_status_change($order, 'canceled');
            Order::where(['id' => $id])->update([
                'order_status' => 'canceled'
            ]);
            Toastr::success(translate('successfully_canceled'));
            return back();
        }
        Toastr::error(translate('status_not_changable_now'));
        return back();
    }

    public function generate_invoice($id)
    {
        $order = Order::with('seller')->with('shipping')->where('id', $id)->first();
        $data["email"] = $order->customer["email"];
        $data["order"] = $order;

        $mpdf_view = \View::make('web-views.invoice')->with('order', $order);
        Helpers::gen_mpdf($mpdf_view, 'order_invoice_', $order->id);
    }
}
