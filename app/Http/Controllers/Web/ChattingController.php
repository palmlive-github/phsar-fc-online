<?php

namespace App\Http\Controllers\Web;

use Auth;
use Pusher\Pusher;
use App\Events\Chat;
use App\Model\Chatting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ChattingController extends Controller
{
    public function chat_with_seller(Request $request)
    {
        // $last_chat = Chatting::with('seller_info')->where('user_id', auth('customer')->id())
        //     ->orderBy('created_at', 'DESC')
        //     ->first();
        $last_chat = Chatting::with(['shop'])->where('user_id', auth('customer')->id())
            ->orderBy('created_at', 'DESC')
            ->first();
            $chattings = collect();
            $unique_shops = collect();
        if (isset($last_chat)) {
            $chattings = Chatting::join('shops', 'shops.id', '=', 'chattings.shop_id')
                ->select('chattings.*', 'shops.name', 'shops.image')
                ->where('chattings.user_id', auth('customer')->id())
                ->where('shop_id', $last_chat->shop_id)
                ->get();

            $unique_shops = Chatting::join('shops', 'shops.id', '=', 'chattings.shop_id')
                ->select('chattings.*', 'shops.name', 'shops.image')
                ->where('chattings.user_id', auth('customer')->id())
                ->orderBy('chattings.created_at', 'desc')
                ->get()
                ->unique('shop_id')->map(function($row){
                    $row->unseen = Chatting::where('shop_id',$row->shop_id)->where('user_id', $row->user_id)->where('seen_by_customer',0)->count();
                    return $row;
                });;


        }
        $chatShop = $unique_shops->map(function($row){

            return [
                'shop_id' => $row->shop_id,
                'seller_id' => $row->seller_id,
                'user_id' => $row->user_id
            ];
        });
        return view('web-views.users-profile.profile.chat-with-seller', compact('chattings', 'unique_shops', 'last_chat' ,'chatShop'));

    }
    public function messages(Request $request)
    {
        $chat = Chatting::join('shops', 'shops.id', '=', 'chattings.shop_id')
            ->select('chattings.*', 'shops.name', 'shops.image')
            ->where('user_id', auth('customer')->id())
            ->where('chattings.shop_id', json_decode($request->shop_id))
            ->latest()
            ->paginate(20);
            Chatting::where('shop_id',$request->shop_id)->where('user_id', auth('customer')->id())->update(['seen_by_customer'=> 1]);
            event(new Chat([
                'seen_by_customer'=> 1,
                'shop_id' => $request->shop_id,
                'user_id' => auth('customer')->id(),
                'seller_id' => $chat->first()->seller_id,
            ],'chat-seen-event'));
        return response()->json([
            'page' => $chat->currentPage(),
            'data' => array_values($chat->reverse()->toArray()),
        ]);
    }

    public function oldMessages(Request $request)
    {
        $chat = Chatting::join('shops', 'shops.id', '=', 'chattings.shop_id')
            ->select('chattings.*', 'shops.name', 'shops.image')
            ->where('user_id', auth('customer')->id())
            ->where('chattings.shop_id', json_decode($request->shop_id))
            ->latest()
            ->paginate(20);
        return response()->json([
            'page' => $chat->currentPage(),
            'data' => array_values($chat->reverse()->toArray()),
        ]);
    }

    public function messages_store(Request $request)
    {

        if ($request->message == '') {
            Toastr::warning('Type Something!');
            return response()->json('type something!');
        } else {


             $chat = Chatting::create( [
                'user_id'          => auth('customer')->id(),
                'shop_id'          => $request->shop_id,
                'seller_id'        => $request->seller_id,
                'message'          => $request->message,
                'sent_by_customer' => 1,
                'seen_by_seller' => 0,
                'created_at'       => now(),
            ]);
            $chat->image = asset('storage/profile/'.$chat->customer->image);
            event(new Chat([
                'shop_id' => $chat->shop_id,
                'sent_by_customer' => 1,
                'sent_by_seller' => 0,
                'user_id' => auth('customer')->id(),
                'seller_id' => $request->seller_id,
                'message' => $request->message,
                'image' =>  $chat->image,
                'created_at'=> now(),
            ]));
            event(new Chat([
                'seen_by_customer' => 1,
                'shop_id' => $chat->shop_id,
                'seller_id' =>  $request->seller_id,
                'user_id' => auth('customer')->id(),
            ],'chat-seen-event'));
            return response()->json($request->message);
        }
    }

}
