<?php

namespace App\Http\Controllers\Web\Express;

use App\CPU\Helpers;
use App\CPU\ImageManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ExpressProduct;
use Brian2694\Toastr\Facades\Toastr;

use function App\CPU\translate;

class ProductController extends Controller
{
    public $products = [];
    public function __construct()
    {
        if (request('products')) {

            $this->products = [];

            foreach (request('products') as $value) {
                if (!empty(array_filter($value))) {
                    $this->products[] = $value;
                }
            }
        }
    }
    public function index(Request $request)
    {
        $expressProducts =  ExpressProduct::whereHas('sender',function($sender){
            $sender->where('id',auth('customer')->id());
        })->where(function($query){

            $query->where('status','warehousing');
            $query->orWhereNull('status');
        })
        ->when(request('date'),function($query){
            $query->whereBetween('created_at',explode(' - ',request('date')));
        })->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());

        auth('customer')->user()->notify()->where('model',ExpressProduct::class)->update([
            'seen' => 1,
        ]);
        return view('web-views.users-profile.express.product.index', compact('expressProducts'));
    }

    public function create(Request $request)
    {
        return view('web-views.users-profile.express.product.create');
    }
    public function updateOrCreate(Request $request, $update = false)
    {

        $request->merge(['sender_id'=> auth('customer')->id()]);
        $request->validate([
            'sender_id'=> 'required',
            //'products.*.name' => 'required',
            'products.*.weight' => 'required',
            'products.*.unit' => 'required',
        ], [], [
            'sender_id' => translate('Sender'),
            'products.*.weight' => translate('Product weight'),
            'products.*.unit' => translate('Product unit'),
        ]);
        $products = collect();
        if ($this->products) {
            foreach ($this->products as $product) {
                if (@$product['image']) {
                $image = collect(explode('/',  $product['image']))->last();
                    $file  = ImageManager::move('temp/' . $image, 'express-product-image/', $image);
                    if ($file) {
                        $product['image'] = asset('storage/express-product-image/' . $file);
                    }
                }
                $product['sender_id'] = $request->sender_id;
                $product = ExpressProduct::updateOrCreate(['id' => @$product['id']], $product);
                $products->add($product);
            }
        }

        if ($update) {
            if (count($this->products) > 1) {
                Toastr::success(translate('Express product create successfully!'));
            }
            Toastr::success(translate('Express product updated successfully!'));
            return redirect()->route('express.product.index');
        } else {
            Toastr::success(translate('Express product create successfully!'));
            return redirect()->route('express.product.index');
        }
    }
    public function store(Request $request)
    {
        return $this->updateOrCreate($request);
    }

    public function edit(Request $request, $id)
    {
        $products = ExpressProduct::
        whereHas('sender',function($sender){
            $sender->where('id',auth('customer')->id());
        })->
        whereIn('id',explode(',',$id))->get();

        return view('web-views.users-profile.express.product.edit', compact('products'));
    }


    public function update(Request $request, $id)
    {
        return $this->updateOrCreate($request, true);
    }

    public function destroy($id)
    {
        $expressProduct = ExpressProduct::whereHas('sender',function($sender){
            $sender->where('id',auth('customer')->id());
        })->findOrFail($id);
        if($expressProduct->status == null){
            if ($expressProduct->image) {
                $image = str_replace(asset('storage/'), '', $expressProduct->image);
                ImageManager::delete($image);
            }

            $expressProduct->delete();
            Toastr::success(translate('Express product delete successfully!'));
        }else{
            Toastr::error(translate('Express product delete unsuccessful!'));
        }

    }


}
