<?php

namespace App\Http\Controllers\Web\Express;

use App\CPU\Helpers;
use App\User;
use App\CPU\ImageManager;
use Illuminate\Http\Request;
use function App\CPU\commission_level;
use function App\CPU\extract_phone;
use function App\CPU\generate_refcode;
use function App\CPU\translate;
use function App\CPU\validatePhone;

use App\Http\Controllers\Controller;
use App\Model\BusinessSetting;
use App\Model\Express;
use App\Model\ExpressDelivery;
use App\Model\ExpressProduct;
use App\Model\PaymentMethod;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;

class DeliveryController extends Controller
{

    public $products = [];
    public function __construct()
    {
        if (request('products')) {
            $this->products = [];
            foreach (request('products') as $value) {
                $unit = $value['unit'];
                    unset($value['unit']);
                if (!empty(array_filter($value))) {
                    $value['unit'] = $unit;
                    $this->products[] = $value;
                }
            }
        }
    }

    public function index(Request $request)
    {

        $express =  Express::whereHas('delivery',function($delivery){
            $delivery->where('sender_id',auth('customer')->id())->orWhere('receiver_id',auth('customer')->id());
        })->when(request('status'),function($query){
            $query->where('status',request('status'));
        })
        ->when(request('date'),function($query){
            $query->whereBetween('created_at',explode(' - ',request('date')));
        })->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
        auth('customer')->user()->notify()->where('model',Express::class)->update([
            'seen' => 1,
        ]);
        return view('web-views.users-profile.express.delivery.index', compact('express'));
    }



    public function searchuser()
    {
       return User::with('shippings')->when(request('search'),function($query){
        $query->where('name','like','%'.request('search').'%');
        $query->orWhere('f_name','like','%'.request('search').'%');
        $query->orWhere('l_name','like','%'.request('search').'%');
        $query->orWhere('refcode','like','%'.request('search').'%');
    })
    ->latest('id')->paginate(Helpers::pagination_limit());
    }

    public function updateOrCreate(Request $request)
    {
        $rules = [
            'from' => 'required',
            'to' => 'required',
            'receiver.name' => 'required',
            'payment' => 'required',
            'receiver.phone.main' => 'required',
            'receiver.address' => 'required',
            'payment_image' => 'required',
        ];
        if($request->from == 'khmer'){
            $rules['products.*.weight'] = 'required' ;
            $rules['products.*.unit'] = 'required' ;
            $rules['products.*.price'] = 'required' ;
        }else{
            if (!$request->express_products) {
                Toastr::error(translate('Product required'));
                return back()->withInput();
            }
        }


        if(@$request->receiver['id']){
            $ids =  array_filter([@$request->receiver['id']]);
            $users = User::with('shippings')->whereIn('id', $ids)->get();
            session()->put('users',$users);
        }

        $request->validate($rules, [], [
            'receiver.name' => translate('Receiver name'),
            'receiver.phone.main' => translate('Receiver phone'),
            'receiver.address' => translate('Receiver address'),
            'price' => translate('Price'),
            'payment_image' => translate('Payment Image'),
            'express_products[]' => translate('Product'),
        ]);


        $sender = User::find(auth('customer')->id());


        if ($request->id) {


                if ($sender->is_vip == 0) {
                    Toastr::error(translate('Referral not VIP'));
                    return back()->withInput();
                }

                if ($sender->is_active == 0) {
                    Toastr::error(translate('Referral is Disabled'));
                    return back()->withInput();
                }
        }



        $sender_info =   [
            'name' => $sender->name?? ( $sender->f_name .' '.$sender->l_name),
            'refcode' => $sender->refcode,
            'phone' => extract_phone($sender->phone),
            'address' => '',
        ];

        $receiver_info = $request->receiver;
        $receiver_info['refcode'] = generate_refcode();

        if (@$request->receiver['id']) {
            $receiver = User::find($request->receiver['id']);
        } else {
            $receiver = User::firstOrCreate([
                'phone' => $request->receiver['phone']['full'],
            ], $receiver_info);
        }



        $address_image = null;
        if($request->address_image){


            $image = collect(explode('/', $request->address_image))->last();
            $file  = ImageManager::move('temp/' . $image, 'address-image/', $image);

            if($file){
                $address_image = asset('storage/address-image/' . $file);
            }

        }



        $delivery = ExpressDelivery::updateOrCreate(['id' => $request->id], [
            'from' => $request->from,
            'to' => $request->to,
            'sender_id' => $sender->id,
           // 'sender_info' => $sender_info,
            'receiver_id' => $receiver->id,
            'receiver_info' => $receiver_info,
        ]);
        if(!$delivery->sender_info){
            $delivery->update([
                'sender_info' => $sender_info,
            ]);
        }
        if($address_image ){
            $delivery->update([
                'address_image' => $address_image
            ]);
        }
        $products = collect();
        if($request->from == 'khmer'){

            if ($this->products) {
                if($request->id){

                    ExpressProduct::whereIn('id',Express::where('express_delivery_id',$request->id)->first()->express_products)->delete();
                }


                foreach ($this->products as $index=> $product) {

                    if (@$product['image']) {
                        $image = collect(explode('/',$product['image']))->last();
                        $file  = ImageManager::move('temp/' . $image, 'express-product-image/', $image);
                        if ($file) {
                            $product['image'] = asset('storage/express-product-image/' . $file);
                        }
                    }
                    $product['index'] = $index+1;
                    $product['sender_id'] = $sender->id;
                    $product['status'] = 'deliver';
                    $product = ExpressProduct::updateOrCreate(['id' => @$product['id']], $product);
                    $products->add($product);
                }
            }
        }else{

            if($request->id){
                ExpressProduct::whereIn('id',Express::where('express_delivery_id',$request->id)->first()->express_products)->update(['status'=> 'warehousing']);
            }
            ExpressProduct::whereIn('id',request('express_products', []))->update(['status'=> 'deliver']);
            $products =  ExpressProduct::whereIn('id',request('express_products', []))->get();
        }


        $payment_image = null;
            if($request->payment_image){

                $image = collect(explode('/', $request->payment_image))->last();
                $file  = ImageManager::move('temp/' . $image, 'payment-image/', $image);

                if($file){
                    $payment_image = asset('storage/payment-image/' . $file);
                }
            }



        $model =  Express::updateOrCreate(
            [
                'express_delivery_id' => $delivery->id,
            ],
            [
                'express_delivery_id' => $delivery->id,
                'express_products' => $products->pluck('id'),
                'price' => $products->sum('price'),
                'noted' => $request->noted,
                'refcode_to' => $sender->refcode,
                'payment' => PaymentMethod::find($request->payment),
            ],
        );
        if($payment_image ){
            $model->update([
                'payment_image' => $payment_image
            ]);
        }

        if ($request->id) {
            ExpressProduct::whereIn('id', $products)->update(['status' => 'deliver']);
            Toastr::success(translate('Express delivery updated successfully!'));
            return back();
        } else {
            if($company_email =  BusinessSetting::where('type', 'company_email')->first()){

                Mail::to($company_email->value)->send(new \App\Mail\ExpressDelivery($model->id,'admin'));
            }

            Toastr::success(translate('Express delivery create successfully!'));
            return redirect()->route('express.delivery.index');
        }
    }

    public function create(Request $request)
    {
        $payment_method = PaymentMethod::latest('id')-> get();
        $expressProducts =  ExpressProduct:: whereHas('sender',function($sender){
            $sender->where('id',auth('customer')->id());
        })->where('status','warehousing')
        ->when(request('date'),function($query){
            $query->whereBetween('created_at',explode(' - ',request('date')));
        })->orderBy('created_at','desc')->get();
        return view('web-views.users-profile.express.delivery.create', compact('expressProducts','payment_method'));
    }
    public function create_thai_khmer(Request $request)
    {
        auth('customer')->user()->notify()->where('model',ExpressProduct::class)->update([
            'seen' => 1,
        ]);

        $payment_method = PaymentMethod::latest('id')-> get();
        $expressProducts =  ExpressProduct:: whereHas('sender',function($sender){
            $sender->where('id',auth('customer')->id());
        })->where('status','warehousing')
        ->when(request('date'),function($query){
            $query->whereBetween('created_at',explode(' - ',request('date')));
        })->orderBy('created_at','desc')->get();
        return view('web-views.users-profile.express.delivery.create-thai-khmer', compact('expressProducts','payment_method'));
    }

    public function create_khmer_thai(Request $request)
    {
        $payment_method = PaymentMethod::latest('id')-> get();

        return view('web-views.users-profile.express.delivery.create-khmer-thai', compact('payment_method'));
    }


    public function store(Request $request)
    {
        return $this->updateOrCreate($request);
    }


    public function edit(Request $request,$id)
    {


    }

    public function edit_khmer_thai(Request $request,$id)
    {
        $express =  Express::findOrFail($id);
        if(!in_array($express->status,['pending','cancel'])){
            abort(403);
        }
        $payment_method = PaymentMethod::latest('id')-> get();
        $expressProducts = ExpressProduct::whereIn('id', $express->express_products->toArray())->orderBy('index')->get();
        return view('web-views.users-profile.express.delivery.edit-khmer-thai', compact('payment_method','express','expressProducts'));
    }


    public function edit_thai_khmer(Request $request,$id)
    {
        $express =  Express::findOrFail($id);
        if(!in_array($express->status,['pending','cancel'])){
            abort(403);
        }
        $payment_method = PaymentMethod::latest('id')-> get();

        $expressProducts =  ExpressProduct::
        whereHas('sender',function($sender){
            $sender->where('id',auth('customer')->id());
        })->
        where('status','warehousing')
        ->when(request('date'),function($query){
            $query->whereBetween('created_at',explode(' - ',request('date')));
        })->orderBy('created_at','desc')->get();
        if ($express) {
            $expressProducts = ExpressProduct::whereIn('id', array_merge($express->express_products->toArray(), $expressProducts->pluck('id')->toArray()))->latest('id')->get();
        }
        $expressProducts = ExpressProduct::whereIn('id', array_merge($express->express_products->toArray(), $expressProducts->pluck('id')->toArray()))->latest('id')->get();
            return view('web-views.users-profile.express.delivery.edit-thai-khmer', compact('payment_method','express','expressProducts'));
        }
    public function update(Request $request,$id)
    {

        $request->merge(['id' => $id]);
        return $this->updateOrCreate($request);

    }
    public function show($id)
    {

        $express =  Express::findOrFail($id);
        return view('web-views.users-profile.express.delivery.show', compact('express'));

    }

    public function destroy($id)
    {
        $express = Express::where('id',$id)->first();
        if(!in_array($express->status,['pending','cancel'])){
            abort(403);
        }
        ExpressProduct::whereIn('id',  $express->express_products->toArray())->update(['status' => 'warehousing']);
        $express->delivery()->delete();
        $express->delete();
    }

}
