<?php

namespace App\Http\Controllers\Web;

use App\CPU\Helpers;
use App\User;
use App\Http\Controllers\Controller;
class ExpressController extends Controller
{
    public function currency($amount)
    {
       return Helpers::currency_converter($amount);
    }

    public function searchuser()
    {
       return User::with('shippings')->when(request('search'),function($query){
            $query->where('name','like','%'.request('search').'%');
            $query->orWhere('f_name','like','%'.request('search').'%');
            $query->orWhere('l_name','like','%'.request('search').'%');
            $query->orWhere('refcode','like','%'.request('search').'%');
        }) ->latest('id')->paginate(Helpers::pagination_limit());
    }


}
