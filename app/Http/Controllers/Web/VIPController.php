<?php

namespace App\Http\Controllers\Web;

use App\User;
use Carbon\Carbon;
use App\CPU\Helpers;
use App\Model\Order;
use App\Model\Wishlist;
use App\CPU\ImageManager;
use App\CPU\OrderManager;
use App\Model\VIPSettings;
use App\CPU\CustomerManager;
use App\Model\PaymentMethod;
use App\Model\SupportTicket;
use Illuminate\Http\Request;
use App\Model\ShippingAddress;

use function App\CPU\check_refcode;
use function App\CPU\commission_level;
use function App\CPU\generate_refcode;
use function App\CPU\translate;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\BusinessSetting;
use App\Model\Express;
use App\Model\ExpressDelivery;
use App\Model\ExpressProduct;
use App\Model\VIPRequest;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class VIPController extends Controller
{

    public function balance()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            $history_balance = $customerDetail->balances()->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
            return view('web-views.users-profile.vip.account-balance', compact('customerDetail','history_balance'));
        } else {
            return redirect()->route('home');
        }
    }
    public function withdrawal()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){
                $vip = VIPSettings::first();
                $amount = request('amount');
                $balance = $customerDetail->balances->sum('balance') -  $customerDetail->withdrawals->sum('balance');
                if(request('amount')> $balance ){
                    Toastr::error(translate('request_unsuccessful'));
                }elseif(request('amount') <  $vip->can_withdrawal){
                    Toastr::error(translate('Withdrawal up to 100$'));
                    Toastr::error(translate('request_unsuccessful'));
                }else{
                    $withdrawal = $customerDetail->withdrawals->where('status','requesting')->first();
                    if($withdrawal){
                        $amount+=$withdrawal->balance;
                        $withdrawal->update([
                            'balance' =>$amount,
                            'created_at' => now(),
                        ]);
                    }else{
                        $customerDetail->withdrawals()->create([
                            'balance' =>$amount,
                            'status' => 'requesting',
                        ]);
                    }
                    Toastr::info(translate('request_successfully'));
                    return redirect()->back();
                }

            }
            $withdrawals = $customerDetail->withdrawals()->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
            return view('web-views.users-profile.account-withdrawal', compact('customerDetail','withdrawals'));
        } else {
            return redirect()->route('home');
        }
    }


    public function withdrawal_cancel($id)
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            $customerDetail->withdrawals->find($id)->delete();
            Toastr::info(translate('cancel_successfully'));
            return redirect()->back();
        }

        return redirect()->route('home');
    }



    public function refcode()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){
                $refcode = random_int(100000, 999999);
                if($customerDetail->refcode == null){
                    $refcode = substr(request('refcode',$refcode), 0, 6);
                    if(User::where('refcode',$refcode)->exists()){
                        return back()->withErrors(['refcode' => 'Referral code is exists.']);
                    }else{
                        $customerDetail->update([
                            'refcode' => $refcode,
                        ]);
                        Toastr::info(translate('updated_successfully'));
                    }
                }
            }
            $myrefs  = $customerDetail->myrefs()->orderBy('created_at','desc')->paginate(Helpers::pagination_limit());
            return view('web-views.users-profile.account-refcode', compact('customerDetail','myrefs'));
        } else {
            return redirect()->route('home');
        }
    }

    public function period()
    {
        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();

            if(request()->method() == 'POST'){
                if (request()->period) {
                    $period = explode(' - ',request()->period);
                    $start = $period[0];
                    $end =$period[1];

                    $customerDetail->update([
                        'period_start' =>  $start,
                        'period_end' =>  $end,
                    ]);
                    Toastr::info(translate('updated_successfully'));
                }else{
                    return back()->withErrors(['refcode' => 'Referral period is required.']);
                }
            }
            return view('web-views.users-profile.account-period', compact('customerDetail'));
        } else {
            return redirect()->route('home');
        }
    }
    public function become_vip()
    {
        $payment_method = PaymentMethod::latest('id')->get();

        if (auth('customer')->check()) {
            $customerDetail = User::where('id', auth('customer')->id())->first();
            $vip = VIPSettings::first()??new VIPSettings();
            if(request()->method() == 'POST'){
                $customerDetail->update([
                    'payment_method_id' => request()->payment_method_id,
                    //'bank_name' => PaymentMethod::find($request->payment_method_id)->name,
                    'holder_name' => request()->holder_name,
                    'account_no' => request()->account_no,
                    'id_card_image' => ImageManager::upload('nationid-card/', 'png', request()->file('id_card_image')),
                ]);

                $model = $customerDetail->vipRequests()->create([
                    'status' => 'requesting',
                    'total_fee' => $vip->fee,
                    'payment' => PaymentMethod::find(request('payment')),
                    'upload_payment' => ImageManager::upload('upload-payment/', 'png', request()->file('upload_payment')),
                ]);
                if($company_email =  BusinessSetting::where('type', 'company_email')->first()){

                    Mail::to($company_email->value)->send(new \App\Mail\BecomeVIP($model->id,'admin'));
                }
                Toastr::success(translate('request_successfully'));
            }

            return view('web-views.users-profile.vip.account-become-vip', compact('customerDetail','payment_method','vip'));
        } else {
            return redirect()->route('home');
        }
    }

}
