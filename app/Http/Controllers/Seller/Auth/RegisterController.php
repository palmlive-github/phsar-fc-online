<?php

namespace App\Http\Controllers\Seller\Auth;

use App\CPU\Helpers;
use App\CPU\ImageManager;
use App\Http\Controllers\Controller;
use App\Model\PaymentMethod;
use App\Model\PhoneOrEmailVerification;
use App\Model\Seller;
use App\Model\Shop;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use function App\CPU\translate;

class RegisterController extends Controller
{
    public function create()
    {
        $payment_methods = PaymentMethod::get();
        return view('seller-views.auth.register',compact('payment_methods'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|unique:sellers',
            'password' => 'required|min:8',
            'payment_method_id' => 'required',
            'holder_name' => 'required',
            'account_no' => 'required',
        ]);
        $full_number = $request['phone']['full'];
        $user = Seller::query()->where('email', $request->email)->orWhere('phone', $full_number)->first();
        if ($user) {
            if (!$user->is_phone_verified) {
                return redirect(route('seller.auth.check', [$user->id]));
            }
            Toastr::success(translate('You have already registered'));
            return redirect()->route('seller.auth.login');
        }
        DB::transaction(function ($r) use ($request, &$user) {
            $seller = new Seller();
            $seller->f_name = $request->f_name;
            $seller->l_name = $request->l_name;
            $seller->phone = $request['phone']['full'];
            $seller->email = $request->email;
            $seller->phone_verify_at = null;
            $seller->image = ImageManager::upload('seller/', 'png', $request->file('image'));
            $seller->passport_image = ImageManager::upload('seller/', 'png', $request->file('passport_image'));
            $seller->password = bcrypt($request->password);
            $seller->status = "approved";  // pending , suspended
            $seller->payment_method_id = $request->payment_method_id;
            $seller->holder_name = $request->holder_name;
            $seller->account_no = $request->account_no;
            $seller->save();

            $shop = new Shop();
            $shop->seller_id = $seller->id;
            $shop->name = $request->shop_name;
            $shop->address = $request->shop_address;
            $shop->contact = $request['phone']['full'];
            $shop->image = ImageManager::upload('shop/', 'png', $request->file('logo'));
            $shop->banner = ImageManager::upload('shop/banner/', 'png', $request->file('banner'));
            $shop->save();

            DB::table('seller_wallets')->insert([
                'seller_id' => $seller['id'],
                'withdrawn' => 0,
                'commission_given' => 0,
                'total_earning' => 0,
                'pending_withdraw' => 0,
                'delivery_charge_earned' => 0,
                'collected_cash' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $user = $seller;
        });
        if (!$user->is_phone_verified) {
            return redirect(route('seller.auth.check', [$user->id]));
        }
        Toastr::success(translate('Shop apply successfully!'));
        return redirect()->route('seller.auth.login');

    }

    public static function check($id)
    {
        $user = Seller::findOrFail($id);
        // check if already verified
        if ($user->is_phone_verified) {
            return redirect()->route('seller.auth.login');
        }
        $token = rand(100000, 999999);

        DB::table('phone_or_email_verifications')->insert([
            'phone_or_email' => $user->phone,
            'token' => $token,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        return view('seller-views.auth.verify', compact('user'));
    }

    public static function verify(Request $request)
    {
        Validator::make($request->all(), [
            'token' => 'required',
        ]);

        $user = Seller::findOrFail($request->id);
        $verify = PhoneOrEmailVerification::query()
            ->where(['phone_or_email' => $user->phone])->first();

        if ($verify) {
            try {
                $user->phone_verify_at = now();
                $user->save();
                auth('seller')->login($user);
                PhoneOrEmailVerification::query()->where(['phone_or_email' => $user->phone])->delete();
            } catch (\Exception $exception) {
                Toastr::info('Try again');
            }
            Toastr::success(translate('Verification Successfully Done'));
        } else {
            Toastr::error(translate('Verification code/ OTP mismatched'));
        }
        return redirect(route('seller.auth.login'));
    }
}
