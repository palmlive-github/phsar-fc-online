<?php

namespace App\Http\Controllers\Seller;

use App\Events\Chat;
use App\Http\Controllers\Controller;
use App\Model\Chatting;
use App\Model\Seller;
use App\Model\Shop;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChattingController extends Controller
{
    public function chat()
    {
        $shop_id = Shop::where('seller_id', auth('seller')->id())->first()->id;
        $chattings = collect();
        $chattings_user = collect();
        $last_chat = Chatting::where('shop_id', $shop_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if (isset($last_chat)) {
            //get messages of last user chatting
            $chattings = Chatting::join('users', 'users.id', '=', 'chattings.user_id')
                ->select('chattings.*', 'users.f_name', 'users.l_name', 'users.image')
                ->where('chattings.shop_id', $shop_id)
                ->where('user_id', $last_chat->user_id)
                ->get();

            $chattings_user = Chatting::join('users', 'users.id', '=', 'chattings.user_id')
                ->select('chattings.*', 'users.f_name', 'users.l_name', 'users.image')
                ->where('chattings.shop_id', $shop_id)
                ->orderBy('chattings.created_at', 'desc')
                ->get()
                ->unique('user_id')->map(function($row){
                    $row->unseen = Chatting::where('shop_id',$row->shop_id)->where('user_id', $row->user_id)->where('seen_by_seller',0)->count();
                    return $row;
                });


        }

        return view('seller-views.chatting.chat', compact('chattings', 'chattings_user', 'last_chat','shop_id'));
    }

    public function message_by_user(Request $request)
    {
        $shop_id = Shop::where('seller_id', auth('seller')->id())->first()->id;
        $chat = Chatting::join('users', 'users.id', '=', 'chattings.user_id')
            ->select('chattings.*', 'users.f_name', 'users.l_name', 'users.image')
            ->where('chattings.shop_id', $shop_id)
            ->where('chattings.user_id', $request->user_id)
            ->latest()
            ->paginate(20);
            Chatting::where('shop_id',$shop_id)->where('user_id', $request->user_id)->update(['seen_by_seller'=> 1]);
            event(new Chat([
                'shop_id' => $shop_id,
                'seller_id' => auth('seller')->id(),
                'user_id' => $request->user_id,
            ],'chat-seen-event'));

        return response()->json([
            'page' => $chat->currentPage(),
            'data' => array_values($chat->reverse()->toArray()),
        ]);
    }

    public function oldMessages(Request $request)
    {
        $shop_id = Shop::where('seller_id', auth('seller')->id())->first()->id;
        $chat = Chatting::join('users', 'users.id', '=', 'chattings.user_id')
            ->select('chattings.*', 'users.f_name', 'users.l_name', 'users.image')
            ->where('chattings.shop_id', $shop_id)
            ->where('chattings.user_id', $request->user_id)
            ->latest()
            ->paginate(20);
        return response()->json([
            'page' => $chat->currentPage(),
            'data' => array_values($chat->reverse()->toArray()),
        ]);
    }

    // Store massage
    public function seller_message_store(Request $request)
    {
        if ($request->message == '') {
            Toastr::warning('Type Something!');
            return response()->json('type something!');
        } else {
            $shop_id = Shop::where('seller_id', auth('seller')->id())->first()->id;

            $message = $request->message;
            $time = now();

          $chat = Chatting::create([
                'user_id'        => $request->user_id, //user_id == seller_id
                'seller_id'      => auth('seller')->id(),
                'shop_id'        => $shop_id,
                'message'        => $request->message,
                'sent_by_seller' => 1,
                'seen_by_seller' => 1,
                'created_at'     => now(),
            ]);


            if($chat->shop->image == 'def.png'){
                $chat->image = asset('storage/'.$chat->shop->image);
            } else {
                $chat->image = asset('storage/shop/'.$chat->shop->image);
            }
            event(new Chat([
                'sent_by_customer' => 0,
                'sent_by_seller' => 1,
                'shop_id' => $shop_id,
                'seller_id' => auth('seller')->id(),
                'user_id' => $request->user_id,
                'message' => $request->message,
                'image' =>  $chat->image,
                'created_at' => now(),
            ]));

            event(new Chat([
                'shop_id' => $shop_id,
                'seller_id' => auth('seller')->id(),
                'user_id' => $request->user_id,
            ],'chat-seen-event'));

            return response()->json(['message' => $message, 'time' => $time]);

        }
    }

}
