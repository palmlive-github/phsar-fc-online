<?php

namespace App\Http\Controllers\api\v1;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
    public function ImageUpload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:5120'],  // 5MB
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }
        if ($request->file('image') != null) {
            $imageName = Carbon::now()->toDateString() . "-" . uniqid() . ".png";
            if (!Storage::disk('public')->exists('uploads')) {
                Storage::disk('public')->makeDirectory('uploads');
            }
            Storage::disk('public')->put('uploads/' . $imageName, file_get_contents($request->file('image')));
        } else {
            $imageName = 'def.png';
        }
        return response([
            'image' => $imageName,
            'image_url' => asset('storage/uploads/' . $imageName)
        ]);
    }
}
