<?php

namespace App\Http\Controllers\api\v1;

use App\CPU\CartManager;
use App\CPU\Helpers;
use App\CPU\ImageManager;
use App\CPU\OrderManager;
use App\Http\Controllers\Controller;
use App\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function App\CPU\translate;

class OrderController extends Controller
{
    public function track_order(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        return response()->json(OrderManager::track_order($request['order_id']), 200);
    }

    public function place_order(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'shipping_method_id' => 'required',
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
//        }
        $unique_id = $request->user()->id . '-' . rand(000001, 999999) . '-' . time();
        $order_ids = [];
        $card_group_Ids = CartManager::get_cart_group_ids($request);
        if (count($card_group_Ids) == 0) {
            return response()->json([
                'message' => "Card is empty. cannot place order"
            ], 400);
        }
        foreach ($card_group_Ids as $group_id) {
            $data = [
                'payment_method' => 'cash_on_delivery',
                'order_status' => 'pending',
                'payment_status' => 'unpaid',
                'transaction_ref' => '',
                'order_group_id' => $unique_id,
                'cart_group_id' => $group_id,
                'request' => $request
            ];
            $order_id = OrderManager::generate_order($data);
            array_push($order_ids, $order_id);
        }
        CartManager::cart_clean($request);
        return response()->json(translate('order_placed_successfully'), 200);
    }

    public function place_order_api(Request $request)
    {
        $unique_id = $request->user()->id . '-' . rand(000001, 999999) . '-' . time();
        $order_ids = [];
        $card_group_Ids = CartManager::get_cart_group_ids($request);
        if (count($card_group_Ids) == 0) {
            return response()->json([
                'message' => "Card is empty. cannot place order"
            ], 400);
        }
        $user = $request->user();
        foreach ($card_group_Ids as $group_id) {
            $data = [
                'payment_method' => 'cash_on_delivery',
                'order_status' => 'pending',
                'payment_status' => 'unpaid',
                'transaction_ref' => '',
                'order_group_id' => $unique_id,
                'cart_group_id' => $group_id,
                'customer_id' => $user['id'],
                'request' => $request
            ];
            $order_id = OrderManager::generate_order_api($data);
            array_push($order_ids, $order_id);
        }

        CartManager::cart_clean($request);

        return response()->json([
            'order_id' => $order_ids[0],
            'message' => translate('order_placed_successfully')
        ], 200);
    }

    public function payment_image(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'payment_image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:5120'],  // 5MB
            'bank_name' => ['required']
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $order = Order::query()
            ->where(['id' => $id, 'customer_id' => auth()->id()])
            ->first();
        if (!$order) {
            return response()->json(['message' => translate('Order not Found')], 404);
        }
        $order->payment_image = ImageManager::update('order/', $order->payment_iamge, 'png', $request->file('payment_image'));
        $order->bank_name = $request->bank_name;
        $order->save();
        $order->payment_image = asset('storage/order/' . $order->payment_image);
        return response()->json([
            'message' => '',
            'data' => $order
        ], 200);
    }
}
