<?php

namespace App\Http\Controllers\api\v1;

use App\CPU\CustomerManager;
use App\CPU\Helpers;
use App\CPU\ImageManager;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\ShippingAddress;
use App\Model\SupportTicket;
use App\Model\SupportTicketConv;
use App\Model\VIPSettings;
use App\Model\Wishlist;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use function App\CPU\translate;

class CustomerController extends Controller
{
    public function info(Request $request)
    {
        $data = [
            'status' => true,
            'data' => collect($request->user())->merge([
                'image' => $request->user()->image == 'def.png' ? asset('assets/front-end/img/image-place-holder.png') : asset('storage/profile/' . $request->user()->image),
                'id_card_image' => $request->user()->id_card_image == 'def.png' ? asset('assets/front-end/img/image-place-holder.png') : asset('storage/nationid-card/' . $request->user()->id_card_image),
            ])
        ];
        return response()->json($data, 200);
    }
    public function becomeVIP(Request $request)
    {
        $data = [
            'status' => true,
            'data' =>  $request->user()->vipRequests->map(function ($row) {
                $row->upload_payment = asset('storage/upload-payment/' . $row->upload_payment);
                $row->status = translate($row->status);
                $row->total_fee = \App\CPU\Helpers::currency_converter($row->total_fee);
                $row->payment = collect($row->payment)->merge(
                    [
                        'image' => asset('storage/vip-payment-image/' . $row->payment['image'])
                    ],
                );

                return $row;
            })
        ];
        return response()->json($data, 200);
    }

    public function becomeVIPRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required',
            'holder_name' => 'required',
            'account_no' => 'required',
            'id_card_image' => 'required',
            'payment_via' => 'required',
            'upload_payment' => 'required',
        ], [], [
            'payment_method_id' => 'Payment Method',
            'account_no' => 'Account number',
            'id_card_image' => 'Nation ID card',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => Helpers::error_processor($validator)
            ], 403);
        }
        $vip = VIPSettings::first();

        $request->user()->update([
            'payment_method_id' => $request->payment_method_id,
            'holder_name' => $request->holder_name,
            'account_no' => $request->account_no,
            'id_card_image' => ImageManager::upload('nationid-card/', 'png', $request->file('id_card_image')),
        ]);

        $request->user()->vipRequests()->create([
            'status' => 'requesting',
            'total_fee' => $vip->fee,
            'payment' => $vip->payments->get(request('payment_via')),
            'upload_payment' => ImageManager::upload('upload-payment/', 'png', request()->file('upload_payment')),
        ]);

        return response()->json([
            'status' => true,
            'message' => translate('request_successfully'),
        ], 200);
    }

    public function bank(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required',
            'holder_name' => 'required',
            'account_no' => 'required',
        ], [], [
            'payment_method_id' => 'Payment Method',
            'account_no' => 'Account number',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => Helpers::error_processor($validator)
            ], 403);
        }


        $request->user()->update([
            'payment_method_id' => $request->payment_method_id,
            'holder_name' => $request->holder_name,
            'account_no' => $request->account_no,
        ]);
        return response()->json([
            'status' => true,
            'message' => translate('update_successfully'),
        ], 200);
    }


    public function balance(Request $request)
    {
        if ($request->vip == 0) {

            $earning =  $request->user()->balances->sum('balance');
            $withdrawal =  $request->user()->withdrawals->sum('balance');
            $data = [
                'status' => true,
                'data' => [
                    'balance' => \App\CPU\Helpers::currency_converter($earning - $withdrawal),
                    'earning_history' => $request->user()->balances->map(function ($row) {
                        return [
                            'balance' => \App\CPU\Helpers::currency_converter($row->balance),
                            'type' => collect(explode('\\', $row->model))->last(),
                            'commission' => $row->commission . '%',
                            'created_at' => $row->created_at->translatedFormat('d-M-Y'),
                        ];
                    }),
                    'withdrawal_history' => $request->user()->withdrawals->map(function ($row) {
                        return [
                            'balance' =>  \App\CPU\Helpers::currency_converter($row->balance),
                            'status' => translate($row->status),
                            'created_at' => $row->created_at->translatedFormat('d-M-Y'),
                        ];
                    }),
                ]
            ];
            return response()->json($data, 200);
        }

        $data = [
            'status' => false,
            'message' => translate('You are not member of VIP'),
        ];
        return response()->json($data, 403);
    }

    public function withdrawal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => Helpers::error_processor($validator)
            ], 403);
        }


        $amount = $request->amount;
        $balance = $request->user()->balances->sum('balance') - $request->user()->withdrawals->sum('balance');
        if($amount > $balance){
            return response()->json([
                'status' => false,
                'meassage' => translate('request_unsuccessful'),
            ], 403);
        }
        $withdrawal = $request->user()->withdrawals->where('status','requesting')->first();
        if($withdrawal){
            $amount+=$withdrawal->balance;
            $withdrawal->update([
                'balance' =>$amount,
                'created_at' => now(),
            ]);
        }else{
            $request->user()->withdrawals()->create([
                'balance' => $amount,
                'status' => 'requesting',
            ]);
        }
        return response()->json([
            'status' => true,
            'message' => translate('request_successfully'),
        ], 200);
    }
    public function withdrawalCancel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => Helpers::error_processor($validator)
            ], 403);
        }
        if($withdrawal =  $request->user()->withdrawals->find($request->id)){
            if($withdrawal->delete()){
                return response()->json([
                    'status' => true,
                    'meassage' => translate('cancel_successful'),
                ], 200);
            }

        }
        return response()->json([
            'status' => false,
            'meassage' => translate('cancel_unsuccessful'),
        ], 200);
    }
    public function create_support_ticket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'type' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $request['customer_id'] = $request->user()->id;
        $request['priority'] = 'low';
        $request['status'] = 'pending';

        try {
            CustomerManager::create_support_ticket($request);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'code' => 'failed',
                    'message' => translate('Something went wrong'),
                ],
            ], 422);
        }
        return response()->json(['message' => translate('Support ticket created successfully.')], 200);
    }

    public function reply_support_ticket(Request $request, $ticket_id)
    {
        $support = new SupportTicketConv();
        $support->support_ticket_id = $ticket_id;
        $support->admin_id = 1;
        $support->customer_message = $request['message'];
        $support->save();
        return response()->json(['message' => translate('Support ticket reply sent.')], 200);
    }

    public function get_support_tickets(Request $request)
    {
        return response()->json(SupportTicket::where('customer_id', $request->user()->id)->get(), 200);
    }

    public function get_support_ticket_conv($ticket_id)
    {
        return response()->json(SupportTicketConv::where('support_ticket_id', $ticket_id)->get(), 200);
    }

    public function add_to_wishlist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $wishlist = Wishlist::where('customer_id', $request->user()->id)->where('product_id', $request->product_id)->first();

        if (empty($wishlist)) {
            $wishlist = new Wishlist;
            $wishlist->customer_id = $request->user()->id;
            $wishlist->product_id = $request->product_id;
            $wishlist->save();
            return response()->json(['message' => translate('successfully added!')], 200);
        }

        return response()->json(['message' => translate('Already in your wishlist')], 200);
    }

    public function remove_from_wishlist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $wishlist = Wishlist::where('customer_id', $request->user()->id)->where('product_id', $request->product_id)->first();

        if (!empty($wishlist)) {
            Wishlist::where(['customer_id' => $request->user()->id, 'product_id' => $request->product_id])->delete();
            return response()->json(['message' => translate('successfully removed!')], 200);
        }
        return response()->json(['message' => translate('No such data found!')], 404);
    }

    public function wish_list(Request $request)
    {
        return response()->json(Wishlist::whereHas('product')->where('customer_id', $request->user()->id)->get(), 200);
    }

    public function address_list(Request $request)
    {
        $data = [
            'status' => true,
            'data' => $request->user()->shippings,
        ];
        return response()->json($data, 200);
    }

    public function add_new_address(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contact_person_name' => 'required',
            'address_type' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'errors' => Helpers::error_processor($validator)], 403);
        }

        $address = [
            'customer_id' => $request->user()->id,
            'contact_person_name' => $request->name,
            'address_type' => $request->address_type,
            'address' => $request->address,
            'city' => $request->city,
            'zip' => $request->zip,
            'phone' => $request->phone,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        DB::table('shipping_addresses')->insert($address);
        return response()->json([
            'status'    => true,
            'message' => translate('successfully added!')], 200);
    }

    public function delete_address(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'errors' => Helpers::error_processor($validator)], 403);
        }

        if (DB::table('shipping_addresses')->where(['id' => $request['address_id'], 'customer_id' => $request->user()->id])->first()) {
            DB::table('shipping_addresses')->where(['id' => $request['address_id'], 'customer_id' => $request->user()->id])->delete();
            return response()->json([
                'status'    => true,
                'message' => translate('successfully removed!')], 200);
        }
        return response()->json([
            'status'    => false,
            'message' => translate('No such data found!')], 404);
    }

    public function get_order_list(Request $request)
    {
        $orders = Order::where(['customer_id' => $request->user()->id])->get();
        $orders->map(function ($data) {
            $data['shipping_address_data'] = json_decode($data['shipping_address_data']);
            return $data;
        });
        return response()->json($orders, 200);
    }

    public function get_order_details(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $details = OrderDetail::where(['order_id' => $request['order_id']])->get();
        $details->map(function ($query) {
            $query['variation'] = json_decode($query['variation'], true);
            $query['product_details'] = Helpers::product_data_formatting(json_decode($query['product_details'], true));
            return $query;
        });
        return response()->json($details, 200);
    }

    public function update_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',
        ], [
            'f_name.required' => translate('First name is required!'),
            'l_name.required' => translate('Last name is required!'),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => Helpers::error_processor($validator)], 403);
        }

        if ($request->has('image')) {
            $imageName = ImageManager::update('profile/', $request->user()->image, 'png', $request->file('image'));
        } else {
            $imageName = $request->user()->image;
        }

        if ($request['password'] != null && strlen($request['password']) > 5) {
            $pass = bcrypt($request['password']);
        } else {
            $pass = $request->user()->password;
        }

        $userDetails = [
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'phone' => $request->phone,
            'image' => $imageName,
            'password' => $pass,
            'updated_at' => now(),
        ];

        User::where(['id' => $request->user()->id])->update($userDetails);

        return response()->json([
                'status' => true,
                'message' => translate('successfully updated!')], 200);
    }

    public function update_cm_firebase_token(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cm_firebase_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        DB::table('users')->where('id', $request->user()->id)->update([
            'cm_firebase_token' => $request['cm_firebase_token'],
        ]);

        return response()->json(['message' => translate('successfully updated!')], 200);
    }
}
