<?php

namespace App\Http\Controllers\api\v1\auth;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use function App\CPU\translate;

class ForgotPassword extends Controller
{
    public function reset_password_request(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

//        $verification_by = Helpers::get_business_settings('forgot_password_verification');
//        DB::table('password_resets')->where('identity', 'like', "%{$request['identity']}%")->delete();
//
//        if ($verification_by == 'email') {
//            $customer = User::Where(['email' => $request['identity']])->first();
//            if (isset($customer)) {
//                $token = Str::random(120);
//                DB::table('password_resets')->insert([
//                    'identity' => $customer['email'],
//                    'token' => $token,
//                    'created_at' => now(),
//                ]);
//                $reset_url = url('/') . '/customer/auth/reset-password?token=' . $token;
//                Mail::to($customer['email'])->send(new \App\Mail\PasswordResetMail($reset_url));
//                return response()->json(['message' => translate('Email sent successfully.')], 200);
//            }
//        } elseif ($verification_by == 'phone') {
//            $customer = User::where('phone', 'like', "%{$request['identity']}%")->first();
//            if (isset($customer)) {
//                $token = rand(1000, 9999);
//                DB::table('password_resets')->insert([
//                    'identity' => $customer['phone'],
//                    'token' => $token,
//                    'created_at' => now(),
//                ]);
//                SMS_module::send($customer->phone, $token);
//                return response()->json(['message' => translate('otp sent successfully.')], 200);
//            }
//        }
        $customer = User::where('phone', $request['phone'])->first();
        if (isset($customer)) {
            $token = Str::random(120);
            DB::table('password_resets')->insert([
                'identity' => $customer['phone'],
                'token' => $token,
                'created_at' => now(),
            ]);
//                SMS_module::send($customer->phone, $token);
            return response()->json([
                'message' => translate('reset password request success'),
                'token' => $token
            ], 200);
        }
        return response()->json(['errors' => [
            ['code' => 'not-found', 'message' => translate('user not found!')]
        ]], 404);
    }

    public function otp_verification_submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'identity' => 'required',
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $id = session('forgot_password_identity');
        $data = DB::table('password_resets')->where(['token' => $request['otp']])
            ->where('identity', 'like', "%{$id}%")
            ->first();

        if (isset($data)) {
            return response()->json(['message' => translate('otp verified.')], 200);
        }

        return response()->json(['errors' => [
            ['code' => 'not-found', 'message' => translate('invalid OTP')]
        ]], 404);
    }

    public function reset_password_submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'token' => 'required',
            'password' => 'required|same:confirm_password|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $data = DB::table('password_resets')
            ->where('identity', $request['phone'])
            ->where(['token' => $request['token']])->first();

        if (isset($data)) {
            DB::table('users')->where('phone', $data->identity)
                ->update([
                    'password' => bcrypt(str_replace(' ', '', $request['password']))
                ]);

            DB::table('password_resets')
                ->where('identity', 'like', "%{$request['identity']}%")
                ->where(['token' => $request['otp']])->delete();

            return response()->json(['message' => translate('Password changed successfully.')], 200);
        }
        return response()->json(['errors' => [
            ['code' => 'invalid', 'message' => translate('Invalid token.')]
        ]], 400);
    }
}
