<?php

namespace App\Http\Controllers\api\v1\auth;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use App\Model\VIPSettings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use function App\CPU\check_refcode;
use function App\CPU\generate_refcode;
use function App\CPU\translate;


class PassportAuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users',
            'password' => 'required|min:8',
            'refcode_to' => 'required',
        ], [
            'f_name.required' => 'The first name field is required.',
            'l_name.required' => 'The last name field is required.',
            'refcode_to' => 'The Referral code field is required.'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => Helpers::error_processor($validator)
            ], 403);
        }
        $temporary_token = Str::random(40);

        $refcode_to = substr($request->refcode_to, 0, 6);
        $vip = VIPSettings::first();
        if($refcode_to == $vip->base_refcode){
            $refcode_to = '';
        }else{
            $refcode_user = check_refcode($refcode_to);
            if(!$refcode_user){
                return response()->json([
                    'status' => false,
                    'errors' => [['code' => 'refcode_to', 'message'=> translate('Referral code not found') ]]
                ], 403);
            }
            if($refcode_user->is_vip == 0){
                return response()->json([
                    'status' => false,
                    'errors' => [['code' => 'refcode_to', 'message'=> translate('Referral code not VIP') ]]
                ], 403);
            }

            if($refcode_user->is_active == 0){
                return response()->json([
                    'status' => false,
                    'errors' => [['code' => 'refcode_to', 'message'=> translate('Referral code is Disabled') ]]
                ], 403);
            }
        }
        $user = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'is_active' => 1,
            'password' => bcrypt($request->password),
            'temporary_token' => $temporary_token,
            'refcode_to' => $refcode_to,
            'refcode' => generate_refcode(),
        ]);

        $phone_verification = Helpers::get_business_settings('phone_verification');
        $email_verification = Helpers::get_business_settings('email_verification');
        if ($phone_verification && !$user->is_phone_verified) {
            return response()->json([
                'status' => true,
                'token' => $temporary_token], 200);
        }
        if ($email_verification && !$user->is_email_verified) {
            return response()->json([
                 'status' => true,
                'token' => $temporary_token], 200);
        }
        $token = $user->createToken('LaravelAuthApp')->accessToken;
        return response()->json([
            'status' => true,
            'token' => $token], 200);
    }

    public function verifyToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => ['string', 'required'],
        ]);

        if ($validator->fails()) {
            return response()->json([ 'status' => false,'errors' => Helpers::error_processor($validator)], 403);
        }
        $user = User::where(['temporary_token' => $request->temporary_token])->first();
        if ($user) {
            $user->update([
                'is_phone_verified' => 1,
                'is_email_verified' => 1,
                'temporary_token' => null
            ]);
            auth()->login($user);
            $token = $user->createToken('LaravelAuthApp')->accessToken;
            return response()->json([ 'status' => true,'token' => $token], 200);
        }
        return response()->json([ 'status' => false,'message' => translate('Invalid Temporary Token')], 400);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }
        $medium = 'phone';
        $user_id = $request['email'];
        if (filter_var($user_id, FILTER_VALIDATE_EMAIL)) {
            $medium = 'email';
        } else {
            // $count = strlen(preg_replace("/[^\d]/", "", $user_id));
            // if ($count >= 9 && $count <= 15) {
            //     $errors = [];
            //     array_push($errors, ['code' => 'email', 'message' => translate('Invalid email address or phone number')]);
            //     return response()->json([
            //         'errors' => $errors
            //     ], 403);
            // }
        }

        $data = [
            $medium => $user_id,
            'password' => $request->password
        ];

        $user = User::where([$medium => $user_id])->first();

        if (isset($user) && $user->is_active && auth()->attempt($data)) {
            $user->temporary_token = Str::random(40);
            $user->save();

            $phone_verification = Helpers::get_business_settings('phone_verification');
            $email_verification = Helpers::get_business_settings('email_verification');
            if ($phone_verification && !$user->is_phone_verified) {
                return response()->json([
                     'status' => true,
                     'token' => $user->temporary_token], 200);
            }
            if ($email_verification && !$user->is_email_verified) {
                return response()->json([
                     'status' => true,
                     'token' => $user->temporary_token], 200);
            }

            $token = request()->user()->createToken('LaravelAuthApp')->accessToken;
            return response()->json([
                 'status' => true,
                 'token' => $token], 200);
        } else {
            $errors = [];
            array_push($errors, [ 'code' => 'auth-001', 'message' => translate('Customer_not_found_or_Account_has_been_suspended')]);
            return response()->json([
                'status' => false,
                'errors' => $errors
            ], 401);
        }
    }
}
