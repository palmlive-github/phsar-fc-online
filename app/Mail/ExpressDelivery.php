<?php

namespace App\Mail;

use App\Model\Express;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExpressDelivery extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $express_id;
    protected $for;

    public function __construct($express_id,$for = 'user')
    {
        $this->express_id = $express_id;
        $this->for = $for;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $express = Express::find($this->express_id);
        $for = $this->for;
        return $this->view('email-templates.express-delivery',compact('express','for'));
    }
}
