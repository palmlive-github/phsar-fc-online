<?php

namespace App\Mail;

use App\Model\VIPRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BecomeVIP extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $vip_request = VIPRequest::find($this->id);
        return $this->view('email-templates.becomevip',compact('vip_request'));
    }
}
