<?php

namespace App;

use App\Model\Order;
use App\Model\PaymentMethod;
use App\Model\Wishlist;
use App\Model\ShippingAddress;
use App\Model\UserBalance;
use App\Model\UserNotify;
use App\Model\UserWithdrawal;
use App\Model\VIPRequest;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Schema;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name', 'l_name', 'name', 'email', 'password', 'phone', 'image', 'login_medium','is_active','social_id','is_phone_verified','temporary_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }
    public function createdBy()
    {
        return $this->belongsTo(self::class, 'created_by');
    }

    public function wish_list()
    {
        return $this->hasMany(Wishlist::class, 'customer_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function shipping()
    {
        return $this->belongsTo(ShippingAddress::class, 'shipping_address');
    }
    public function shippings()
    {
        return $this->hasMany(ShippingAddress::class, 'customer_id');
    }
    public function notify()
    {
        return $this->hasMany(UserNotify::class, 'user_id');
    }
    public function getAddresssAttribute($key)
    {
        return $this->shippings;
    }


    public function myrefs()
    {
        return $this->hasMany(self::class,'refcode_to','refcode');
    }
    public function refuser()
    {
        return $this->hasOne(self::class,'refcode','refcode_to')->with('refuser');
    }
    public function balances()
    {
        return $this->hasMany(UserBalance::class);
    }
    public function withdrawals()
    {
        return $this->hasMany(UserWithdrawal::class);
    }

    public function paymentmethod()
    {
        return $this->belongsTo(PaymentMethod::class,'payment_method_id');
    }

    public function vipRequests()
    {
        return $this->hasMany(VIPRequest::class);
    }
}
