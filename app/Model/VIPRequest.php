<?php

namespace App\Model;

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class VIPRequest extends Model
{
    protected $casts = [
        'payment' => 'collection',
    ];
    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
