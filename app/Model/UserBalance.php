<?php

namespace App\Model;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'model_id');
    }
    public function express()
    {
        return $this->belongsTo(Order::class,'model_id');
    }
}
