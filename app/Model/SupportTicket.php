<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    public function conversations()
    {
        return $this->hasMany(SupportTicketConv::class);
    }
}
