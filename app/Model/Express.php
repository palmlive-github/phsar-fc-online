<?php

namespace App\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\CPU\Helpers;
use App\User;
use Illuminate\Support\Facades\App;
class Express extends Model
{
    protected $casts = [
        'express_products' => 'collection',
        'payment' => 'collection',
    ];
    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('translate', function (Builder $builder) {
            $builder->with(['translations' => function ($query) {
                if (strpos(url()->current(), '/api')){
                    return $query->where('locale', App::getLocale());
                }else{
                    return $query->where('locale', Helpers::default_lang());
                }
            }]);
        });
    }

    public function translations()
    {
        return $this->morphMany('App\Model\Translation', 'translationable');
    }

    public function refuser()
    {
        return $this->belongsTo(User::class,'refcode_to','refcode');
    }
    public function delivery()
    {
        return $this->belongsTo(ExpressDelivery::class,'express_delivery_id');
    }

    public function getProductsAttribute()
    {
        return ExpressProduct::whereIn('id',$this->express_products)->get();
    }
}
