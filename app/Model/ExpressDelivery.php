<?php

namespace App\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\CPU\Helpers;
use Illuminate\Support\Facades\App;
use App\User;
class ExpressDelivery extends Model
{
    protected $casts = [
        'sender_info' => 'collection',
        'receiver_info' => 'collection',
    ];
    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('translate', function (Builder $builder) {
            $builder->with(['translations' => function ($query) {
                if (strpos(url()->current(), '/api')){
                    return $query->where('locale', App::getLocale());
                }else{
                    return $query->where('locale', Helpers::default_lang());
                }
            }]);
        });
    }

    public function translations()
    {
        return $this->morphMany('App\Model\Translation', 'translationable');
    }
    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    public function express()
    {
        return $this->hasOne(Express::class,'express_delivery_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }
}
