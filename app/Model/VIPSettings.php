<?php

namespace App\Model;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class VIPSettings extends Model
{
    protected $casts = [
        'payments' => 'collection'
    ];
    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }

}
