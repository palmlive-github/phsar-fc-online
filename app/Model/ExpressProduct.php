<?php

namespace App\Model;
use App\User;
use App\CPU\Helpers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ExpressProduct extends Model
{
    protected $casts = [
        'product' => 'collection',
    ];
    public function __construct(array $attributes = [])
    {
        $this->fillable = Schema::getColumnListing($this->getTable());
        parent::__construct($attributes);
    }
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('translate', function (Builder $builder) {
            $builder->with(['translations' => function ($query) {
                if (strpos(url()->current(), '/api')){
                    return $query->where('locale', App::getLocale());
                }else{
                    return $query->where('locale', Helpers::default_lang());
                }
            }]);
        });
    }

    public function translations()
    {
        return $this->morphMany('App\Model\Translation', 'translationable');
    }
    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
}
