@extends('layouts.front-end.app')
@section('title', \App\CPU\translate('Forgot Password'))
@push('css_or_js')
    <style>
        .text-primary {
            color: {{$web_config['primary_color']}}                !important;
        }

        .iti--allow-dropdown {
            width: 100%;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
@endpush

@section('content')
    @php($verification_by=\App\CPU\Helpers::get_business_settings('forgot_password_verification'))
    <!-- Page Content-->
    <div class="container py-4 py-lg-5 my-4">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10">
                <h2 class="h3 mb-4">{{\App\CPU\translate('Forgot your password')}}?</h2>
                <p class="font-size-md">{{\App\CPU\translate('Change your password in three easy steps. This helps to keep your new password secure')}}
                    .</p>
                @if($verification_by=='email')
                    <ol class="list-unstyled font-size-md">
                        <li><span
                                class="text-primary mr-2">{{\App\CPU\translate('1')}}.</span>{{\App\CPU\translate('Fill in your email address below')}}
                            .
                        </li>
                        <li><span
                                class="text-primary mr-2">{{\App\CPU\translate('2')}}.</span>{{\App\CPU\translate('We will email you a temporary code')}}
                            .
                        </li>
                        <li><span
                                class="text-primary mr-2">{{\App\CPU\translate('3')}}.</span>{{\App\CPU\translate('Use the code to change your password on our secure website')}}
                            .
                        </li>
                    </ol>
                    <div class="card py-2 mt-4">
                        <form class="card-body needs-validation" action="{{route('customer.auth.forgot-password')}}"
                              method="post">
                            @csrf
                            <div class="form-group">
                                <label for="recover-email">{{\App\CPU\translate('Enter your email address')}}</label>
                                <input class="form-control" type="email" name="identity" id="recover-email" required>
                                <div
                                    class="invalid-feedback">{{\App\CPU\translate('Please provide valid email address')}}
                                    .
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{\App\CPU\translate('Get new password')}}</button>
                        </form>
                    </div>
                @else
                    <div class="card py-2 mt-4">
                        <form class="card-body needs-validation" action="{{route('customer.auth.forgot-password')}}"
                              method="post">
                            @csrf
                            <div class="form-group">
                                <label for="recover-email">{{\App\CPU\translate('Enter your phone number')}}</label>
                                <div class="mb-4">
                                    <input class="form-control" type="phone" name="phone[main]" id="recover-phone"
                                    >
                                </div>
                                <div
                                    class="invalid-feedback">{{\App\CPU\translate('Please provide valid phone number')}}
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{\App\CPU\translate('proceed')}}</button>
                        </form>
                    </div>
                @endif

            </div>
        </div>
    </div>
    </div>
@endsection

@push('script')
    @if($verification_by=='phone')
    <script>
        const phoneInputField = document.querySelector("#recover-phone");
        const phoneInput = window.intlTelInput(phoneInputField, {
            separateDialCode: true,
            hiddenInput: "full",
            initialCountry: "auto",
            geoIpLookup: function(success) {
                success('KH')
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
        });
    </script>
    @endif
{{--    <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>--}}
    {{--    <script>--}}

    {{--        const firebaseConfig = {--}}
    {{--            apiKey: "{{ env('FIREBASE_apiKey') }}",--}}
    {{--            authDomain: "{{ env('FIREBASE_authDomain') }}",--}}
    {{--            projectId: "{{ env('FIREBASE_projectId') }}",--}}
    {{--            storageBucket: "{{ env('FIREBASE_storageBucket') }}",--}}
    {{--            messagingSenderId: "{{ env('FIREBASE_messagingSenderId') }}",--}}
    {{--            appId: "{{ env('FIREBASE_appId') }}"--}}
    {{--        };--}}

    {{--        firebase.initializeApp(firebaseConfig);--}}
    {{--    </script>--}}
    {{--    <script>--}}
    {{--        window.onload = function () {--}}
    {{--            render();--}}
    {{--        };--}}

    {{--        function render() {--}}
    {{--            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');--}}
    {{--            recaptchaVerifier.render();--}}
    {{--        }--}}

    {{--        function checkUserExist() {--}}
    {{--            const phoneNumber = $('#recover-phone').val()--}}
    {{--            if (phoneNumber === '') {--}}
    {{--                $("#firebase-error").text('Please enter Phone Number');--}}
    {{--                $("#firebase-error").show();--}}
    {{--            }--}}
    {{--            console.log('phone', phoneNumber)--}}
    {{--            $.ajaxSetup({--}}
    {{--                headers: {--}}
    {{--                    'X-CSRF-TOKEN': "{{ csrf_token() }}"--}}
    {{--                }--}}
    {{--            });--}}
    {{--            $.ajax({--}}
    {{--                type: "POST",--}}
    {{--                url: "{{ route('customer.auth.check-user-exist') }}",--}}
    {{--                data: {--}}
    {{--                    method: "POST",--}}
    {{--                    phone: phoneNumber--}}
    {{--                },--}}
    {{--                dataType: 'json',--}}
    {{--                success: function ({exist}) {--}}
    {{--                    if (exist) {--}}
    {{--                        phoneSendAuth()--}}
    {{--                    } else {--}}
    {{--                        $("#firebase-error").text('Phone number is not exist');--}}
    {{--                        $("#firebase-error").show();--}}
    {{--                    }--}}
    {{--                },--}}
    {{--                error: function (data) {--}}
    {{--                    console.log('error', data);--}}
    {{--                }--}}
    {{--            });--}}
    {{--        }--}}

    {{--        function phoneSendAuth() {--}}
    {{--            const phoneNumber = phoneInput.getNumber();--}}
    {{--            firebase.auth().signInWithPhoneNumber(phoneNumber, window.recaptchaVerifier).then(function (confirmationResult) {--}}
    {{--                window.confirmationResult = confirmationResult;--}}
    {{--                coderesult = confirmationResult;--}}
    {{--                console.log('result', coderesult);--}}
    {{--                $("#firebase-success").text("Message Sent Successfully.");--}}
    {{--                $("#firebase-success").show();--}}
    {{--            }).catch(function (error) {--}}
    {{--                $("#firebase-error").text(error.message);--}}
    {{--                $("#firebase-error").show();--}}
    {{--                console.log('error', error.message)--}}
    {{--            });--}}
    {{--        }--}}

    {{--        function codeverify() {--}}
    {{--            var code = $("#otp_number").val();--}}
    {{--            if (code === '') {--}}
    {{--                $("#firebase-error").text('Please enter OTP Code');--}}
    {{--                $("#firebase-error").show();--}}
    {{--            } else {--}}
    {{--                coderesult.confirm(code).then(function (result) {--}}
    {{--                    var user = result.user;--}}
    {{--                    console.log(user);--}}
    {{--                    $("#firebase-success").text("OTP Verified");--}}
    {{--                    $("#firebase-success").show();--}}
    {{--                    $('#apply').prop('disabled', false)--}}
    {{--                }).catch(function (error) {--}}
    {{--                    $('#apply').prop('disabled', true)--}}
    {{--                    $("#firebase-error").text(error.message);--}}
    {{--                    $("#firebase-error").show();--}}
    {{--                    console.log(error.message)--}}
    {{--                });--}}
    {{--            }--}}
    {{--        }--}}
    {{--    </script>--}}
@endpush
