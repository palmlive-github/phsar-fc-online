@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('User Withdrawal'))

@push('css_or_js')

@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{\App\CPU\translate('Dashboard')}}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{\App\CPU\translate('User Withdrawal')}}</li>
            </ol>
        </nav>



        <div class="row" style="margin-top: 20px" id="cate-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row flex-between justify-content-between align-items-center flex-grow-1">
                            <div>
                                <h5>{{ \App\CPU\translate('user_withdrawal_table')}} <span style="color: red;">({{ $user_withdrawals->total() }})</span></h5>
                            </div>
                            <div style="width: 30vw;">
                                <!-- Search -->
                                <form action="{{ url()->current() }}" method="GET">
                                    <div class="input-group input-group-merge input-group-flush">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tio-search"></i>
                                            </div>
                                        </div>
                                        <input id="" type="search" name="search" class="form-control"
                                            placeholder="{{\App\CPU\translate('user name')}}, {{\App\CPU\translate('referral code')}}" value="{{ request('search') }}">
                                        <button type="submit" class="btn btn-primary">{{\App\CPU\translate('search')}}</button>
                                    </div>
                                </form>
                                <!-- End Search -->
                            </div>




                        </div>
                    </div>
                    <div class="card-body" style="padding: 0">
                        <div class="table-responsive">
                            <table style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};"
                                class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table">
                                <thead class="thead-light">
                                <tr>
                                    <th style="width: 100px">{{ \App\CPU\translate('ID')}}</th>
                                    <th>{{ \App\CPU\translate('name')}}</th>
                                    <th>{{ \App\CPU\translate('Bank info')}}</th>
                                    <th>{{ \App\CPU\translate('Request Date')}}</th>
                                    <th>{{ \App\CPU\translate('Amount')}}</th>
                                    <th>{{ \App\CPU\translate('Image')}}</th>
                                    <th class="text-center" style="width:15%;">{{ \App\CPU\translate('action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user_withdrawals as $key=>$row)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td>{{$row->user->f_name }} {{$row->user->l_name }} ({{ $row->user->refcode }})</td>
                                        <td>
                                            {{ \App\CPU\translate('Bank Name')}} : {{ $row->user->paymentmethod->name }}
                                            <br>
                                            {{ \App\CPU\translate('Account Name')}} : {{ $row->user->holder_name }}
                                            <br>
                                            {{ \App\CPU\translate('Account Number')}} : {{ $row->user->account_no }}
                                        </td>
                                        <td>
                                            {{ $row->created_at->translatedFormat('d-m-Y') }}
                                        </td>
                                        <td>
                                            {{ \App\CPU\Helpers::currency_converter($row->balance)  }}
                                        </td>
                                        <td>
                                            <img width="50px"
                                            onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                            src="{{$row->image}}"
                                        >
                                        </td>
                                        <td>
                                                <a href="{{ route('admin.user-withdrawal.edit', $row->id)  }}" class="btn btn-primary btn-sm edit" style="cursor: pointer;">
                                                    <i class="tio-edit"></i>{{ \App\CPU\translate('Edit') }}
                                                </a>
                                          
                                                <a class="btn btn-danger btn-sm delete" style="cursor: pointer;"
                                                    id="{{ $row['id'] }}">
                                                    <i class="tio-add-to-trash"></i>{{ \App\CPU\translate('Delete') }}
                                                </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-footer">
                        {{$user_withdrawals->links()}}
                    </div>
                    @if(count($user_withdrawals)==0)
                        <div class="text-center p-4">
                            <img class="mb-3" src="{{asset('assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                            <p class="mb-0">{{\App\CPU\translate('no_data_found')}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    <script>


        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>

    <script>
        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            Swal.fire({
                title: '{{\App\CPU\translate('Are_you_sure')}}?',
                text: "{{\App\CPU\translate('You_will_not_be_able_to_revert_this')}}!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{\App\CPU\translate('Yes')}}, {{\App\CPU\translate('delete_it')}}!'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{route('admin.user-withdrawal.destroy','')}}/"+id,
                        method: 'DELETE',
                        data: {id: id},
                        success: function () {
                            toastr.success('{{\App\CPU\translate('Cancel_Successfully.')}}');
                            location.reload();
                        }
                    });
                }
            })
        });
    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFileEg1").change(function () {
            readURL(this);
        });
    </script>
@endpush
