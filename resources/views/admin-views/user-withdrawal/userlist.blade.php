@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('User Withdrawal'))

@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

    </style>
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{\App\CPU\translate('Dashboard')}}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{\App\CPU\translate('List User VIP')}}</li>
            </ol>
        </nav>
        <div class="card">
            <div class="card-header">
                <div class="col-12 p-0">
                    <!-- Search -->
                    <form action="{{ url()->current() }}" method="GET">
                        <div class="input-group input-group-merge input-group-flush">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="tio-search"></i>
                                </div>
                            </div>
                            <input id="" type="search" name="search" class="form-control"
                                placeholder="{{\App\CPU\translate('user name')}}, {{\App\CPU\translate('referral code')}}" value="{{ request('search') }}">
                            <button type="submit" class="btn btn-primary">{{\App\CPU\translate('search')}}</button>
                        </div>
                    </form>
                    <!-- End Search -->
                </div>
            </div>
            <div class="card-body">
                <div class="row" style="margin-top: 20px" id="cate-table">
                    @foreach ($users as $user)
                       <div class="col-xl-4 col-md-6 mb-3">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <img width="50px"
                                        class="avatar rounded-circle"
                                        onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                        src="{{asset('storage/profile/'.$user->image)}}"
                                    >
                                    <strong>
                                        {{ $user->name??$user->f_name .' '.$user->l_name }}
                                    </strong>
                                    </div>
                                    <strong>
                                        {{ $user->refcode}}
                                    </strong>
                                </div>
                                <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                                    <div class="parent">
                                        <h5>
                                            @php
                                                 $balance = $user->balances()->sum('balance') - $user->withdrawals()->sum('balance');
                                                 
                                            @endphp
                                            {{ \App\CPU\Helpers::currency_converter($balance) }}
                                        </h5>
                                    </div>
            
            
                                </div>
                                <div class="card-body">
        
                                    <p>{{\App\CPU\translate('Bank')}} : {{ $user->paymentmethod->name }}</p>
                                    <p>{{\App\CPU\translate('Account Name')}} : {{ $user->holder_name }}</p>
                                    <p>{{\App\CPU\translate('Account Number')}} : {{ $user->account_no }}</p>
                                </div>
                                <div class="card-footer  align-content-between">
                                  <div class="form-row">
                                    <div class="col">
                                        <a href="{{ route('admin.user-withdrawal.history',$user->id) }}" target="_blank" rel="noopener noreferrer" class="btn btn-seconedry">
                                            {{\App\CPU\translate('Referral history')}}
                                        </a>
                                    </div>
                                   <div class="col {{  $balance>0?'':'d-none' }} ">
                                    <a href="{{ route('admin.user-withdrawal.clear',$user->id) }}" target="_blank" rel="noopener noreferrer" class="btn btn-primary">
                                        {{\App\CPU\translate('Clear commision')}}
                                    </a>
                                   </div>
                                  </div>
                                </div>
                            </div>
                       </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer">
                {{$users->links()}}
            </div>
        </div>
     
       
        
    </div>
@endsection

@push('script')

@endpush
