@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('User Withdrawal'))

@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

    </style>
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('admin.dashboard') }}">{{ \App\CPU\translate('Dashboard') }}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('List User VIP') }}</li>
            </ol>
        </nav>

        <div class="row" style="margin-top: 20px">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <img width="50px" class="avatar rounded-circle"
                            onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                            src="{{ asset('storage/profile/' . $user->image) }}">
                        <strong>
                            {{ $user->name ?? $user->f_name . ' ' . $user->l_name }}
                            </span>
                    </div>
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <div class="parent">
                            <h5>
                                @php
                                    $balance = $user->balances()->sum('balance') - $user->withdrawals()->sum('balance');
                                @endphp
                                {{ \App\CPU\Helpers::currency_converter($balance) }}
                            </h5>
                        </div>


                    </div>
                    <div class="card-body">

                        <p>{{ \App\CPU\translate('Bank') }} : {{ $user->paymentmethod->name }}</p>
                        <p>{{ \App\CPU\translate('Account Name') }} : {{ $user->holder_name }}</p>
                        <p>{{ \App\CPU\translate('Account Number') }} : {{ $user->account_no }}</p>
                    </div>
                    <div class="card-footer  align-content-between">
                        <div class="form-row">

                            <div class="col-12">
                                <a href="{{ route('admin.user-withdrawal.clear', $user->id) }}"
                                    rel="noopener noreferrer" class="btn btn-primary">
                                    {{ \App\CPU\translate('Clear commision') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-xl-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="h4 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                            {{ \App\CPU\translate('Referral History') }}</h4>
                    </div>
                   
                    <div class="card-body">
                        <div class="mb-3">
                            <form action="{{ url()->current() }}" method="GET">
                                <div class="input-group input-group-merge input-group-flush">
                                    <input id="date" type="text" name="date" class="form-control" placeholder="{{ \App\CPU\translate('Date')}}"
                                    value="{{ request('date') }}" autocomplete="off">
                                        <select name="type" class="form-control">
                                            <option value="">{{ \App\CPU\translate('All Type') }}</option>
                                                @foreach ($history_types as $type)
                                                    <option value="{{  $type }}">{{ \App\CPU\translate( $type) }}</option>
                                                @endforeach
                                        </select>
                                    <button type="submit"
                                        class="btn btn-primary ml-2">{{ \App\CPU\translate('search') }}</button>
                                </div>
                            </form>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <th width=1>{{ \App\CPU\translate('Id') }}</th>
                                <th>{{ \App\CPU\translate('Type') }}</th>
                                <th>{{ \App\CPU\translate('Commission') }}</th>
                                <th>{{ \App\CPU\translate('Date') }}</th>
                            </thead>
                            <tbody>
                                @foreach ($history_balance as $key => $row)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                            {{ collect(explode('\\', $row->model))->last() }}
                                        </td>
                                        <td class="text-green">
                                            {{ \App\CPU\Helpers::currency_converter($row->balance) }}</td>
                                        <td>{{ $row->created_at->translatedFormat('d-M-Y') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">{{ \App\CPU\translate('Total') }}</td>
                                    <td colspan="2">
                                        {{ \App\CPU\Helpers::currency_converter($user->balances()->sum('balance')) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">{{ \App\CPU\translate('Withdrawl') }}</td>
                                    <td>{{ \App\CPU\Helpers::currency_converter($user->withdrawals()->sum('balance')) }}
                                    </td>
                                    <td>{{ $user->withdrawals->last() ? $user->withdrawals->last()->created_at->translatedFormat('d-M-Y') : null }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">{{ \App\CPU\translate('Final Total') }}</td>
                                    <td colspan="2">
                                        {{ \App\CPU\Helpers::currency_converter($user->balances()->sum('balance') - $user->withdrawals()->sum('balance')) }}
                                    </td>
                                </tr>
                            </tfoot>

                        </table>
                        {!! $history_balance->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $('input[name="date"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: "YYYY/MM/DD",
            cancelLabel: 'Clear'
        }
    });
    $('input[name="date"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
    });

    $('input[name="date"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

</script>
    
@endpush