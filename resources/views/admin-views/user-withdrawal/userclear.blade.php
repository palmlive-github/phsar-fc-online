@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('User Withdrawal'))

@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

    </style>
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('admin.dashboard') }}">{{ \App\CPU\translate('Dashboard') }}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('List User VIP') }}</li>
            </ol>
        </nav>

        <div class="row" style="margin-top: 20px">
            <div class="col-xl-6 mb-3">
                <div class="card">
                    <div class="card-header">
                        <img width="50px" class="avatar rounded-circle"
                            onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                            src="{{ asset('storage/profile/' . $user->image) }}">
                        <strong>
                            {{ $user->name ?? $user->f_name . ' ' . $user->l_name }}
                        </strong>
                    </div>
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <div class="parent">
                            <h5>
                                @php
                                    $balance = $user->balances()->sum('balance') - $user->withdrawals()->sum('balance');
                                @endphp
                                {{ \App\CPU\Helpers::currency_converter($balance) }}
                            </h5>
                        </div>


                    </div>
                    <div class="card-body">

                        <p>{{ \App\CPU\translate('Bank') }} : {{ $user->paymentmethod->name }}</p>
                        <p>{{ \App\CPU\translate('Account Name') }} : {{ $user->holder_name }}</p>
                        <p>{{ \App\CPU\translate('Account Number') }} : {{ $user->account_no }}</p>
                    </div>

                </div>
            </div>
            <div class="col-xl-6">

                <form action="" method="post">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h4
                                class="h4 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                                {{ \App\CPU\translate('Form Clear Commission') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="balance">{{ \App\CPU\translate('Amount') }}</label>
                                <input type="number" class="form-control" name="balance" id="balance" value="{{ old('balance') }}" step=".01">
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="noted">{{ \App\CPU\translate('Noted') }}</label>
                                        <textarea name="noted" id="" cols="20" rows="4" class="form-control">{{ old('noted') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">{{ \App\CPU\translate('Transection image') }}</label>
                                        <input type="hidden" name="image" value="{{ old('image') }}">
                                        <input type="file" id="image" class="d-none">
                                        <img width="100%" class="" id="image-trigger"
                                            onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                            src="{{ old('image') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">
                                {{ \App\CPU\translate('Ok') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/compress.js') }}"></script>
    <script>
        $(`#image-trigger`).click(() => {
            $(`#image`).click();
        });

        $(document).on('input', 'input#image', function(e) {
            if (e.target.files.length) {
                $(`#image-trigger`).attr('src', `{{ asset('assets/front-end/img/loader_.gif') }}`);
                var compressor = new window.Compress();
                compressor.compress([...e.target.files], {
                    size: 4,
                    quality: 0.75,
                }).then((output) => {
                    var v = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                    var formData = new FormData();
                    formData.append('_token', `{{ csrf_token() }}`)
                    formData.append('image', v)
                    $.ajax({
                        url: `{{ route('admin.image') }}`,
                        method: 'POST',
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: (res) => {
                            $(`#image-trigger`).attr('src', res.url);
                            if (res.status) {
                             $(`[name="image"]`).val(res.url);
                            } else {
                                $(`[name="image"]`).val('');
                            }
                            if (res.message) {
                                toastr.error(res.message);
                            }
                        }
                    });
                    $(e.target).val('');
                });

            }

            // $(this).parent().find('img').attr('src', URL.createObjectURL(e.target.files[0]));
        });
    </script>
@endpush
