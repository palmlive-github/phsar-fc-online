@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('VIP Request'))

@push('css_or_js')

@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{\App\CPU\translate('Dashboard')}}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{\App\CPU\translate('VIP Request')}}</li>
            </ol>
        </nav>



        <div class="row" style="margin-top: 20px" id="cate-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row flex-between justify-content-between align-items-center flex-grow-1">
                            <div>

                                <h5>{{ \App\CPU\translate('vip_request_table')}} : <span style="color: red;">({{ $vip_requests->count() }})</span></h5>

                            </div>
                            <div style="width: 50vw;">
                                <!-- Search -->
                                <form action="{{ url()->current() }}" method="GET">
                                    <div class="input-group input-group-merge input-group-flush">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tio-search"></i>
                                            </div>
                                        </div>
                                        <input id="" type="search" name="search" class="form-control"
                                            placeholder="{{\App\CPU\translate('user name')}}, {{\App\CPU\translate('referral code')}}" value="{{ request('search') }}">
                                            <select name="status" class="form-control w-auto">
                                                <option value="">{{ \App\CPU\translate('All') }}</option>
                                                @foreach (['requesting','approved','canceled'] as $status)
                                                <option {{ request('status') == $status ? 'selected':null }} value="{{ $status }}">{{ \App\CPU\translate($status)}}</option>
                                                @endforeach
                                            </select>
                                        <button type="submit" class="btn btn-primary">{{\App\CPU\translate('search')}}</button>
                                    </div>
                                </form>
                                <!-- End Search -->
                            </div>




                        </div>
                    </div>
                    <div class="card-body" style="padding: 0">
                        <div class="table-responsive">
                            <table style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};"
                                class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table">
                                <thead class="thead-light">
                                <tr>
                                    <th style="width: 100px">{{ \App\CPU\translate('ID')}}</th>
                                    <th>{{ \App\CPU\translate('name')}}</th>
                                    <th>{{ \App\CPU\translate('Upload ID Card')}}</th>
                                    {{-- <th>{{ \App\CPU\translate('Bank info')}}</th> --}}
                                    <th>{{ \App\CPU\translate('Request Date')}}</th>
                                    <th>{{ \App\CPU\translate('Total fee')}}</th>
                                    <th>{{ \App\CPU\translate('Payment')}}</th>
                                    <th>{{ \App\CPU\translate('Status')}}</th>
                                    <th class="text-center" style="width:15%;">{{ \App\CPU\translate('action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($vip_requests as $key=>$row)
                                    <tr>
                                        <td class="text-center">{{$key + 1}}</td>
                                        <td>{{$row->user->f_name }} {{$row->user->l_name }}
                                            ({{$row->user->refcode }})
                                        </td>
                                        <td>
                                            <a href="/storage/nationid-card/{{ $row->user->id_card_image }}" target="_blank">
                                                <img src="/storage/nationid-card/{{ $row->user->id_card_image }}" style="
                                                    width: 50px;
                                                    height: fit-content;
                                                ">
                                            </a>

                                        </td>
                                        {{-- <td>
                                            {{ \App\CPU\translate('Bank Name')}} : {{ $row->user->paymentmethod->name }}
                                            <br>
                                            {{ \App\CPU\translate('Account Name')}} : {{ $row->user->holder_name }}
                                            <br>
                                            {{ \App\CPU\translate('Account Number')}} : {{ $row->user->account_no }}
                                        </td> --}}
                                        <td>
                                            {{ $row->created_at->translatedFormat('d-m-Y') }}
                                        </td>
                                        <td>
                                            {{ \App\CPU\Helpers::currency_converter($row->total_fee)  }}
                                        </td>
                                        <td class="d-flex">
                                            <a href="/storage/upload-payment/{{ $row->upload_payment }}" target="_blank">
                                                <img src="/storage/upload-payment/{{ $row->upload_payment }}" style="
                                                    width: 50px;
                                                    height: fit-content;
                                                ">
                                            </a>

                                            <div class="pl-2">
                                                {{ \App\CPU\translate('Payment via account')}}:
                                                <br>
                                                {{ \App\CPU\translate('Bank Name')}} : {{ $row->payment['name'] }}
                                                <br>
                                                {{ \App\CPU\translate('Account Name')}} : {{ $row->payment['account_name'] }}
                                                <br>
                                                {{ \App\CPU\translate('Account Number')}} : {{ $row->payment['account_number'] }}
                                            </div>
                                        </td>
                                        <th>{{ \App\CPU\translate( $row->status)}}</th>
                                        <td style="vertical-align: top;text-align: center;">
                                            @if ($row->status == 'approved' && $vip_settings->show_vip_request_reject_button )
                                                <form action="{{ route('admin.vip-request.update',$row) }}" method="POST">
                                                    @method('put')
                                                    @csrf
                                                    <input name="status" type="hidden" value="canceled">
                                                        <button style="submit" class="btn btn-danger btn-sm edit" style="cursor: pointer;"
                                                            href="{{route('admin.vip-request.update',$row)}}">
                                                        {{ \App\CPU\translate('Reject')}}
                                                    </button>
                                                </form>
                                            @elseif($row->status == 'requesting')
                                                <form action="{{ route('admin.vip-request.update',$row) }}" method="POST">
                                                    @method('put')
                                                    @csrf
                                                    <input name="status" type="hidden" value="canceled">
                                                        <button style="submit" class="btn btn-danger btn-sm edit" style="cursor: pointer;"
                                                            href="{{route('admin.vip-request.update',$row)}}">
                                                        {{ \App\CPU\translate('Cancel')}}
                                                    </button>
                                                </form>
                                                <form action="{{ route('admin.vip-request.update',$row) }}" method="POST">
                                                    @method('put')
                                                    @csrf
                                                    <input name="status" type="hidden" value="approved">
                                                        <button style="submit" class="btn btn-primary btn-sm edit" style="cursor: pointer;"
                                                            href="{{route('admin.vip-request.update',$row)}}">
                                                        {{ \App\CPU\translate('approve')}}
                                                    </button>
                                                </form>

                                                @elseif($row->status == 'canceled')
                                            <form action="{{ route('admin.vip-request.update',$row) }}" method="POST">
                                                @method('put')
                                                @csrf
                                                <input name="status" type="hidden" value="approved">
                                                    <button style="submit" class="btn btn-primary btn-sm edit" style="cursor: pointer;"
                                                        href="{{route('admin.vip-request.update',$row)}}">
                                                    {{ \App\CPU\translate('approve')}}
                                                </button>
                                            </form>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-footer">
                        {{$vip_requests->links()}}
                    </div>
                    @if(count($vip_requests)==0)
                        <div class="text-center p-4">
                            <img class="mb-3" src="{{asset('assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                            <p class="mb-0">{{\App\CPU\translate('no_data_found')}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@endpush
