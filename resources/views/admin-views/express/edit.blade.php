@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('express'))

@push('css_or_js')
    <link rel="stylesheet" href="{{ asset('assets/back-end/css/select2.min.css') }}">
    <style>

        .droppable-area1,
        .droppable-area2,
        .ui-sortable-placeholder {
            height: 170px;
        }
        .ui-sortable-placeholder {
            background: rgb(139 139 139 / 5%) !important
        }

    </style>
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">
                        {{ \App\CPU\translate('Dashboard') }}
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    {{ \App\CPU\translate('express') }}
                </li>
            </ol>
        </nav>

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ \App\CPU\translate('express_form') }}
                    </div>
                    <div class="card-body"
                        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                        <form action="{{ route('admin.express.update', $express) }}" method="POST"
                            enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <ul class="nav nav-tabs mb-4">
                                <li class="nav-item">
                                    <a class="nav-link express_link active" href="#" id="delivery-link">
                                        {{ \App\CPU\translate('Select delivery') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link express_link" href="#" id="product-link">
                                        {{ \App\CPU\translate('Select products') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link express_link" href="#" id="price-link">
                                        {{ \App\CPU\translate('Price') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link express_link" href="#" id="referral-link">
                                        {{ \App\CPU\translate('Referral') }}
                                    </a>
                                </li>
                            </ul>
                            <div class="form-group express_form" id="delivery-form">
                                <div class="form-group">
                                    <select name="express_delivery_id" class="form-control"
                                        data-placeholder="{{ \App\CPU\translate('Select') }}">
                                        @foreach ($deliveries as $delivery)
                                            <option value="{{ $delivery->id }}"
                                                {{ old('express_delivery_id') == $delivery->id ? 'selected' : null }}>
                                                {{ $delivery->name }} - ({{ \App\CPU\translate($delivery->from) }} →
                                                {{ \App\CPU\translate($delivery->to) }})
                                            </option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>
                            <div class="form-group express_form d-none" id="product-form">
                                <div class="form-row">
                                    <div class="col-12">
                                        <table class="table table-bordered" id="product">
                                            <thead>

                                                <tr>
                                                    <th colspan="2">
                                                        <input data-toggle="search" type="text" class="form-control"
                                                            placeholder=" {{ \App\CPU\translate('Search') }}">
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th width="1"></th>
                                                    <th>{{ \App\CPU\translate('Name') }}</th>
                                                    <th>{{ \App\CPU\translate('length')}}</th>
                                                    <th>{{ \App\CPU\translate('height')}}</th>
                                                    <th>{{ \App\CPU\translate('width')}}</th>
                                                    <th>{{ \App\CPU\translate('unit')}}</th>
                                                    <th>{{ \App\CPU\translate('Status')}}</th>
                                                </tr>

                                            </thead>
                                            <tbody class="droppable-area2">
                                                @foreach ($products as $product)

                                                <tr class="bg-white" data-id="{{ $product->id }}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="product-{{  $product->id }}"
                                                                name="express_products[]" value="{{ $product->id }}" {{ in_array( $product->id,old('express_products',$express->express_products->toArray())) ?'checked':null }}>
                                                                <label class="custom-control-label text-muted" for="product-{{  $product->id }}"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <img width="50px" class="border"
                                                            onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                                            src="{{  $product->image }}" alt="">
                                                        {{ $product->name }}
                                                    </td>
                                                    <td> {{ $product->length }}</td>
                                                    <td> {{ $product->height }}</td>
                                                    <td> {{ $product->width }}</td>
                                                    <td>{{ \App\CPU\translate( $product->unit)}} </td>
                                                    <td>{{ \App\CPU\translate( $product->status)}}</td>
                                                </tr>

                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group express_form d-none" id="price-form">
                                <input type="number" class="form-control" name="price"
                                    value="{{ old('price', $express->price) }}">
                            </div>

                            <div class="form-group express_form d-none" id="referral-form">
                                <input type="number" class="form-control" name="refcode_to" id="refcode_to" {{  $express->refcode_to?'readonly':null }}
                                  maxlength="6"  value="{{ old('refcode_to', $express->refcode_to) }}">
                                  <strong>
                                    <input type="checkbox" class="mr-1 {{ $express->refcode_to?'d-none':null }}" value="1"
                                           name="without_refcode" id="without_refcode">
                                </strong>
                                <label for="without_refcode" class="{{ $express->refcode_to?'d-none':null }}"">{{\App\CPU\translate('Without Referral Code')}}</label>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(".express_link").click(function(e) {
            e.preventDefault();
            $(".express_link").removeClass('active');
            $(".express_form").addClass('d-none');
            $(this).addClass('active');

            let form_id = this.id;
            let tab = form_id.split("-")[0];
            $("#" + tab + "-form").removeClass('d-none');
        });

        var deliveries = {};
            deliveries[`{{ $deliveries->first()->id }}`]  = {!! $deliveries->first() !!};

        $(`[name="express_delivery_id"]`).select2({
        minimumInputLength: 0,
        ajax: {
        url: `{{ route('admin.express.more.delivery') }}`,
        dataType       : 'json',
        type: 'GET',
        delay          : 250,
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1
            }
        return query;
        },
        processResults: function (res,params) {
            params.page = params.page || 1;
           return results =  {
                results: res.data.map(function(data){
                    deliveries[data.id] = data;
                    return {
                        id : data.id,
                        text : data.name,
                    }
                }),
                pagination: {
                    more: params.page  < res.last_page
                }
            };
            console.log(results)

        },
        cache: true,
    }
    });

        $(`[name="express_delivery_id"]`).change(function() {
            var id = $(this).val();
            var delivery = deliveries[id];
            var $t = $(`<div id="show-delivery">
                     <div class="input-group mb-3">
                    <input type="text" readonly value="${delivery.from}" class="form-control">
                    <button type="button" class="bg-transparent border-0" data-toggle="switch-location">
                        <span class="mr-2">
                            <i class="text-primary">→</i>
                        </span>
                    </button>
                    <input type="text" readonly value="${delivery.to}" class="form-control">
                    </div>
                    <table class="table table-bordered" id="delivery">
                    <thead>
                        <th>{{ \App\CPU\translate('Sender') }}</th>
                        <th>{{ \App\CPU\translate('Receiver') }}</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <table class="table table-bordered" id="sender">
                                    <tr>
                                        <td width=1>{{ \App\CPU\translate('Name') }} </td>
                                        <td>
                                            <input readonly type="text" class="form-control" value="${delivery.sender_info.name}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=1>{{ \App\CPU\translate('Phone') }} </td>
                                        <td>
                                            <input readonly type="text" class="form-control" value="${delivery.sender_info.phone.full}" id="phone">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=1>{{ \App\CPU\translate('Address') }} </td>
                                        <td>
                                            <textarea readonly class="form-control"
                                                cols="30" rows="2">${delivery.sender_info.address}</textarea>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                            <table class="table table-bordered" id="receiver">
                                <tr>
                                    <td width=1>{{ \App\CPU\translate('Name') }} </td>
                                    <td>
                                        <input readonly type="text" class="form-control" value="${delivery.receiver_info.name}">
                                    </td>
                                </tr>
                                <tr>
                                    <td width=1>{{ \App\CPU\translate('Phone') }} </td>
                                    <td>
                                        <input readonly type="text" class="form-control" value="${delivery.receiver_info.phone.full}" id="phone">
                                    </td>
                                </tr>
                                <tr>
                                    <td width=1>{{ \App\CPU\translate('Address') }} </td>
                                    <td>
                                        <textarea readonly class="form-control"
                                            cols="30" rows="2">${delivery.receiver_info.address}</textarea>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                    </div>`);
            $(this).parent().parent().find(`#show-delivery`).remove();
            $(this).parent().parent().append($t);
        })
        $(`[name="express_delivery_id"]`)
            .val(`{{ old('express_delivery_id', $express->express_delivery_id) }}`)
            .trigger('change');
        $(".droppable-area1, .droppable-area2").sortable({
            connectWith: "tbody",
            stack: 'tbody',
            helper: function(e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function(index) {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                });
                return $helper;
            },
            stop: function(event, ui) {
                var id = ui.item.data('id');
                ui.item.find('[name="express_products[]"]').remove();
                if (ui.item.parent().hasClass('droppable-area1')) {
                    ui.item.find('>td:first').append(
                        `<input type="hidden" name="express_products[]" value="${id}">`);
                }
                $(".droppable-area1, .droppable-area2").parent().find(`tfooter`).remove();
                $(".droppable-area1, .droppable-area2").each(function(){
                    $(this).parent().append(`<tfooter>
                    <tr>
                        <td class="pl-2">
                            {{ \App\CPU\translate('Total') }} : ${$(this).parent().find('tbody>tr[data-id]').length}
                        </td>
                    </tr>
                </tfooter>`);
                });
                tablecheck();
            }

        }).disableSelection();
        $(`[data-toggle="search"]`).on('input', function() {
            var search = $(this).val();
            var $table = $(this).parents('table');
            var value = $(this).val().toLowerCase();
            $table.find("tbody>tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });

        })
        $(`#refcode_to`).on('input',function(){
            if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
        });
        var $ajax = null;
    $(`.pagination li`).click(function() {
        $(`.pagination li`).removeClass('active');
        $(this).addClass('active');
        var url = $(this).data('url');
        if ($ajax) {
            $ajax.abort();
        }

        $ajax=$.get(url).done((res) => {
            if (res.data.length) {
                $(`.droppable-area2`).html('');
                $.each(res.data,(i,$product)=>{
                    if ($(`.droppable-area1 [data-id="${$product.id}"]`).length == 0) {
                        $(`.droppable-area2`).append(tr($product));
                    }
                });
                $('img').each(function(i,img){
                            img.addEventListener("error", function (event) {
                                event.target.src = '{{asset('assets/front-end/img/image-place-holder.png')}}';
                                event.onerror = null
                            })
                        });

            $(".droppable-area2").parent().find(`tfooter`).remove();
            $(".droppable-area2").each(function() {
                $(`.droppable-area2`).parent().append(`<tfooter>
                    <tr>
                        <td class="pl-2">
                            {{ \App\CPU\translate('Total') }} : ${$(`.droppable-area2`).parent().find('tbody>tr[data-id]').length}
                        </td>
                    </tr>
                </tfooter>`);
            });
            }
        });
    });

    function tr($product){
        return $(`<tr class="bg-white" data-id="${$product.id}">
                <td>
                    <img width="50px" class="border"
                        onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                        src="${$product.image}"
                        alt="">
                    ${ $product.name }
                </td>
                <td>
                    <table class="table table-sm">
                        <tr>
                            <td>{{ \App\CPU\translate('Length') }}</td>
                            <td> ${ $product.length }</td>
                        </tr>
                        <tr>
                            <td>{{ \App\CPU\translate('Height') }}</td>
                            <td> ${ $product.height }</td>
                        </tr>
                        <tr>
                            <td>{{ \App\CPU\translate('Width') }}</td>
                            <td> ${ $product.width }</td>
                        </tr>
                        <tr>
                            <td>{{ \App\CPU\translate('Unit') }}</td>
                            <td> ${ $product.unit }</td>
                        </tr>
                    </table>
                </td>
            </tr>`)
    }
    tablecheck();
    function tablecheck(){
        if($(".droppable-area1").find(`tr`).length == 0){
            $(".droppable-area1").append(`<tr class="ui-sortable-placeholder"></tr>`)
        }else{
            $(".droppable-area1").find(`.ui-sortable-placeholder`).remove();
        }
        if($(".droppable-area2").find(`tr`).length == 0){
            $(".droppable-area2").append(`<tr class="ui-sortable-placeholder"></tr>`)
        }else{
            $(".droppable-area2").find(`.ui-sortable-placeholder`).remove();
        }
    }


    </script>
@endpush
