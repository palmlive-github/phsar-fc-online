@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('express'))

@push('css_or_js')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <style>
        .iti--allow-dropdown {
            width: 100%;
        }

    </style>
@endpush
@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">
                        {{ \App\CPU\translate('Dashboard') }}
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{ route('admin.express.index') }}">
                        {{ \App\CPU\translate('Express') }}
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{ route('admin.express.create') }}">
                        {{ \App\CPU\translate('Create') }}
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    {{ \App\CPU\translate('Add New') }}
                </li>
            </ol>
        </nav>

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ \App\CPU\translate('express_form') }}
                        <a class="float-right" href="{{ route('admin.express.create-add') }}">
                            {{ \App\CPU\translate('Back to Create') }}
                        </a>
                    </div>
                    <div class="card-body"
                        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                        <form action="{{ route('admin.express.create-add') }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs mb-4">
                                <li class="nav-item">
                                    <a class="nav-link express_link active" href="#" id="delivery-link">
                                        {{ \App\CPU\translate('Create delivery') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link express_link" href="#" id="product-link">
                                        {{ \App\CPU\translate('Create products') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link express_link" href="#" id="price-link">
                                        {{ \App\CPU\translate('Price') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link express_link" href="#" id="referral-link">
                                        {{ \App\CPU\translate('Referral') }}
                                    </a>
                                </li>
                            </ul>
                            <div class="form-group express_form {{ old('tab', 'delivery') == 'delivery' ? null : 'd-none' }}"
                                id="delivery-form">

                                <div class="form-group">
                                    <label for="name"> {{ \App\CPU\translate('Name') }}</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                </div>
                                <div class="input-group mb-3">
                                    @php
                                        $loctions = collect(['khmer', 'thai']);
                                    @endphp
                                    <select name="from" class="form-control">
                                        @foreach ($loctions as $item)
                                            <option value="{{ $item }}"
                                                {{ old('from', $loctions->first()) == $item ? 'selected' : null }}>
                                                {{ \App\CPU\translate($item) }}
                                            </option>
                                        @endforeach

                                    </select>
                                    <button type="button" class="bg-transparent border-0" data-toggle="switch-location">
                                        <span class="mr-2">
                                            <i class="text-primary">⇄</i>
                                        </span>
                                    </button>
                                    <select name="to" class="form-control">
                                        @foreach ($loctions as $item)
                                            <option value="{{ $item }}"
                                                {{ old('to', $loctions->last()) == $item ? 'selected' : null }}>
                                                {{ \App\CPU\translate($item) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <table class="table table-bordered" id="delivery">
                                    <thead>
                                        <th>{{ \App\CPU\translate('Sender') }}</th>
                                        <th>{{ \App\CPU\translate('Receiver') }}</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table class="table table-bordered" id="sender">
                                                    <tr>
                                                        <td colspan="2">
                                                            <input type="number" maxlength="6" class="form-control"
                                                                name="sender[refcode]" data-toggle="search-user"
                                                                value="{{ old('sender.refcode') }}"
                                                                placeholder="{{ \App\CPU\translate('Search by code') }}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width=1>{{ \App\CPU\translate('Name') }} </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="sender[name]"
                                                                value="{{ old('sender.name') }}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width=1>{{ \App\CPU\translate('Phone') }} </td>
                                                        <td>
                                                            <input type="text" class="form-control"
                                                                name="sender[phone][main]" id="phone"
                                                                value="{{ old('sender.phone.full') }}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width=1>{{ \App\CPU\translate('Address') }} </td>
                                                        <td>
                                                            <textarea class="form-control" name="sender[address]" cols="30" rows="2">{{ old('sender.address') }}</textarea>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="table table-bordered" id="receiver">
                                                    <tr>
                                                        <td colspan="2">
                                                            <input type="number" maxlength="6" class="form-control"
                                                                name="receiver[refcode]" data-toggle="search-user"
                                                                value="{{ old('receiver.refcode') }}"
                                                                placeholder="{{ \App\CPU\translate('Search by code') }}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width=1>{{ \App\CPU\translate('Name') }} </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="receiver[name]"
                                                                value="{{ old('receiver.name') }}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width=1>{{ \App\CPU\translate('Phone') }} </td>
                                                        <td>
                                                            <input type="text" class="form-control"
                                                                value="{{ old('receiver.phone.full') }}"
                                                                name="receiver[phone][main]" id="phone">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width=1>{{ \App\CPU\translate('Address') }} </td>
                                                        <td>
                                                            <textarea class="form-control" name="receiver[address]" cols="30" rows="2">{{ old('receiver.address') }}</textarea>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group express_form {{ old('tab') == 'product' ? null : 'd-none' }}"
                                id="product-form">
                                <table class="table table-bordered" id="product">
                                    <thead>
                                        <th width="100">{{ \App\CPU\translate('Image') }}</th>
                                        <th>{{ \App\CPU\translate('Name') }}</th>
                                        <th width="400">{{ \App\CPU\translate('Dimensions') }}</th>
                                        <th width="1"></th>
                                    </thead>
                                    <tbody>
                                        <tr id="clone" style="display: none">
                                            <td>
                                                <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                    style="object-fit: cover" height="60px" width="60px" id="trigger-image">
                                                <input id="image" class="d-none" type="hidden"
                                                    __name__="products[__index__][image]" class="form-control"
                                                    accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                <input id="input-image" class="d-none" type="file"
                                                    class="form-control"
                                                    accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                            </td>
                                            <td>
                                                <input type="text" __name__="products[__index__][name]"
                                                    class="form-control">
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="number" __name__="products[__index__][length]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Length') }}">
                                                    <input type="number" __name__="products[__index__][height]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Height') }}">
                                                    <input type="number" __name__="products[__index__][width]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Width') }}">
                                                    <select __name__="products[__index__][unit]" id=""
                                                        class="form-control">
                                                        <option value=""></option>
                                                        <option value="cm">{{ \App\CPU\translate('cm') }}</option>
                                                        <option value="kg">{{ \App\CPU\translate('kg') }}</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                        <i class="tio-add"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger" id="del">
                                                        <i class="tio-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>

                                        @if (old('products'))
                                            @foreach (old('products') as $key => $product)
                                                <tr>
                                                    <td>
                                                         <img src="{{ $product['image'] }}" height="60px" width="60px" style="object-fit: cover"
                                                            id="trigger-image">
                                                        <input id="image" class="d-none" type="hidden"
                                                            name="products[{{ $key }}][image]"
                                                            value="{{ $product['image'] }}" class="form-control">
                                                        <input id="input-image" class="d-none" type="file"
                                                            class="form-control"
                                                            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="products[{{ $key }}][name]"
                                                            class="form-control" value="{{ $product['name'] }}">
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="number"
                                                                name="products[{{ $key }}][length]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Length') }}"
                                                                value="{{ $product['length'] }}">
                                                            <input type="number"
                                                                name="products[{{ $key }}][height]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Height') }}"
                                                                value="{{ $product['height'] }}">
                                                            <input type="number"
                                                                name="products[{{ $key }}][width]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Width') }}"
                                                                value="{{ $product['width'] }}">
                                                            @php
                                                                $units = App\CPU\Helpers::units();
                                                            @endphp
                                                            <select name="products[{{ $key }}][unit]" id=""
                                                                class="form-control">
                                                                <option value=""></option>
                                                                @foreach ($units as $item)
                                                                    <option
                                                                        {{ $product['unit'] == $item ? 'selected' : null }}
                                                                        value="{{ $item }}">
                                                                        {{ \App\CPU\translate($item) }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <button type="button" class="btn btn-sm btn-success mr-1"
                                                                id="add">
                                                                <i class="tio-add"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-danger" id="del">
                                                                <i class="tio-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>
                                                    <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                        style="object-fit: cover" height="60px" width="60px" id="trigger-image">
                                                    <input id="image" class="d-none" type="hidden"
                                                        name="products[0][image]" class="form-control">
                                                    <input id="input-image" class="d-none" type="file"
                                                        class="form-control"
                                                        accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                </td>
                                                <td>
                                                    <input type="text" name="products[0][name]" class="form-control">
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="number" name="products[0][length]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('Length') }}">
                                                        <input type="number" name="products[0][height]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('Height') }}">
                                                        <input type="number" name="products[0][width]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('Width') }}">
                                                        @php
                                                            $units = App\CPU\Helpers::units();
                                                        @endphp
                                                        <select name="products[0][unit]" id="" class="form-control">
                                                            <option value=""></option>
                                                            @foreach ($units as $item)
                                                                <option value="{{ $item }}">
                                                                    {{ \App\CPU\translate($item) }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex">
                                                        <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                            <i class="tio-add"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-danger" id="del">
                                                            <i class="tio-delete"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group express_form d-none" id="price-form">
                                <input type="number" class="form-control" name="price" value="{{ old('price') }}">
                            </div>
                            <div class="form-group express_form d-none" id="referral-form">
                                <input type="number" class="form-control" name="refcode_to" id="refcode_to" maxlength="6"
                                    value="{{ old('refcode_to') }}">
                                <input type="checkbox" class="mr-1" value="1" name="without_refcode"
                                    id="without_refcode">
                                </strong>
                                <label for="without_refcode">{{ \App\CPU\translate('Without Referral Code') }}</label>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/compress.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(".express_link").click(function(e) {
            e.preventDefault();
            $(".express_link").removeClass('active');
            $(".express_form").addClass('d-none');
            $(this).addClass('active');

            let form_id = this.id;
            let tab = form_id.split("-")[0];
            $("#" + tab + "-form").removeClass('d-none');
        });
        $(`#refcode_to`).on('input', function() {
            if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
        });

        $(`[data-toggle="switch-location"]`).each(function() {
            $(this).click(function(e) {
                e.preventDefault();
                var fromVal = $(`[name="from"]`).val();
                var toVal = $(`[name="to"]`).val();
                $(`[name="from"]`).val(toVal).trigger('change');
                $(`[name="to"]`).val(fromVal).trigger('change');
            });
        });
        var Express = {
            init: () => {
                Express.delivery.init();
                Express.product.init();
            },
            delivery: {
                init: () => {
                    $("input#phone").each(function(i, el) {
                        const phoneInput = window.intlTelInput(el, {
                            separateDialCode: true,
                            hiddenInput: "full",
                            initialCountry: "auto",
                            geoIpLookup: function(success) {
                                success('KH')
                            },
                            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
                        });
                    });



                    var ajax = null;
                    $(`[data-toggle="search-user"]`).on('input', function() {
                        $(`[for="without_refcode"]`).removeClass('d-none');
                        $(`[id="without_refcode"]`).removeClass('d-none');
                        if ($(this).attr('name') == 'sender[refcode]') {
                            $(`#refcode_to`).removeAttr('readonly').val('');
                        }
                        if (this.value.length >= this.maxLength) {
                            this.value = this.value.slice(0, this.maxLength)
                            if (ajax) {
                                ajax.abort();
                            }
                            ajax = $.get(`{{ route('admin.userbyrefcode', '') }}/${this.value}`).done(
                                res => {
                                    if (res) {
                                        if ($(this).attr('name') == 'sender[refcode]') {
                                            $(`#refcode_to`).attr('readonly', true).val(res.refcode_to);
                                            $(`[for="without_refcode"]`).addClass('d-none');
                                            $(`[id="without_refcode"]`).addClass('d-none');
                                        }

                                        $(this).parent().parent().parent().find(`tr`).eq(1).find(
                                                `[name]`)
                                            .val(`${res.f_name} ${res.l_name}`);
                                        $(this).parent().parent().parent().find(`tr`).eq(2).find(
                                                `[name]`)
                                            .val(`${res.phone}`);
                                        $(this).parent().parent().parent().find(`tr`).eq(3).find(
                                                `[name]`)
                                            .val(`${res.street_address??'N/A'}`);

                                        var shippings = '';
                                        $.each(res.shippings, (i, shipping) => {
                                            shippings +=
                                                `----- ${shipping.address_type} ----- \nPhone : ${shipping.phone}\nCity : ${shipping.city}\nZip Code : ${shipping.zip}\nAddress : ${shipping.address}\nCountry : ${shipping.country}\n\n`;
                                        });
                                        if (shippings) {
                                            if (res.street_address) {
                                                shippings += res.street_address;
                                            }
                                            $(this).parent().parent().parent().find(`tr`).eq(3).find(
                                                    `[name]`)
                                                .val(shippings);
                                        }

                                    }



                                });

                        }
                    });
                }
            },
            product: {
                $append: $(`table#product tbody`),
                $clone: $(`table#product tbody tr#clone`).clone().removeAttr('style').removeAttr('id'),
                $add: `table#product tbody #add`,
                $del: `table#product tbody #del`,
                datetime: () => {
                    return (new Date()).getTime();
                },
                init: () => {

                    $(document).on('click', `img#trigger-image`, function() {
                        $(this).parent().find('input').trigger('click');
                    });
                    $(document).on('input', 'input#input-image', function(e) {
                        if(e.target.files.length){
                            $(e.target).parent().find('img').attr('src', `{{ asset('assets/front-end/img/loader_.gif') }}`);
                            var compressor = new window.Compress();
                            compressor.compress([...e.target.files], {
                                size: 4
                                , quality: 0.75
                            , }).then((output) => {
                                var v = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                                var formData = new FormData();
                                formData.append('_token', `{{ csrf_token() }}`)
                                formData.append('image', v)
                                 $.ajax({
                                    url: `{{ route('admin.image') }}`
                                    , method: 'POST'
                                    , contentType: false
                                    , processData: false
                                    , data: formData
                                    , success: (res) => {
                                        $(e.target).parent().find('img').attr('src', res.url);
                                            if(res.status){
                                                $(e.target).parent().find('#image').val(res.url);
                                            }else{
                                                $(e.target).parent().find('#image').val('');
                                            }
                                            if(res.message){
                                                toastr.error(res.message);
                                            }
                                    }
                                });
                                $(e.target).val('');
                            });

                        }

                       // $(this).parent().find('img').attr('src', URL.createObjectURL(e.target.files[0]));
                    });

                    $(document).on('click', Express.product.$add, function(e) {
                        e.preventDefault();
                        var index = Express.product.datetime();
                        var clone = Express.product.$clone.prop('outerHTML').replaceAll('__name__', 'name');
                        clone = clone.replaceAll('__index__', index);
                        $(this).parents('tr').after(clone);
                    });
                    $(document).on('click', Express.product.$del, function(e) {
                        e.preventDefault();
                        if (Express.product.$append.find(`tr:not(#clone)`).length > 1) {
                            $(this).parents('tr').remove();
                        }

                    });
                }
            }
        };
        Express.init();

        var leave_page_confirm = false;
        $('form input').on('input', function() {
            if ($(this).val()) {
                leave_page_confirm = true;
            }
        });
        window.onbeforeunload = function() {
            if (leave_page_confirm) {
                return "You have unsaved information on the page.";
            }
        }
        $(`form`).submit(() => {
            leave_page_confirm = false;
        });
    </script>
@endpush
