@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('express'))

@push('css_or_js')
<link rel="stylesheet" href="{{ asset('assets/back-end/css/select2.min.css') }}">
<style>
    .droppable-area1,
    .droppable-area2,
    .ui-sortable-placeholder {
        height: 170px;
    }
    .ui-sortable-placeholder {
     background: rgb(139 139 139 / 5%) !important
    }
</style>
@endpush

@section('content')
<div class="content container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">
                    {{ \App\CPU\translate('Dashboard') }}
                </a>
            </li>
            <li class="breadcrumb-item" aria-current="page">
                <a href="{{ route('admin.express.index') }}">
                    {{ \App\CPU\translate('Express') }}
                </a>
            </li>
            <li class="breadcrumb-item" aria-current="page">
                {{ \App\CPU\translate('Create') }}
            </li>

        </ol>

    </nav>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {{-- <div class="card-header">
                    {{ \App\CPU\translate('express_form') }}
                    <a class="float-right" href="{{ route('admin.express.create-add') }}">
                        {{ \App\CPU\translate('Add New') }}
                    </a>
                </div> --}}
                <div class="card-body"
                    style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                    <form action="{{ route('admin.express.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <ul class="nav nav-tabs mb-4">
                            <li class="nav-item">
                                <a class="nav-link express_link active" href="#" id="delivery-link">
                                    {{ \App\CPU\translate('Select delivery') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link express_link" href="#" id="product-link">
                                    {{ \App\CPU\translate('Select products') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link express_link" href="#" id="price-link">
                                    {{ \App\CPU\translate('Price') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link express_link" href="#" id="referral-link">
                                    {{ \App\CPU\translate('Referral') }}
                                </a>
                            </li>
                        </ul>

                        <div class="form-group express_form" id="delivery-form">
                            <div class="form-group">
                                <select class="form-control" data-toggle="select"
                                    data-placeholder="{{ \App\CPU\translate('Select') }}">
                                    @foreach ( session('deliveries',[]) as $delivery)
                                        <option value="{{ $delivery->id }}"
                                            {{ request('express_delivery_id') == $delivery->id ? 'selected' : null }}>
                                            {{ $delivery->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="express_delivery_id" value="{{ request('express_delivery_id') }}">
                            </div>
                            @if ($delivery = session('deliveries.0'))

                            <div id="show-delivery">
                                <div class="input-group mb-3">
                               <input type="text" readonly value="{{ \App\CPU\translate($delivery->from) }}" class="form-control">
                               <button type="button" class="bg-transparent border-0" data-toggle="switch-location">
                                   <span class="mr-2">
                                       <i class="text-primary">→</i>
                                   </span>
                               </button>
                               <input type="text" readonly value="{{ \App\CPU\translate( $delivery->to) }}" class="form-control">
                               </div>
                               <table class="table table-bordered" id="delivery">
                               <thead>
                                   <th>{{ \App\CPU\translate('Sender') }}</th>
                                   <th>{{ \App\CPU\translate('Receiver') }}</th>
                               </thead>
                               <tbody>
                                   <tr>
                                       <td>
                                           <table class="table table-bordered" id="sender">
                                               <tr>
                                                   <td width=1>{{ \App\CPU\translate('Name') }} </td>
                                                   <td>
                                                       <input readonly type="text" class="form-control" value="{{ $delivery->sender_info['name'] }}">
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td width=1>{{ \App\CPU\translate('Phone') }} </td>
                                                   <td>
                                                       <input readonly type="text" class="form-control" value="{{ $delivery->sender_info['phone']['full'] }}" id="phone">
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td width=1>{{ \App\CPU\translate('Address') }} </td>
                                                   <td>
                                                       <textarea readonly class="form-control"
                                                           cols="30" rows="2">{{ $delivery->sender_info['address'] }}</textarea>
                                                   </td>
                                               </tr>
                                           </table>
                                       </td>
                                       <td>
                                        <table class="table table-bordered" id="receiver">
                                            <tr>
                                                <td width=1>{{ \App\CPU\translate('Name') }} </td>
                                                <td>
                                                    <input readonly type="text" class="form-control" value="{{ $delivery->receiver_info['name'] }}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width=1>{{ \App\CPU\translate('Phone') }} </td>
                                                <td>
                                                    <input readonly type="text" class="form-control" value="{{ $delivery->receiver_info['phone']['full'] }}" id="phone">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width=1>{{ \App\CPU\translate('Address') }} </td>
                                                <td>
                                                    <textarea readonly class="form-control"
                                                        cols="30" rows="2">{{ $delivery->receiver_info['address'] }}</textarea>
                                                </td>
                                            </tr>
                                        </table>
                                   </td>
                                   </tr>
                               </tbody>
                               </table>
                               </div>
                            @endif
                        </div>
                        <div class="form-group express_form d-none" id="product-form">
                            <div class="form-row">
                                <div class="col-12">
                                    <table class="table table-bordered" id="product">
                                        <thead>

                                            <tr>
                                                <th colspan="2">
                                                    <input data-toggle="search" type="text" class="form-control"
                                                        placeholder=" {{ \App\CPU\translate('Search') }}">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="1"></th>
                                                <th>{{ \App\CPU\translate('Name') }}</th>
                                                <th>{{ \App\CPU\translate('length')}}</th>
                                                <th>{{ \App\CPU\translate('height')}}</th>
                                                <th>{{ \App\CPU\translate('width')}}</th>
                                                <th>{{ \App\CPU\translate('unit')}}</th>
                                                <th>{{ \App\CPU\translate('Status')}}</th>
                                            </tr>

                                        </thead>
                                        <tbody class="droppable-area2">
                                            @foreach ($products as $product)

                                            <tr class="bg-white" data-id="{{ $product->id }}">
                                                <td>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="product-{{  $product->id }}"
                                                            name="express_products[]" value="{{ $product->id }}" {{ in_array( $product->id,old('express_products',[])) ?'checked':null }}>
                                                            <label class="custom-control-label text-muted" for="product-{{  $product->id }}"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <img width="50px" class="border"
                                                        onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                                        src="{{  $product->image }}" alt="">
                                                    {{ $product->name }}
                                                </td>
                                                <td> {{ $product->length }}</td>
                                                <td> {{ $product->height }}</td>
                                                <td> {{ $product->width }}</td>
                                                <td>{{ \App\CPU\translate( $product->unit)}} </td>
                                                <td>{{ \App\CPU\translate( $product->status)}}</td>
                                            </tr>

                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <div class="form-group express_form d-none" id="price-form">
                            <input type="number" class="form-control" name="price" value="{{ old('price') }}">
                        </div>
                        @php
                            $refcode_to =  old('refcode_to',  session('deliveries.0.sender.refcode_to'));
                        @endphp
                        <div class="form-group express_form d-none" id="referral-form">
                            <input type="number" class="form-control" name="refcode_to" id="refcode_to" maxlength="6"
                                value="{{ $refcode_to }}" {{  $refcode_to ? 'readonly': null }}>
                                @if (!$refcode_to)
                            <strong>

                                <input type="checkbox" class="mr-1" value="1" name="without_refcode"
                                    id="without_refcode">
                            </strong>
                            <label for="without_refcode">{{\App\CPU\translate('Without Referral Code')}}</label>
                            @endif
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    $(".express_link").click(function(e) {
        e.preventDefault();
        $(".express_link").removeClass('active');
        $(".express_form").addClass('d-none');
        $(this).addClass('active');

        let form_id = this.id;
        let tab = form_id.split("-")[0];
        $("#" + tab + "-form").removeClass('d-none');
    });


    $(`[data-toggle="select"]`).select2({
        minimumInputLength: 0,
        ajax: {
        url: `{{ route('admin.express.more.delivery') }}`,
        dataType       : 'json',
        type: 'GET',
        delay          : 250,
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1
            }
        return query;
        },
        processResults: function (res,params) {
            params.page = params.page || 1;

           return results =  {
                results: res.map(function(data){

                    return {
                        id : data.id,
                        text : data.name,
                    }
                }),
                pagination: {
                    more: params.page  < res.last_page
                }
            };


        },
        cache: true,
    }
    });
   $(`[data-toggle="select"]`)
        .val(null)
        .trigger(`{{ request('express_delivery_id') }}`);
   $(`[data-toggle="select"]`).change(function() {
        var id = $(this).val();
        location.href = `{{ route('admin.express.create','express_delivery_id=') }}${id}`;
    });


    $(`[data-toggle="search"]`).on('input', function() {
        var search = $(this).val();
        var $table = $(this).parents('table');
        var value = $(this).val().toLowerCase();
        $table.find("tbody>tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });

    })
    $(`#refcode_to`).on('input', function() {
        if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
    });

    var leave_page_confirm = false;
    $('form input').on('input', function() {
        if ($(this).val()) {
            leave_page_confirm = true;
        }
    });
    window.onbeforeunload = function() {
        if (leave_page_confirm) {
            return "You have unsaved information on the page.";
        }
    }
    $(`form`).submit(() => {
        leave_page_confirm = false;
    });

</script>
@endpush
