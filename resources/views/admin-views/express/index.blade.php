@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('Express'))

@push('css_or_js')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <style>
        .iti--allow-dropdown {
            width: 100%;
        }

    </style>
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('admin.dashboard') }}">{{ \App\CPU\translate('Dashboard') }}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('Express') }}</li>
            </ol>
        </nav>
        <div class="row" style="margin-top: 20px" id="cate-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row flex-between justify-content-between align-items-center flex-grow-1">
                            <div>
                                <h5>{{ \App\CPU\translate('express_table') }} <span
                                        style="color: red;">({{ $express->total() }})</span></h5>
                            </div>
                            <div style="width: 50vw">
                                <!-- Search -->
                                <form action="{{ url()->current() }}" method="GET">
                                    <div class="input-group input-group-merge input-group-flush">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tio-search"></i>
                                            </div>
                                        </div>
                                        <input id="" type="search" name="search" class="form-control" placeholder=""
                                        value="{{ request('search') }}">
                                        @php
                                            $express_status = App\CPU\Helpers::express_status();
                                            unset($express_status[0]);
                                            unset($express_status[2]);
                                        @endphp
                                        <select name="status" class="form-control w-auto">
                                            <option value="">{{ \App\CPU\translate('All') }}</option>
                                            @foreach ($express_status as $status)
                                            <option {{ request('status') == $status ? 'selected':null }} value="{{ $status }}">{{ \App\CPU\translate($status)}}</option>
                                            @endforeach
                                        </select>
                                        <button type="submit"
                                            class="btn btn-primary">{{ \App\CPU\translate('search') }}</button>
                                    </div>
                                </form>
                                <!-- End Search -->
                            </div>

                            <div>
                                <a href="{{ route('admin.express.create') }}" class="btn btn-primary  float-right">
                                    <i class="tio-add-circle"></i>
                                    <span class="text">{{ \App\CPU\translate('Create') }}</span>
                                </a>
                            </div>


                        </div>
                    </div>
                    <div class="card-body" style="padding: 0">
                        <div class="table-responsive">
                            <table style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};"
                                class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="width: 100px">{{ \App\CPU\translate('ID') }}</th>
                                       
                                        <th>{{ \App\CPU\translate('Sender')}}</th>
                                        <th>{{ \App\CPU\translate('Receiver')}}</th>
                                        <th>{{ \App\CPU\translate('Product')}}</th>
                                        <th>{{ \App\CPU\translate('Price')}}</th>
                                        <th>{{ \App\CPU\translate('Status')}}</th>
                                        <th class="text-center" style="width:15%;">
                                            {{ \App\CPU\translate('action') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($express as $key => $row)
                                        <tr>
                                            <td class="text-center">{{ $row['id'] }}</td>
                                           
                                            <td>
                                                {{ \App\CPU\translate($row->delivery->from)}}
                                                <table class="table table-sm table-borderless border">
                                                    <tr>
                                                        <td>{{ \App\CPU\translate('name')}}</td>
                                                        <td>{{$row->delivery->sender_info['name']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ \App\CPU\translate('phone')}}</td>
                                                        <td>{{$row->delivery->sender_info['phone']['full']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ \App\CPU\translate('Address')}}</td>
                                                        <td style="white-space: pre;">{!! $row->delivery->sender_info['address'] !!}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                {{ \App\CPU\translate($row->delivery->to)}}
                                                <table class="table table-sm table-borderless border">
                                                    <tr>
                                                        <td>{{ \App\CPU\translate('name')}}</td>
                                                        <td>{{$row->delivery->receiver_info['name']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ \App\CPU\translate('phone')}}</td>
                                                        <td>{{$row->delivery->receiver_info['phone']['full']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ \App\CPU\translate('Address')}}</td>
                                                        <td style="white-space: pre;">{!! $row->delivery->receiver_info['address'] !!}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>{{ $row->express_products->count() }} {{ __('products') }}</td>
                                            <td>{{ \App\CPU\Helpers::currency_converter($row->price) }}</td>
                                            <td class="w-100">
                                                <select class="form-control w-auto" id="status" data-id="{{ $row->id }}">
                                                    @foreach ($express_status as $status)
                                                    <option {{  $row->status == $status ? 'selected':null }} value="{{ $status }}">{{ \App\CPU\translate($status)}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-sm edit" style="cursor: pointer;"
                                                    href="{{ route('admin.express.edit', $row) }}">
                                                    <i class="tio-edit"></i>{{ \App\CPU\translate('Edit') }}
                                                </a>
                                                <a class="btn btn-danger btn-sm delete" style="cursor: pointer;"
                                                    id="{{ $row['id'] }}">
                                                    <i class="tio-add-to-trash"></i>{{ \App\CPU\translate('Delete') }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-footer">
                        {!! $express->appends(request()->except('page'))->links() !!}
                    </div>
                    @if (count($express) == 0)
                        <div class="text-center p-4">
                            <img class="mb-3" src="{{ asset('assets/back-end') }}/svg/illustrations/sorry.svg"
                                alt="Image Description" style="width: 7rem;">
                            <p class="mb-0">{{ \App\CPU\translate('no_data_found') }}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });
    </script>

    <script>
        $(document).on('click', '.delete', function() {
            var id = $(this).attr("id");
            Swal.fire({
                title: '{{ \App\CPU\translate('Are_you_sure') }}?',
                text: "{{ \App\CPU\translate('You_will_not_be_able_to_revert_this') }}!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ \App\CPU\translate('Yes') }}, {{ \App\CPU\translate('delete_it') }}!'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ route('admin.express.destroy', '') }}/" + id,
                        method: 'DELETE',
                        data: {
                            id: id
                        },
                        success: function() {
                            toastr.success(
                                '{{ \App\CPU\translate('Category_deleted_Successfully.') }}'
                                );
                            location.reload();
                        }
                    });
                }
            })
        });
    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#viewer').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFileEg1").change(function() {
            readURL(this);
        });
    </script>

    <script>
        $(".express_link").click(function(e) {
            e.preventDefault();
            $(".express_link").removeClass('active');
            $(".express_form").addClass('d-none');
            $(this).addClass('active');

            let form_id = this.id;
            let tab = form_id.split("-")[0];
            $("#" + tab + "-form").removeClass('d-none');
        });


        $(document).on('change',`#status`,function(){
            var v = $(this).val();
            var id = $(this).data('id');

            $.post(`{{ route('admin.express.update.status') }}`,{
                _token : `{{ csrf_token() }}`,
                status : v,
                id : id,
            }).done(res=>{
                if(res.status){
                    toastr.success(res.message);
                }else{
                    toastr.error(res.message);
                }
            });
        });


    </script>
@endpush
