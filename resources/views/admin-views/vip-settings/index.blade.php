@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('VIP Settings'))

@push('css_or_js')
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('admin.dashboard') }}">{{ \App\CPU\translate('Dashboard') }}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('VIP Settings') }}</li>
            </ol>
        </nav>



        <div class="row" style="margin-top: 20px" id="cate-table">
            <div class="col-md-12">
                <form action="{{ route('admin.vip-settings.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ $vip->id }}">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <label for="fee">{{ __('Fee') }} ($)</label>
                                        <input type="number" class="form-control" value="{{ old('fee', $vip->fee) }}"
                                            name="fee" id="fee">
                                    </div>
                                    {{-- <div class="mb-3">
                                        <label for="revenue_from_register">{{ __('Revenue from register') }} ($)</label>
                                        <input type="number" class="form-control"
                                            value="{{ old('revenue_from_register', $vip->revenue_from_register) }}"
                                            name="revenue_from_register" id="revenue_from_register">
                                    </div> --}}
                                    <div class="mb-3">
                                        <label for="base_refcode">{{ __('Base Referral code') }}</label>
                                        <input type="number" class="form-control" maxlength="6"
                                            value="{{ old('base_refcode', $vip->base_refcode) }}"
                                            name="base_refcode" id="base_refcode">

                                    </div>

                                    {{-- <div class="mb-3">
                                        <label for="can_withdrawal">{{ __('Can withdrawal') }} ($)</label>
                                        <input type="number" class="form-control"
                                            value="{{ old('can_withdrawal', $vip->can_withdrawal) }}"
                                            name="can_withdrawal">

                                    </div> --}}

                                    <div class="mb-3">

                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="show_vip_request_reject_button"
                                                {{ $vip->show_vip_request_reject_button ? 'checked':null}}
                                                 name="show_vip_request_reject_button" value="1">
                                                <label class="custom-control-label" for="show_vip_request_reject_button">{{ __('Show button reject on VIP Request') }}</label>
                                              </div>
                                    </div>

                                </div>
                                <div class="col-md-8 d-none">
                                    <table class="table table-bordered" id="payment">
                                        <thead>
                                            <tr>
                                                <th colspan="5">{{ \App\CPU\translate('Payment Manual') }}</th>
                                            </tr>
                                            <tr>
                                                <th width="100">{{ \App\CPU\translate('Image') }}</th>
                                                <th>{{ \App\CPU\translate('Bank Name') }}</th>
                                                <th>{{ \App\CPU\translate('Account') }}</th>
                                                <th width="1"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="clone" style="display: none">
                                                <td>
                                                    <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                        width="60px" id="trigger-image">
                                                    <input id="image" class="d-none" type="hidden" __name__="payments[__index__][image]" class="form-control">
                                                    <input id="input-image" class="d-none" type="file" class="form-control"
                                                        accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                </td>
                                                <td>
                                                    <input type="text" __name__="payments[__index__][name]"
                                                        class="form-control">
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" __name__="payments[__index__][account_name]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('account_name') }}">
                                                        <input type="number" __name__="payments[__index__][account_number]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('account_number') }}">

                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex">
                                                        <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                            <i class="tio-add"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-danger" id="del">
                                                            <i class="tio-delete"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            @if (old('payments', $vip->payments))
                                                @foreach (old('payments', $vip->payments) as $key => $payment)
                                                    <tr>
                                                        <td>
                                                            <img src=" {{ $payment['image'] }}"  width="60px" id="trigger-image">
                                                            <input id="image"  class="d-none" type="hidden" name="payments[{{ $key }}][image]"  value="{{ $payment['image'] }}" class="form-control">
                                                            <input id="input-image" class="d-none" type="file" class="form-control"
                                                                accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                        </td>
                                                        <td>
                                                            <input type="text" name="payments[{{ $key }}][name]"
                                                                class="form-control" value="{{ $payment['name'] }}">
                                                        </td>
                                                        <td>
                                                            <div class="input-group">
                                                                <input type="text"
                                                                    name="payments[{{ $key }}][account_name]"
                                                                    class="form-control"
                                                                    placeholder="{{ \App\CPU\translate('account_name') }}"
                                                                    value="{{ $payment['account_name'] }}">
                                                                <input type="number"
                                                                    name="payments[{{ $key }}][account_number]"
                                                                    class="form-control"
                                                                    placeholder="{{ \App\CPU\translate('account_number') }}"
                                                                    value="{{ $payment['account_number'] }}">

                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <button type="button" class="btn btn-sm btn-success mr-1"
                                                                    id="add">
                                                                    <i class="tio-add"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-sm btn-danger"
                                                                    id="del">
                                                                    <i class="tio-delete"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                            width="60px" id="trigger-image">
                                                        <input id="input-image" class="d-none" type="file"
                                                            name="payments[0][image]" class="form-control"
                                                            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="payments[0][name]" class="form-control">
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="text" name="payments[0][account_name]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('account_name') }}">
                                                            <input type="number" name="payments[0][account_number]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('account_number') }}">

                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <button type="button" class="btn btn-sm btn-success mr-1"
                                                                id="add">
                                                                <i class="tio-add"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-danger" id="del">
                                                                <i class="tio-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit"
                                class="btn btn-primary">{{ \App\CPU\translate('Save Change') }}</button>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@push('script')
    <script>
        var Payment = {
            $append: $(`table#payment tbody`),
            $clone: $(`table#payment tbody tr#clone`).clone().removeAttr('style').removeAttr('id'),
            $add: `table#payment tbody #add`,
            $del: `table#payment tbody #del`,
            datetime: () => {
                return (new Date()).getTime();
            },
            init: () => {

                $(document).on('click', `img#trigger-image`, function() {
                    $(this).parent().find('input').trigger('click');
                });
                var $ajax = null;
                    $(document).on('input', 'input#input-image', function(e) {

                        if(e.target.files.length){
                            if($ajax){
                                $ajax.abort();
                            }
                            var formData = new FormData();
                                formData.append('_token',`{{ csrf_token() }}`);
                                formData.append('image',e.target.files[0]);
                            $ajax = $.ajax({
                                url : `{{ route('admin.image') }}`,
                                method : 'post',
                                processData: false,
                                contentType: false,
                                data : formData,
                                success:(res)=>{
                                    $(this).parent().find('img').attr('src', res.url);
                                    if(res.status){
                                        $(this).parent().find('#image').val(res.url);
                                    }else{
                                        $(this).parent().find('#image').val('');
                                    }
                                    if(res.message){
                                        toastr.error(res.message);
                                    }
                                }

                            });
                        }

                       // $(this).parent().find('img').attr('src', URL.createObjectURL(e.target.files[0]));
                    });

                $(document).on('click', Payment.$add, function(e) {
                    e.preventDefault();
                    var index = Payment.datetime();
                    var clone = Payment.$clone.prop('outerHTML').replaceAll('__name__', 'name');
                    clone = clone.replaceAll('__index__', index);
                    $(this).parents('tr').after(clone);
                });
                $(document).on('click', Payment.$del, function(e) {
                    e.preventDefault();
                    if (Payment.$append.find(`tr:not(#clone)`).length > 1) {
                        $(this).parents('tr').remove();
                    }

                });
            }
        };
        Payment.init();

        $(`#base_refcode`).on('input',function(){
            if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
        });
    </script>
@endpush
