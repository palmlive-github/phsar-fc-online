<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <input type="text" class="form-control form-control-user"
               id="exampleFirstName"
               name="f_name" value="{{old('f_name', $seller->f_name)}}"
               placeholder="{{\App\CPU\translate('first_name')}}" required>
    </div>
    <div class="col-sm-6">
        <input type="text" class="form-control form-control-user"
               id="exampleLastName"
               name="l_name" value="{{old('l_name', $seller->l_name)}}"
               placeholder="{{\App\CPU\translate('last_name')}}" required>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 mt-2">
        <input type="email" class="form-control form-control-user"
               id="exampleInputEmail" name="email" value="{{old('email', $seller->email)}}"
               placeholder="{{\App\CPU\translate('email_address')}}" required>
    </div>
    <div class="col-sm-6 mt-2">
        <div>
            <input type="number" class="form-control form-control-user"
                   id="phone" name="phone" value="{{old('phone', $seller->phone)}}"
                   placeholder="{{\App\CPU\translate('phone_number')}}" {{ isset($seller->id) ? '' : 'required' }}>
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <input type="password" class="form-control form-control-user" minlength="6"
               id="exampleInputPassword" name="password"
               placeholder="{{\App\CPU\translate('password')}}" {{ isset($seller->id) ? '' : 'required' }}>
    </div>
    <div class="col-sm-6">
        <input type="password" class="form-control form-control-user" minlength="6"
               id="exampleRepeatPassword"
               placeholder="{{\App\CPU\translate('repeat_password')}}" {{ isset($seller->id) ? '' : 'required' }}>
        <div
            class="pass invalid-feedback">{{\App\CPU\translate('Repeat')}}  {{\App\CPU\translate('password')}} {{\App\CPU\translate('not match')}}
            .
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <input type="number" class="form-control form-control-user"
               value="{{old('sales_commission_percentage', $seller->sales_commission_percentage)}}"
               name="sales_commission_percentage"
               placeholder="{{\App\CPU\translate('commission_given')}}" required>
    </div>
    <div class="col-sm-6">
        <label for="vip">
            <input type="checkbox" {{ $seller->vip == true ?  'checked' : '' }} name="vip" id="vip">
            <span class="ml-2">VIP Seller</span>
        </label>
    </div>

</div>
<h5 class="black">{{\App\CPU\translate('Bank Info')}}</h5>
<div class="form-group row">
    <div class="col-sm-4 mb-3 mb-sm-0">
        <label for="name">{{\App\CPU\translate('Bank Name')}} <span
                class="text-danger">*</span></label>
        <input type="text" name="bank_name" value="{{ old('bank_name', $seller->bank_name)}}"
               class="form-control" id="name"
               required>
    </div>
    <div class="col-sm-4">
        <label for="holder_name">{{\App\CPU\translate('Holder Name')}} <span
                class="text-danger">*</span></label>
        <input type="text" name="holder_name" value="{{ old('holder_name', $seller->holder_name)}}"
               class="form-control" id="holder_name"
               required>
    </div>
    <div class="col-sm-4">
        <label for="account_no">{{\App\CPU\translate('Account No')}} <span
                class="text-danger">*</span></label>
        <input type="number" name="account_no" value="{{old('account_no', $seller->account_no)}}"
               class="form-control" id="account_no"
               required>
    </div>
</div>

<h5 class="black">{{\App\CPU\translate('Shop')}} {{\App\CPU\translate('Info')}}</h5>

<div class="form-group row">
    <div class="col-sm-3 mb-3 mb-sm-0">
        <input type="text" class="form-control form-control-user" id="shop_name"
               name="shop_name" placeholder="{{\App\CPU\translate('shop_name')}}"
               value="{{old('shop_name', $seller->shop->name)}}" required>
    </div>
    <div class="col-sm-3 mb-3 mb-sm-0">
        <input type="text" class="form-control form-control-user" id="registration_number"
               name="registration_number"
               placeholder="{{\App\CPU\translate('Registration Number')}} or {{\App\CPU\translate('Shop Number')}}"
               value="{{old('registration_number', $seller->shop->registration_number)}}">
    </div>
    <div class="col-sm-6">
        <textarea name="shop_address" class="form-control" id="shop_address" rows="1"
                  placeholder="{{\App\CPU\translate('shop_address')}}">{{old('shop_address',$seller->shop->address)}}</textarea>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-3 mb-3 mb-sm-0">
        <div class="pb-2 text-center">
            <img
                style="width: auto;border: 1px solid; border-radius: 10px; max-height:200px;"
                id="viewer"
                src="{{asset('assets\back-end\img\400x400\img2.jpg')}}"
                alt="profile image"/>
        </div>

        <div class="form-group mt-2">
            <div class="custom-file" style="text-align: left">
                <input type="file" name="image" id="customFileUpload"
                       class="custom-file-input"
                       accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                <label class="custom-file-label"
                       for="customFileUpload">{{\App\CPU\translate('Upload')}} {{\App\CPU\translate('Profile Image')}}</label>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="pb-2 text-center">
            <img
                style="width: auto;border: 1px solid; border-radius: 10px; max-height:200px;"
                id="viewerPassport"
                src="{{asset('assets\back-end\img\400x400\img2.jpg')}}"
                alt="Passport image"/>
        </div>
        <div class="form-group mt-2">
            <div class="custom-file" style="text-align: left">
                <input type="file" name="passport_image" id="PassportUpload"
                       class="custom-file-input"
                       accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*" required>
                <label class="custom-file-label"
                       for="customFileUpload">{{\App\CPU\translate('Upload')}} {{\App\CPU\translate('Passport Image')}}</label>
            </div>
        </div>
    </div>
    <div class="col-sm-3 mb-3 mb-sm-0">
        <div class="pb-2 text-center">
            <img
                style="width: auto;border: 1px solid; border-radius: 10px; max-height:200px;"
                id="viewerLogo"
                src="{{asset('assets\back-end\img\400x400\img2.jpg')}}"
                alt="Logo image"/>
        </div>

        <div class="form-group mt-2">
            <div class="custom-file" style="text-align: left">
                <input type="file" name="logo" id="LogoUpload"
                       class="custom-file-input"
                       accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                <label class="custom-file-label"
                       for="LogoUpload">{{\App\CPU\translate('Upload')}} {{\App\CPU\translate('logo')}}</label>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="pb-2 text-center">
            <img
                style="width: auto;border: 1px solid; border-radius: 10px; max-height:200px;"
                id="viewerBanner"
                src="{{asset('assets\back-end\img\400x400\img2.jpg')}}"
                alt="banner image"/>
        </div>

        <div class="form-group mt-2">
            <div class="custom-file" style="text-align: left">
                <input type="file" name="banner" id="BannerUpload"
                       class="custom-file-input"
                       accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*"
                       style="overflow: hidden; padding: 2%">
                <label class="custom-file-label"
                       for="BannerUpload">{{\App\CPU\translate('Upload')}} {{\App\CPU\translate('Banner')}}</label>
            </div>
        </div>
    </div>
</div>
