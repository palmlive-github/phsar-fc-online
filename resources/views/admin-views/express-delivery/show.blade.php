@extends('layouts.back-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
<style>
    .parent {
        width: 100px;
        height: 100px;
        display: table;
        border: 1px solid #e9e9e9;
        border-radius: 50%;
    }

    .parent h5 {
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }

    .navbar-sticky {
        position: relative !important;
    }
</style>
@endpush

@section('content')

<div class="content container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">
                    {{ \App\CPU\translate('Dashboard') }}
                </a>
            </li>
            <li class="breadcrumb-item" aria-current="page">
                {{ \App\CPU\translate('express Delivery') }}
            </li>
        </ol>
    </nav>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
                <div class="card box-shadow-sm">
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <table class="table border-0">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="form-row mb-2 justify-content-between">
                                            <div class="col">
                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('Date') }} :
                                                    {{ $express->created_at->translatedFormat('d-M-Y') }}
                                                </p>

                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('From') }} :
                                                    {{ \App\CPU\translate($express->delivery->from) }}
                                                    →
                                                    {{ \App\CPU\translate($express->delivery->to) }}
                                                </p>
                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('Sender') }} :

                                                    {{ $express->delivery->sender_info['name']}}

                                                </p>
                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('Receiver') }} :

                                                    {{ $express->delivery->receiver_info['name']}}

                                                </p>
                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('Receiver Phone') }} :
                                                    {{ $express->delivery->receiver_info['phone']['full']}}
                                                </p>
                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('Receiver Address') }} :
                                                    <div class="border px-3" style="white-space: pre-line;">
                                                    {!! $express->delivery->receiver_info['address'] !!}
                                                    </div>
                                                    @if ($express->delivery->address_image)
                                                    <div>
                                                        <img src="{{  $express->delivery->address_image }}" alt="" style="width: 300px;
                                                        height: 200px;
                                                        object-fit: contain;" class="border">
                                                        </div>
                                                    @endif


                                                </p>




                                            </div>
                                            <div class="col-auto">
                                                <p class="mb-0">
                                                    {{\App\CPU\translate('Price') }} : {{
                                                    \App\CPU\Helpers::currency_converter($express->price) }}
                                                </p>
                                                <p class="mb-0">
                                                    {{\App\CPU\translate('Status') }} :
                                                    {{\App\CPU\translate($express->status) }}
                                                </p>
                                                @if ($express->payment_image)
                                                <p class="mb-0">
                                                    {{ \App\CPU\translate('Payment Image') }} :

                                                    <div>
                                                    <img src="{{  $express->payment_image }}" alt="" style="width: 300px;
                                                    height: 300px;
                                                    object-fit: contain;" class="border">
                                                    </div>

                                                </p>
                                                @endif

                                            </div>
                                        </div>

                                        <table class="table table-bordered">

                                            <head>

                                                <tr>
                                                    <th width=1>{{ \App\CPU\translate('Id') }}</th>
                                                    <th>{{ \App\CPU\translate('name')}}</th>
                                                    <th>{{ \App\CPU\translate('length') }}</th>
                                                    <th>{{ \App\CPU\translate('height') }}</th>
                                                    <th>{{ \App\CPU\translate('width') }}</th>
                                                    <th>{{ \App\CPU\translate('weight') }}</th>
                                                    <th>{{ \App\CPU\translate('unit') }}</th>
                                                    <th>{{ \App\CPU\translate('price') }}</th>
                                                </tr>
                                            </head>
                                            <tbody>

                                                @foreach( $express->products as $j=>$product)
                                                <tr>
                                                    <td class="text-center">{{$j+ 1}}</td>
                                                    <td class="">
                                                        <div>
                                                            <img class="border" style="width: 50px;height:50px;object-fit:contain;"
                                                                onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                                src="{{ $product->image }}" alt="">

                                                            {{$product->name}}
                                                        </div>
                                                    </td>
                                                    <td> {{ $product->length }}</td>
                                                    <td> {{ $product->height }}</td>
                                                    <td> {{ $product->width }}</td>
                                                    <td> {{ $product->weight }}</td>
                                                    <td>{{ \App\CPU\translate($product->unit) }} </td>
                                                    <td>{{ \App\CPU\Helpers::currency_converter($product->price) }}</td>

                                                </tr>
                                                @endforeach
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td>{{ $express->products->sum('weight') }}</td>
                                                        <td>{{ \App\CPU\translate($express->products->first()->unit) }}</td>
                                                        <td>{{  \App\CPU\Helpers::currency_converter($express->products->sum('price')) }}</td>
                                                    </tr>
                                                </tfoot>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>

        </div>
    </div>
</div>
@endsection
