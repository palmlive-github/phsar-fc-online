 <div class="form-group express_form">
                                <div class="form-row">
                                    <div class="col-12">
                                        <table class="table table-bordered">
                                            <thead>

                                                <tr>
                                                    <th colspan="7">{{ \App\CPU\translate('Select products') }}</th>
                                                    <th colspan="2">
                                                        <input data-toggle="search" type="text" class="form-control"
                                                            placeholder=" {{ \App\CPU\translate('Search') }}">
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th width="1"></th>
                                                    <th>{{ \App\CPU\translate('Name') }}</th>
                                                    <th>{{ \App\CPU\translate('length') }}</th>
                                                    <th>{{ \App\CPU\translate('height') }}</th>
                                                    <th>{{ \App\CPU\translate('width') }}</th>
                                                    <th>{{ \App\CPU\translate('Weight') }}</th>
                                                    <th>{{ \App\CPU\translate('unit') }}</th>
                                                    <th>{{ \App\CPU\translate('Price') }}</th>
                                                    <th>{{ \App\CPU\translate('Status') }}</th>
                                                </tr>

                                            </thead>
                                            <tbody class="droppable-area2">
                                             
                                                @foreach ($products as $product)
                                                    <tr class="bg-white" data-id="{{ $product->id }}">
                                                        <td>
                                                            <div class="form-group">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="product-{{ $product->id }}"
                                                                        name="express_products[]"
                                                                        value="{{ $product->id }}"
                                                                        data-product="{{ $product }}"
                                                                        {{ in_array($product->id,$express_products)? 'checked': null }}>
                                                                    <label class="custom-control-label text-muted"
                                                                        for="product-{{ $product->id }}"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <img width="50px" class="border"
                                                                onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                                                src="{{ $product->image }}" alt="">
                                                            {{ $product->name }}
                                                        </td>
                                                        <td> {{ $product->length }}</td>
                                                        <td> {{ $product->height }}</td>
                                                        <td> {{ $product->width }}</td>
                                                        <td> {{ $product->weight }}</td>
                                                        <td>{{ \App\CPU\translate($product->unit) }} </td>
                                                        <td>{{ \App\CPU\Helpers::currency_converter ($product->price) }}</td>
                                                        <td>{{ \App\CPU\translate($product->status) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        @if(count($products)==0)
                                        <div class="text-center p-4">
                                            <img class="mb-3" src="{{asset('assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                                            <p class="mb-0">{{\App\CPU\translate('no_data_found')}}</p>
                                        </div>
                                    @endif
                                    </div>
                                </div>
                            </div>