@if(count($combinations) > 0)
    <table class="table table-bordered">
        <thead>
        <tr>
            <td class="text-center">
                <label for="" class="control-label">{{\App\CPU\translate('Variant')}}</label>
            </td>
            <td class="text-center">
                <label for="" class="control-label">{{\App\CPU\translate('Variant Price')}}</label>
            </td>
            <td class="text-center">
                <label for="" class="control-label">{{\App\CPU\translate('SKU')}}</label>
            </td>
            <td class="text-center">
                <label for="" class="control-label">{{\App\CPU\translate('Quantity')}}</label>
            </td>
            <td class="text-center d-none">
                <label for="" class="control-label">{{\App\CPU\translate('Image')}}</label>
            </td>
        </tr>
        </thead>
        <tbody>
        @endif
        @foreach ($combinations as $key => $combination)
            <tr>
                <td>
                    <label for="" class="control-label">{{ $combination['type'] }}</label>
                </td>
                <td>
                    <input type="number" name="price_{{ $combination['type'] }}"
                           value="{{ \App\CPU\Convert::default($combination['price']) }}" min="0"
                           step="0.01"
                           class="form-control" required>
                </td>
                <td>
                    <input type="text" name="sku_{{ $combination['type'] }}" value="{{ $combination['sku'] }}"
                           class="form-control" required>
                </td>
                <td>
                    <input type="number" name="qty_{{ $combination['type'] }}" value="{{ $combination['qty'] }}" min="1"
                           max="100000" step="1"
                           class="form-control"
                           required>
                </td>
                <td class="d-none">

                    <input  id="image"  class="d-none" type="hidden" name="image_{{ @$combination['color'] }}"  value="{{  @$combination['image'] }}" class="form-control">
                    <img src="{{ @$combination['image'] }}"
                    style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                    {{-- <input id="image"  class="d-none" type="hidden" name="image_{{ $str }}"  value="{{ request('image_'.$str)??request('image_'.@$strColor??$str) }}" class="form-control"> --}}
                    <input id="input-image" class="d-none" type="file"class="form-control"
                        accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">

                    {{-- @php( $image =$combination['image']  ?? 'def.png')
                    <input type="hidden" name="old_image_{{ $key}}" value="{{ $image }}">
                    <input type="file" name="image_{{ $key }}" id="image_{{ $key }}"
                           class="d-none variant-image" data-preview="#variant-image-preview-{{ $key }}" required>
                    <label for="image_{{ $key }}" style="cursor: pointer">
                        <img src="{{ asset('storage/product/variant/' . $image ?? 'def.png') }}"
                             alt=""
                             id="variant-image-preview-{{ $key }}" width="50px">
                    </label> --}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

