<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.vip-request.index','status=requesting')}}" style="background: #9b3703">
        <div class="card-body">
            <div class="flex-between align-items-center mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('requesting')}}</h6>
                    <span class="card-title h2" style="color: white!important;">
                        {{App\Model\VIPRequest::where('status','requesting')->count()}}
                    </span>
                </div>
                <div class="mt-2">
                    <i class="tio-time" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.vip-request.index','status=approved')}}" style="background: #037830">
        <div class="card-body">
            <div class="flex-between align-items-center gx-2 mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('approved')}}</h6>
                    <span class="card-title h2" style="color: white!important;">
                        {{App\Model\VIPRequest::where('status','approved')->count()}}
                    </span>
                </div>

                <div class="mt-2">
                    <i class="tio-checkmark-circle-outlined" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.vip-request.index','status=canceled')}}" style="background: #ff4444">
        <div class="card-body">
            <div class="flex-between align-items-center gx-2 mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('canceled')}}</h6>
                    <span class="card-title h2" style="color: white!important;">
                        {{App\Model\VIPRequest::where('status','canceled')->count()}}
                    </span>
                </div>

                <div class="mt-2">
                    <i class="tio-remove-from-trash" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

