<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.express-delivery.index','status=pending')}}" style="background: #4e35f4">
        <div class="card-body">
            <div class="flex-between align-items-center mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('pending')}}</h6>
                    <span class="card-title h2" style="color: white!important;">
                        {{App\Model\Express::where('status','pending')->count()}}
                    </span>
                </div>
                <div class="mt-2">
                    <i class="tio-time" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.express-delivery.index','status=confirmed')}}" style="background: #2d5dd9">
        <div class="card-body">
            <div class="flex-between align-items-center mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('confirmed')}}</h6>
                     <span class="card-title h2" style="color: white!important;">
                        {{App\Model\Express::where('status','confirmed')->count()}}
                     </span>
                </div>

                <div class="mt-2">
                    <i class="tio-checkmark-circle" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.express-delivery.index','status=received')}}" style="background: #31d370">
        <div class="card-body">
            <div class="flex-between align-items-center gx-2 mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('received')}}</h6>
                    <span class="card-title h2" style="color: white!important;">
                        {{App\Model\Express::where('status','received')->count()}}
                    </span>
                </div>

                <div class="mt-2">
                    <i class="tio-checkmark-circle-outlined" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

<div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
    <!-- Card -->
    <a class="card card-hover-shadow h-100" href="{{route('admin.express-delivery.index','status=canceled')}}" style="background: #f57070">
        <div class="card-body">
            <div class="flex-between align-items-center gx-2 mb-1">
                <div style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                    <h6 class="card-subtitle" style="color: white!important;">{{\App\CPU\translate('canceled')}}</h6>
                    <span class="card-title h2" style="color: white!important;">
                        {{App\Model\Express::where('status','canceled')->count()}}
                    </span>
                </div>

                <div class="mt-2">
                    <i class="tio-remove-from-trash" style="font-size: 30px;color: white"></i>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </a>
    <!-- End Card -->
</div>

