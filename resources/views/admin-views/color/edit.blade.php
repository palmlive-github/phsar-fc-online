@extends('layouts.back-end.app')
@section('title', \App\CPU\translate('Attribute'))
@push('css_or_js')
    <!-- Custom styles for this page -->
    <link href="{{asset('assets/back-end')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{asset('assets/back-end/css/croppie.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="content container-fluid">

        <div class="row">
            <div class="col-md-12" style="margin-bottom: 10px;">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ \App\CPU\translate('update')}} {{ \App\CPU\translate('Color')}}</h3>
                    </div>
                    <div class="card-body"
                         style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
                        <form action="{{route('admin.colors.update',[$color['id']])}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label
                                    for="name">{{ \App\CPU\translate('Color_name')}}</label>
                                <input type="text" name="name" class="form-control" id="name"
                                       placeholder="{{\App\CPU\translate('Enter_Color_Name')}}"
                                       value="{{ $color->name }}">
                            </div>

                            <div class="form-group">
                                <label
                                    for="code">{{ \App\CPU\translate('Color_code')}}</label>
                                <input name="code" class="form-control color-picker" id="code"
                                       placeholder="{{\App\CPU\translate('Enter_Color_Name')}}"
                                       value="{{ $color->code }}">
                            </div>
                            <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('update')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div>
@endsection

@push('script')

@endpush
