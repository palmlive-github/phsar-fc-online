@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('express'))

@push('css_or_js')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <style>
        .iti--allow-dropdown {
            width: 100%;
        }
                .select2-container {
            width: 100% !important;
        }

        .select2-container .select2-selection--single {
            height: 40px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 40px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 40px;
        }

    </style>
@endpush
@php
$express_create_product_status = App\CPU\Helpers::express_create_product_status();
@endphp
@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">
                        {{ \App\CPU\translate('Dashboard') }}
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    {{ \App\CPU\translate('express Product') }}
                </li>
            </ol>
        </nav>

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ \App\CPU\translate('express_product_form') }}
                    </div>
                    <div class="card-body"
                        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                        <form action="{{ route('admin.express-product.store') }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group express_form" id="sender-form">
                                <label for="">{{ \App\CPU\translate('Sender') }} <span style="color:red">*</span> </label>
                                <div class="form-group">
                                    <select name="sender_id" class="form-control"
                                        data-placeholder="{{ \App\CPU\translate('Select') }}">
                                    </select>
                                </div>

                            </div>

                            <div class="form-group express_form" id="product-form">
                                <table class="table table-bordered" id="product">
                                    <thead>
                                        <th width="100">{{ \App\CPU\translate('Image') }}</th>
                                        <th>{{ \App\CPU\translate('Name') }}</th>
                                        <th width="400">{{ \App\CPU\translate('Dimensions') }} ({{ \App\CPU\translate('Weight') }} <span style="color:red">*</span>, {{ \App\CPU\translate('Unit') }} <span style="color:red">*</span>)</th>
                                        <th width="100">{{ \App\CPU\translate('Price') }}</th>
                                        <th width="150">{{ \App\CPU\translate('Status') }}</th>
                                        <th width="1"></th>
                                    </thead>
                                    <tbody>
                                        <tr id="clone" style="display: none">
                                            <td>
                                                <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                    style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                                                <input id="image" class="d-none" type="hidden" __name__="products[__index__][image]" class="form-control">
                                                <input id="input-image" class="d-none" type="file"
                                                    __name__="products[__index__][image]" class="form-control"
                                                    accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                            </td>
                                            <td>
                                                <input type="text" __name__="products[__index__][name]"
                                                    class="form-control">
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="number" __name__="products[__index__][length]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Length') }}">
                                                    <input type="number" __name__="products[__index__][height]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Height') }}">
                                                    <input type="number" __name__="products[__index__][width]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('weight') }}">
                                                        <input type="number" __name__="products[__index__][weight]"
                                                        class="form-control"
                                                        placeholder="{{ \App\CPU\translate('weight') }}">
                                                        @php
                                                        $units = App\CPU\Helpers::units();
                                                    @endphp
                                                    <select __name__="products[__index__][unit]" id=""
                                                        class="form-control">

                                                        @foreach ($units as $item)
                                                        <option value="{{ $item }}">
                                                            {{ \App\CPU\translate($item) }}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="number" __name__="products[__index__][price]" step=".01"
                                                    class="form-control" placeholder="$">
                                            </td>

                                            <td>

                                                <select __name__="products[__index__][status]" id=""
                                                    class="form-control">
                                                    @foreach ($express_create_product_status as $item)
                                                        <option

                                                            value="{{ $item }}">
                                                            {{ \App\CPU\translate($item) }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                        <i class="tio-add"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger" id="del">
                                                        <i class="tio-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        @if (old('products'))
                                            @foreach (old('products') as $key => $product)
                                                <tr>
                                                    <td>
                                                        <img onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                        src="{{ $product['image'] }}"
                                                        style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                                                        <input id="image"  class="d-none" type="hidden" name="products[{{ $key }}][image]"  value="{{ $product['image'] }}" class="form-control">
                                                        <input id="input-image" class="d-none" type="file" class="form-control"
                                                            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="products[{{ $key }}][name]" class="form-control"
                                                            value="{{ $product['name'] }}">
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="number"
                                                                name="products[{{ $key }}][length]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Length') }}"
                                                                value="{{ $product['length'] }}">
                                                            <input type="number"
                                                                name="products[{{ $key }}][height]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Height') }}"
                                                                value="{{ $product['height'] }}">
                                                            <input type="number"
                                                                name="products[{{ $key }}][width]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Width') }}"
                                                                value="{{ $product['width'] }}">

                                                                <input type="number"
                                                                name="products[{{ $key }}][weight]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Weight') }}"
                                                                value="{{ $product['weight'] }}">
                                                                @php
                                                                $units = App\CPU\Helpers::units();
                                                            @endphp
                                                            <select name="products[{{ $key }}][unit]" id=""
                                                                class="form-control">
                                                                @foreach ($units as $item)
                                                                    <option {{  $product['unit']== $item  ? 'selected':null }} value="{{ $item }}">
                                                                        {{ \App\CPU\translate($item) }}</option>
                                                                @endforeach
                                                            </select>


                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="number" name="products[{{ $key }}][price]" step=".01"
                                                            class="form-control" value="{{ $product['price'] }}" placeholder="$">
                                                    </td>
                                                    <td>

                                                        <select name="products[{{ $key }}][status]" id=""
                                                            class="form-control">
                                                            @foreach ($express_create_product_status as $item)
                                                                <option
                                                                    {{ $product['status'] == $item ? 'selected' : null }}
                                                                    value="{{ $item }}">
                                                                    {{ \App\CPU\translate($item) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <button type="button" class="btn btn-sm btn-success mr-1"
                                                                id="add">
                                                                <i class="tio-add"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-danger" id="del">
                                                                <i class="tio-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else

                                        <tr>
                                            <td>
                                                <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                    style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                                                <input id="image"  class="d-none" type="hidden" name="products[0][image]"  value="" class="form-control">
                                                <input id="input-image" class="d-none" type="file"class="form-control"
                                                    accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                            </td>
                                            <td>
                                                <input type="text" name="products[0][name]" class="form-control">
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="number" name="products[0][length]" class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Length') }}">
                                                    <input type="number" name="products[0][height]" class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Height') }}">
                                                    <input type="number" name="products[0][width]" class="form-control"
                                                        placeholder="{{ \App\CPU\translate('Width') }}">
                                                        <input type="number" name="products[0][weight]" class="form-control"
                                                        placeholder="{{ \App\CPU\translate('weight') }}">
                                                    @php
                                                        $units = App\CPU\Helpers::units();
                                                    @endphp
                                                    <select name="products[0][unit]" id="" class="form-control">

                                                        @foreach ($units as $item)
                                                            <option value="{{ $item }}">
                                                                {{ \App\CPU\translate($item) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="number" name="products[0][price]" step=".01"
                                                    class="form-control" placeholder="$">
                                            </td>
                                            <td>

                                                <select name="products[0][status]" id=""
                                                    class="form-control">
                                                    @foreach ($express_create_product_status as $item)
                                                        <option value="{{ $item }}">
                                                            {{ \App\CPU\translate($item) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                        <i class="tio-add"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger" id="del">
                                                        <i class="tio-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>

                            <hr>
                            <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script src="{{ asset('js/compress.js') }}"></script>
    <script>
        $(`[data-toggle="switch-location"]`).each(function() {
            $(this).click(function(e) {
                e.preventDefault();
                var fromVal = $(`[name="from"]`).val();
                var toVal = $(`[name="to"]`).val();
                $(`[name="from"]`).val(toVal).trigger('change');
                $(`[name="to"]`).val(fromVal).trigger('change');
            });
        });


    $(`[name="sender_id"]`).select2({
        minimumInputLength: 0,
        ajax: {
        url: `{{ route('admin.express.more.user') }}`,
        dataType       : 'json',
        type: 'GET',
        delay          : 250,
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1
            }
        return query;
        },
        processResults: function (res,params) {
            params.page = params.page || 1;
           return results =  {
                results: res.data.map(function(data){
                    return {
                        id : data.id,
                        text : `${data.name?? `${data.f_name} ${data.l_name}`} (${data.refcode})`,
                    }
                }),
                pagination: {
                    more: params.page  < res.last_page
                }
            };

        },
        cache: true,
    }
    });
    $(`[name="sender_id"]`)
        .val(null)
        .trigger('change');

    if (`{{ old('sender_id') }}`) {
        $(`[name="sender_id"]`)
            .val(`{{ old('sender_id') }}`)
            .trigger('change');
    }



        var Express = {
            init: () => {
                Express.product.init();
            },
            product: {
                $append: $(`table#product tbody`),
                $clone: $(`table#product tbody tr#clone`).clone().removeAttr('style').removeAttr('id'),
                $add: `table#product tbody #add`,
                $del: `table#product tbody #del`,
                datetime: () => {
                    return (new Date()).getTime();
                },
                init: () => {

                    $(document).on('click', `img#trigger-image`, function() {
                        $(this).parent().find('input').trigger('click');
                    });
                    $(document).on('input', 'input#input-image', function(e) {
                        if(e.target.files.length){
                            $(e.target).parent().find('img').attr('src', `{{ asset('assets/front-end/img/loader_.gif') }}`);
                            var compressor = new window.Compress();
                            compressor.compress([...e.target.files], {
                                size: 4
                                , quality: 0.75
                            , }).then((output) => {
                                var v = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                                var formData = new FormData();
                                formData.append('_token', `{{ csrf_token() }}`)
                                formData.append('image', v)
                                 $.ajax({
                                    url: `{{ route('admin.image') }}`
                                    , method: 'POST'
                                    , contentType: false
                                    , processData: false
                                    , data: formData
                                    , success: (res) => {
                                        $(e.target).parent().find('img').attr('src', res.url);
                                            if(res.status){
                                                $(e.target).parent().find('#image').val(res.url);
                                            }else{
                                                $(e.target).parent().find('#image').val('');
                                            }
                                            if(res.message){
                                                toastr.error(res.message);
                                            }
                                    }
                                });
                                $(e.target).val('');
                            });

                        }

                       // $(this).parent().find('img').attr('src', URL.createObjectURL(e.target.files[0]));
                    });

                    $(document).on('click', Express.product.$add, function(e) {
                        e.preventDefault();
                        var index = Express.product.datetime();
                        var clone = Express.product.$clone.prop('outerHTML').replaceAll('__name__', 'name');
                        clone = clone.replaceAll('__index__', index);
                        $(this).parents('tr').after(clone);
                    });
                    $(document).on('click', Express.product.$del, function(e) {
                        e.preventDefault();
                        if (Express.product.$append.find(`tr:not(#clone)`).length > 1) {
                            $(this).parents('tr').remove();
                        }

                    });
                }
            }
        };
        Express.init();
    </script>
@endpush
