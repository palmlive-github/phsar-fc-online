@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('Category'))

@push('css_or_js')
@endpush

@section('content')
    <div class="content container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('admin.dashboard') }}">{{ \App\CPU\translate('Dashboard') }}</a>
                </li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('category') }}</li>
            </ol>
        </nav>

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ \App\CPU\translate('marketing_level_form') }}
                    </div>
                    <div class="card-body"
                        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                        <form action="{{ route('admin.marketing-level.update', $marketingLevel) }}" method="POST"
                            enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            @php($language = \App\Model\BusinessSetting::where('type', 'pnc_language')->first())
                            @php($language = $language->value ?? null)
                            @php($default_lang = 'en')

                            @php($default_lang = json_decode($language)[0])
                            <ul class="nav nav-tabs mb-4">
                                @foreach (json_decode($language) as $lang)
                                    <li class="nav-item">
                                        <a class="nav-link lang_link {{ $lang == $default_lang ? 'active' : '' }}" href="#"
                                            id="{{ $lang }}-link">{{ \App\CPU\Helpers::get_language_name($lang) . '(' . strtoupper($lang) . ')' }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="row">
                                <div class="col-6">
                                    @foreach (json_decode($language) as $lang)
                                        <?php
                                        if (count($marketingLevel['translations'])) {
                                            $translate = [];
                                            foreach ($marketingLevel['translations'] as $t) {
                                                if ($t->locale == $lang && $t->key == 'name') {
                                                    $translate[$lang]['name'] = $t->value;
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="form-group {{ $lang != $default_lang ? 'd-none' : '' }} lang_form"
                                            id="{{ $lang }}-form">
                                            <label class="input-label">{{ \App\CPU\translate('name') }}
                                                ({{ strtoupper($lang) }})
                                            </label>
                                            <input type="text" name="name[]"
                                                value="{{ $lang == $default_lang ? $marketingLevel['name'] : $translate[$lang]['name'] ?? '' }}"
                                                class="form-control"
                                                placeholder="{{ \App\CPU\translate('New') }} {{ \App\CPU\translate('Name') }}"
                                                {{ $lang == $default_lang ? 'required' : '' }}>
                                        </div>
                                        <input type="hidden" name="lang[]" value="{{ $lang }}">
                                    @endforeach
                                </div>

                                <div class="col-6 from_part_2">
                                    <label>{{ \App\CPU\translate('commission') }} ({{ $maxCommission }}) %</label>
                                    <div class="custom-file" style="text-align: left">
                                        <input type="number" name="commission" id="commission" class="form-control"
                                            required max="{{ $maxCommission }}"
                                            value="{{ $marketingLevel['commission'] }}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('update') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(".lang_link").click(function(e) {
            e.preventDefault();
            $(".lang_link").removeClass('active');
            $(".lang_form").addClass('d-none');
            $(this).addClass('active');

            let form_id = this.id;
            let lang = form_id.split("-")[0];
            console.log(lang);
            $("#" + lang + "-form").removeClass('d-none');
            if (lang == '{{ $default_lang }}') {
                $(".from_part_2").removeClass('d-none');
            } else {
                $(".from_part_2").addClass('d-none');
            }
        });


        $(`#commission`).on('input', function() {

            var v = $(this).val();
            var max = $(this).attr('max');

            if (parseInt(v) > parseInt(max)) {
                $(this).val(max);
            }
        });
    </script>
@endpush
