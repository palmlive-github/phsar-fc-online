<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ \App\CPU\translate('Express Delivery') }}</title>
    <style>
        body {
            background-color: #FFFFFF;
            padding: 0;
            margin: 0;
        }

        table {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        table td,
        table th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .text-center {
    text-align: center !important;
}
    .btn {
        display: inline-block;
        font-weight: normal;
        color: #fff;
        width: 100%;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: #0313f4;
        border: 1px solid #0313f4;
        padding: 0.625rem 1.375rem;
        font-size: .9375rem;
        line-height: 1.5;
        border-radius: 0.3125rem;
        transition: color 0.25s ease-in-out, background-color 0.25s ease-in-out, border-color 0.25s ease-in-out, box-shadow 0.2s ease-in-out;
    }
        table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #0313f4;
            color: white;
        }

        .pl-3,
        .px-3 {
            padding-left: 1rem !important;
        }

        .pr-3,
        .px-3 {
            padding-right: 1rem !important;
        }

        .border {
            border: 1px solid #e3e9ef !important;
        }

        .justify-content-between {
            -ms-flex-pack: justify !important;
            justify-content: space-between !important;
        }

        .form-row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -5px;
            margin-left: -5px;
        }

        .col {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -ms-flex-positive: 1;
            flex-grow: 1;
            min-width: 0;
            max-width: 100%;
        }

        .col-auto {
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: 100%;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #4b566b;
        }

        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #e3e9ef;
        }

        .table thead th {
            vertical-align: middle;
            border-bottom: 2px solid #e3e9ef;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #e3e9ef;
        }

        thead th,
        th {
            font-weight: 500;
        }

        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: middle;
            border-top: 1px solid #e3e9ef;
        }

    </style>

</head>

<body style="background-color: #FFFFFF; padding: 0; margin: 0;">
    <table class="table border-0">
        <tbody>
            <tr>
                <td>
                    <div class="form-row mb-2 justify-content-between">
                        <div class="col">
                            <p class="mb-0">
                                {{ \App\CPU\translate('Date') }} :
                                {{ $express->created_at->translatedFormat('d-M-Y') }}
                            </p>

                            <p class="mb-0">
                                {{ \App\CPU\translate('From') }} :
                                {{ \App\CPU\translate($express->delivery->from) }}
                                →
                                {{ \App\CPU\translate($express->delivery->to) }}
                            </p>
                            <p class="mb-0">
                                {{ \App\CPU\translate('Sender') }} :

                                {{ $express->delivery->sender_info['name'] }}

                            </p>
                            <p class="mb-0">
                                {{ \App\CPU\translate('Receiver') }} :

                                {{ $express->delivery->receiver_info['name'] }}

                            </p>
                            <p class="mb-0">
                                {{ \App\CPU\translate('Receiver Phone') }} :
                                {{ $express->delivery->receiver_info['phone']['full'] }}
                            </p>
                            <p class="mb-0">
                                {{ \App\CPU\translate('Receiver Address') }} :
                            <div class="border px-3" style="white-space: pre-line;">
                                {!! $express->delivery->receiver_info['address'] !!}
                            </div>
                            @if ($express->delivery->address_image)
                                <div>
                                    <img src="{{ $express->delivery->address_image }}" alt="" style="width: 300px;
                                    height: 200px;
                                    object-fit: contain;" class="border">
                                </div>
                            @endif


                            </p>




                        </div>
                        <div class="col-auto">
                            <p class="mb-0">
                                {{ \App\CPU\translate('Price') }} :
                                {{ \App\CPU\Helpers::currency_converter($express->price) }}
                            </p>
                            <p class="mb-0">
                                {{ \App\CPU\translate('Status') }} :
                                {{ \App\CPU\translate($express->status) }}
                            </p>
                            @if ($express->payment_image)
                                <p class="mb-0">
                                    {{ \App\CPU\translate('Payment Image') }} :

                                <div>
                                    <img src="{{ $express->payment_image }}" alt="" style="width: 300px;
                                height: 300px;
                                object-fit: contain;" class="border">
                                </div>

                                </p>
                            @endif

                        </div>
                    </div>

                    <table class="table table-bordered">

                        <head>

                            <tr>
                                <th width=1>{{ \App\CPU\translate('Id') }}</th>
                                <th>{{ \App\CPU\translate('name') }}</th>
                                <th>{{ \App\CPU\translate('length') }}</th>
                                <th>{{ \App\CPU\translate('height') }}</th>
                                <th>{{ \App\CPU\translate('width') }}</th>
                                <th>{{ \App\CPU\translate('weight') }}</th>
                                <th>{{ \App\CPU\translate('unit') }}</th>
                                <th>{{ \App\CPU\translate('price') }}</th>
                            </tr>
                        </head>
                        <tbody>

                            @foreach ($express->products as $j => $product)
                                <tr>
                                    <td class="text-center">{{ $j + 1 }}</td>
                                    <td class="">
                                        <div>
                                            <img class="border"
                                                style="width: 50px;height:50px;object-fit:contain;"
                                                onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                                src="{{ $product->image }}" alt="">

                                            {{ $product->name }}
                                        </div>
                                    </td>
                                    <td> {{ $product->length }}</td>
                                    <td> {{ $product->height }}</td>
                                    <td> {{ $product->width }}</td>
                                    <td> {{ $product->weight }}</td>
                                    <td>{{ \App\CPU\translate($product->unit) }} </td>
                                    <td>{{ \App\CPU\Helpers::currency_converter($product->price) }}</td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>

    </table>
    @if ($for =='user')
        <a class="btn" href="{{ route('express.delivery.show',$express->id) }}">{{\App\CPU\translate('View')  }}</a>
    @else
         <a class="btn" href="{{ route('admin.express-delivery.edit', $express->express_delivery_id) }}">{{\App\CPU\translate('Go to Edit')  }}</a>
    @endif
  
</body> 

</html>
