@extends('layouts.front-end.app')

@section('title',\App\CPU\translate('FAQ'))

@push('css_or_js')
<meta property="og:image" content="{{asset('storage/company')}}/{{$web_config['web_logo']->value}}" />
<meta property="og:title" content="FAQ of {{$web_config['name']->value}} " />
<meta property="og:url" content="{{env('APP_URL')}}">
<meta property="og:description" content="{!! substr($web_config['about']->value,0,100) !!}">

<meta property="twitter:card" content="{{asset('storage/company')}}/{{$web_config['web_logo']->value}}" />
<meta property="twitter:title" content="FAQ of {{$web_config['name']->value}}" />
<meta property="twitter:url" content="{{env('APP_URL')}}">
<meta property="twitter:description" content="{!! substr($web_config['about']->value,0,100) !!}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<style>
    .headerTitle {
        font-size: 25px;
        font-weight: 700;
        margin-top: 2rem;
    }

    #faq .card .card-header .btn-header-link:after {
        content: "\f107";
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
        float: right;
    }


    #faq .card .card-header .btn-header-link.collapsed:after {
        content: "\f106";
    }

    #faq .card .card-header .btn-header-link {
        color: #fff;
        display: block;
        text-align: left;
        color: #222;
        padding: 20px;
    }
</style>
@endpush

@section('content')
<!-- Page Title-->
<div class="container rtl">
    <div class="row">
        <div class="col-md-12 sidebar_heading text-center mb-2">
            <h1 class="h3  mb-0 folot-left headerTitle">{{\App\CPU\translate('frequently_asked_question')}}</h1>
        </div>
    </div>
    <hr>
</div>

<!-- Page Content-->
<div class="container py-2 rtl">
    <div class="row">
        <div class="col-md-8">
            <div class="accordion" id="faq">
                @foreach ($helps as $help)
                <div class="card">
                    <div class="card-header" id="faqhead-{{  $help->id }}">
                        <a href="#" class="btn-lg btn-header-link w-100 text-left text-accent" data-toggle="collapse"
                            data-target="#faq-{{  $help->id }}" aria-expanded="true"
                            aria-controls="faq-{{  $help->id }}">{{ $help->question }}</a>
                    </div>
                    <div id="faq-{{  $help->id }}" class="collapse {{ $loop->first?'show':null }}"
                        aria-labelledby="faqhead-{{  $help->id }}" data-parent="#faq">
                        <div class="card-body">
                            {{ $help->answer }}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <img
                src="https://images01.nicepage.com/a1389d7bc73adea1e1c1fb7e/6299863bfc6e59dc973b2eca/pexels-photo-4247766.jpeg">
        </div>
    </div>
</div>

@endsection
