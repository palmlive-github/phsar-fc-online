@extends('layouts.front-end.app')

@section('title',auth('customer')->user()->f_name.' '.auth('customer')->user()->l_name)


@section('content')
    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{Session::get('direction') === "rtl" ? 'right' : 'left'}} headerTitle">{{\App\CPU\translate('referral_code')}}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
         style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
        <div class="row">
            <!-- Sidebar-->
        @include('web-views.partials._profile-aside')
        <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="card box-shadow-sm">
                    <div class="card-body {{Session::get('direction') === "rtl" ? 'mr-3' : 'ml-3'}}">
                        <form class="mt-3" action="{{route('account-refcode')}}" method="post"
                              enctype="multipart/form-data">

                                @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="refcode">{{\App\CPU\translate('code')}} ({{\App\CPU\translate('6 digit')}})</label>

                                            <input type="number" class="form-control" id="refcode" name="refcode" placeholder="123456" {{ $customerDetail['refcode']?'readonly' : null }}
                                                   value="{{old('refcode',$customerDetail['refcode'])}}" maxlength="6" minlength="6" required>
                                                @error('refcode')
                                                    <div class="text-danger text-sm">{{ $message }}</div>
                                                @enderror

                                                   @if (!$customerDetail['refcode'])
                                                    <button type="button" class="btn btn-sm border mt-2" id="generate">{{\App\CPU\translate('generate')}} </button>
                                                   @endif

                                        </div>
                                    </div>
                                    @if (!$customerDetail['refcode'])
                                        <button type="submit" class="btn btn-primary float-{{Session::get('direction') === "rtl" ? 'left' : 'right'}}">{{\App\CPU\translate('Make Code')}}</button>
                                        <div class="notify">
                                            {{\App\CPU\translate('This code can not be change after you make it')}}
                                        </div>
                                    @endif


                        </form>
                    </div>
                    <div class="card-body {{Session::get('direction') === "rtl" ? 'mr-3' : 'ml-3'}}">
                        <h4 class="h4 float-{{Session::get('direction') === "rtl" ? 'right' : 'left'}} headerTitle">{{\App\CPU\translate('My referral')}}</h4>
                        <table class="table table-bordered">
                            <thead>
                                <th width=1>{{\App\CPU\translate('Id')}}</th>
                                <th>{{\App\CPU\translate('Name')}}</th>
                                <th>{{\App\CPU\translate('Date')}}</th>
                            </thead>
                            <tbody>
                                @foreach ($myrefs as $key => $ref)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>
                                        <div>
                                            <img src="" width="50px" class="border rounded-circle"
                                            onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                            src="{{asset('storage/profile')}}/{{$ref['image']}}">
                                            {{ $ref['f_name'] }} {{ $ref['l_name'] }}
                                        </div>
                                    </td>
                                    <td>{{ $ref->created_at->translatedFormat('d-M-Y') }}</td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                        {!! $myrefs->links() !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(`#generate`).click(function(e){
            e.preventDefault();
            var code = Math.floor(100000 + Math.random() * 900000);
            $(`#refcode`).val(code);
        })
        $(`#refcode`).on('input',function(){
            if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
        });
    </script>
@endpush
