<div class="form-group express_form">
    <div class="form-row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>

                    <tr>
                        <th colspan="6">{{ \App\CPU\translate('Select products') }} <span style="color:red">*</span></th>
                        <th colspan="2">
                            <input data-toggle="search" type="text" class="form-control"
                                placeholder=" {{ \App\CPU\translate('Search') }}">
                        </th>
                    </tr>
                    <tr>
                        <th width="1">


                                <div class="custom-control custom-checkbox">

                                    <input type="checkbox" class="custom-control-input"
                                        id="ck-all">
                                    <label class="custom-control-label text-muted"
                                        for="ck-all"><span>{{ \App\CPU\translate('All') }}</span></label>
                                </div>

                        </th>
                        <th>{{ \App\CPU\translate('Name') }}</th>
                        <th>{{ \App\CPU\translate('length') }}</th>
                        <th>{{ \App\CPU\translate('height') }}</th>
                        <th>{{ \App\CPU\translate('width') }}</th>
                        <th>{{ \App\CPU\translate('weight') }}</th>
                        <th>{{ \App\CPU\translate('unit') }}</th>
                        <th>{{ \App\CPU\translate('price') }}</th>
                    </tr>

                </thead>
                <tbody class="droppable-area2">
                    @foreach ($products as $key => $product)
                        <tr class="bg-white" data-id="{{ $product->id }}">
                            <td>

                                    <div class="custom-control custom-checkbox">

                                        <input type="checkbox" class="custom-control-input"
                                            id="product-{{ $product->id }}"
                                            name="express_products[]"
                                            data-product='{{ $product }}'
                                            value="{{ $product->id }}"
                                            {{ in_array($product->id, old('express_products', $express_products))? 'checked': null }}>
                                        <label class="custom-control-label text-muted"
                                            for="product-{{ $product->id }}"><span>{{ $key+1 }}</span></label>
                                    </div>

                            </td>
                            <td>
                                <img  class="border" style="object-fit: cover;height:50px;width:50px"
                                    onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                    src="{{ $product->image }}" alt="">
                                {{ $product->name }}
                            </td>
                            <td> {{ $product->length }}</td>
                            <td> {{ $product->height }}</td>
                            <td> {{ $product->width }}</td>
                            <td> {{ $product->weight }}</td>
                            <td>{{ \App\CPU\translate($product->unit) }} </td>
                            <td>{{ \App\CPU\Helpers::currency_converter($product->price) }}</td>
                        </tr>
                    @endforeach
                </tbody>

                <tbody>
                    <tr>
                        <td colspan="5"></td>
                        <td>{{ $products->sum('weight') }}</td>
                        <td>{{ \App\CPU\translate( $products->first()->unit) }}</td>
                        <td>{{ \App\CPU\Helpers::currency_converter($products->sum('price')) }}</td>
                    </tr>
                </tbody>
            </table>

            @if(count($products)==0)
            <div class="text-center p-4">
                <img class="mb-3" src="{{asset('assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                <p class="mb-0">{{\App\CPU\translate('no_data_found')}}</p>
            </div>
        @endif
        </div>
    </div>
</div>
