@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .navbar-sticky{
            position: relative !important;
        }
    </style>
@endpush

@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('Express Delivery') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="card box-shadow-sm">
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">


                            @if(count($express)==0)
                            <div class="text-center p-4">
                                <img class="mb-3" src="{{asset('assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                                <p class="mb-0">{{\App\CPU\translate('no_data_found')}}</p>
                            </div>
                        @else

                        <div class="mb-3">
                            <form action="{{ url()->current() }}" method="GET">
                                <div class="input-group input-group-merge input-group-flush">
                                    <input id="date" type="text" name="date" class="form-control" placeholder="{{ \App\CPU\translate('Date')}}"
                                    value="{{ request('date') }}" autocomplete="off">
                                            @php
                                                $express_status = App\CPU\Helpers::express_status();
                                                unset($express_status[0]);
                                            @endphp
                                    <select name="status" class="form-control">
                                        <option value="">{{ \App\CPU\translate('All') }}</option>
                                        @foreach ($express_status as $status)
                                        <option {{ request('status') == $status ? 'selected':null }} value="{{ $status }}">{{ \App\CPU\translate($status)}}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit"
                                        class="btn btn-primary ml-2">{{ \App\CPU\translate('search') }}</button>
                                </div>
                            </form>
                        </div>
                    <table class="table table-bordered">
                        <thead>
                            <th width=1>{{ \App\CPU\translate('Id') }}</th>
                            <th>{{ \App\CPU\translate('Product') }}</th>
                        </thead>
                        <tbody>
                            @foreach ($express as $key => $row)
                                <tr>
                                    <td>{{ $key + 1 }}</td>

                                    <td>
                                            <div class="form-row mb-2 justify-content-between">
                                                <div class="col">
                                                    <p class="mb-0">
                                                        {{ \App\CPU\translate('Date') }} :
                                                        {{ $row->created_at->translatedFormat('d-M-Y') }}
                                                    </p>

                                                    <p class="mb-0">
                                                        {{ \App\CPU\translate('From') }} :
                                                        {{ \App\CPU\translate($row->delivery->from) }}
                                                        →
                                                        {{ \App\CPU\translate($row->delivery->to) }}
                                                    </p>
                                                        @if ($row->delivery->from == 'khmer')
                                                        <p class="mb-0"> {{ \App\CPU\translate('Sender') }} :  <span data-toggle="tooltip" data-placement="top" data-html="true" title="">
                                                            @if ($row->delivery->sender_id == auth('customer')->id())
                                                                <span class="text-primary">{{ \App\CPU\translate('You') }}</span>
                                                            @else
                                                                {{ $row->delivery->sender_info['name']}}
                                                            @endif
                                                          </span>
                                                        </p>

                                                         @endif

                                                    <p class="mb-0">
                                                        {{ \App\CPU\translate('Receiver') }} :    <span data-toggle="tooltip" data-placement="top" data-html="true" title=" {{ nl2br($row->delivery->receiver_info['address'])}}">
                                                            @if ($row->delivery->receiver_id == auth('customer')->id())
                                                                <span class="text-primary">{{ \App\CPU\translate('You') }}</span>
                                                            @else
                                                                {{ $row->delivery->receiver_info['name']}}
                                                            @endif
                                                          </span>
                                                    </p>


                                                </div>
                                                <div class="col-auto">
                                                    <p class="mb-0">
                                                        {{\App\CPU\translate('Price') }} : {{ \App\CPU\Helpers::currency_converter($row->price)  }}
                                                    </p>
                                                    <p class="mb-0">
                                                        {{\App\CPU\translate('Status') }}  : {{\App\CPU\translate($row->status)  }}
                                                    </p>


                                                    <p class="my-3">
                                                        <a  class="btn btn-sm btn-info" href="{{ route('express.delivery.show',$row->id) }}"> {{\App\CPU\translate('View') }}</a>

                                                       @if (in_array($row->status ,['pending','cancel']))
                                                       @if ($row->delivery->from == 'khmer')
                                                        <a class="btn btn-primary btn-sm edit" style="cursor: pointer;"
                                                                href="{{route('express.delivery.edit.khmer-thai',$row)}}">
                                                                <i class="tio-edit"></i>{{ \App\CPU\translate('Edit')}}
                                                            </a>
                                                       @else
                                                        <a class="btn btn-primary btn-sm edit" style="cursor: pointer;"
                                                                href="{{route('express.delivery.edit.thai-khmer',$row)}}">
                                                                <i class="tio-edit"></i>{{ \App\CPU\translate('Edit')}}
                                                            </a>
                                                       @endif

                                                        <a class="btn btn-danger btn-sm delete" style="cursor: pointer;"
                                                        id="{{$row['id']}}">
                                                            <i class="tio-add-to-trash"></i>{{ \App\CPU\translate('Delete')}}
                                                        </a>
                                                       @endif

                                                    </p>
                                                </div>
                                            </div>
                                    </td>


                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {!! $express->appends(request()->except('page'))->links() !!}
                        @endif

                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection
@push('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $('input[name="date"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: "YYYY/MM/DD",
            cancelLabel: 'Clear'
        }
    });
    $('input[name="date"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
    });

    $('input[name="date"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $(`button#more`).click(function(e){
        e.preventDefault();
        var target = $(this).data('target');
        if($(target).hasClass(`d-none`)){
            $(target).removeClass(`d-none`);
            $(this).text(`{{ \App\CPU\translate('Hide')}}`);
        }else{
            $(target).addClass(`d-none`);
            $(this).text(`{{ \App\CPU\translate('More')}}`);
        }

    });


        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            Swal.fire({
                title: '{{\App\CPU\translate('Are_you_sure')}}?',
                text: "{{\App\CPU\translate('You_will_not_be_able_to_revert_this')}}!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{\App\CPU\translate('Yes')}}, {{\App\CPU\translate('delete_it')}}!'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{route('express.delivery.destroy','')}}/"+id,
                        method: 'DELETE',
                        data: {id: id},
                        success: function () {
                            toastr.success('{{\App\CPU\translate('Express_deleted_Successfully.')}}');
                            location.reload();
                        }
                    });
                }
            })
        });


</script>
@endpush
