@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .navbar-sticky{
            position: relative !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .iti--allow-dropdown {
            width: 100%;
        }
                .select2-container {
                width: 100% !important;
            }

            .select2-container .select2-selection--single {
                height: 40px;
            }

            .select2-container--default .select2-selection--single .select2-selection__rendered {
                line-height: 40px;
            }

            .select2-container--default .select2-selection--single .select2-selection__arrow {
                height: 40px;
            }

    </style>
@endpush
@php
$units = App\CPU\Helpers::units();
@endphp
@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('Express') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content Row -->
               <section class="col-lg-9 col-md-9">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ \App\CPU\translate('express_delivery_form') }}
                    ({{\App\CPU\translate('khmer') }} &rightarrow; {{\App\CPU\translate('thai') }})
                </div>
                <div class="card-body" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                    <form action="{{ route('express.delivery.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group express_form" id="product-form">
                            <table class="table table-bordered" id="product">
                                <thead>
                                    <tr>
                                        <th colspan="5">{{ \App\CPU\translate('Create products') }}</th>
                                    </tr>
                                   <tr>
                                    <th width="100">{{ \App\CPU\translate('Image') }}</th>
                                    <th>{{ \App\CPU\translate('Name') }}</th>
                                    <th >{{ \App\CPU\translate('Dimensions') }} ({{ \App\CPU\translate('Weight') }} <span style="color:red">*</span>, {{ \App\CPU\translate('Unit') }} <span style="color:red">*</span>)</th>
                                    <th width="120">{{ \App\CPU\translate('Price') }}</th>
                                    <th width="1"></th>
                                   </tr>
                                </thead>
                                <tbody>
                                    <tr id="clone" style="display: none">
                                        <td>
                                            <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                                            <input id="image" class="d-none" type="hidden" __name__="products[__index__][image]" class="form-control">
                                            <input id="input-image" class="d-none" type="file"
                                                __name__="products[__index__][image]" class="form-control"
                                                accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                        </td>
                                        <td>
                                            <input type="text" __name__="products[__index__][name]"
                                                class="form-control">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" __name__="products[__index__][length]"
                                                    class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Length') }}">
                                                <input type="number" __name__="products[__index__][height]"
                                                    class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Height') }}">
                                                <input type="number" __name__="products[__index__][width]"
                                                    class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Width') }}">
                                                <input type="number" __name__="products[__index__][weight]"
                                                    class="form-control" data-toggle="calc"
                                                    placeholder="{{ \App\CPU\translate('Weight') }}">


                                                <select __name__="products[__index__][unit]" id=""
                                                    class="form-control">
                                                    @foreach ($units as $item)
                                                    <option value="{{ $item }}">
                                                        {{ \App\CPU\translate($item) }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </td>
                                       <td>
                                        <input type="number" __name__="products[__index__][price]"
                                        class="form-control" data-toggle="calc" placeholder="$"
                                        placeholder="{{ \App\CPU\translate('Price') }}">

                                       </td>
                                        <td>
                                            <div class="d-flex">
                                                <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger" id="del">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @if (old('products'))
                                        @foreach (old('products') as $key => $product)
                                            <tr>
                                                <td>
                                                    <img onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                    src="{{ $product['image'] }}"
                                                    style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                                                    <input id="image"  class="d-none" type="hidden" name="products[{{ $key }}][image]"  value="{{ $product['image'] }}" class="form-control">
                                                    <input id="input-image" class="d-none" type="file" class="form-control"
                                                        accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                </td>
                                                <td>
                                                    <input type="text" name="products[{{ $key }}][name]" class="form-control"
                                                        value="{{ $product['name'] }}">
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="number"
                                                            name="products[{{ $key }}][length]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('Length') }}"
                                                            value="{{ $product['length'] }}">
                                                        <input type="number"
                                                            name="products[{{ $key }}][height]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('Height') }}"
                                                            value="{{ $product['height'] }}">
                                                        <input type="number"
                                                            name="products[{{ $key }}][width]"
                                                            class="form-control"
                                                            placeholder="{{ \App\CPU\translate('Width') }}"
                                                            value="{{ $product['width'] }}">
                                                            <input type="number"
                                                            name="products[{{ $key }}][weight]"
                                                            class="form-control" data-toggle="calc"
                                                            placeholder="{{ \App\CPU\translate('Weight') }}"
                                                            value="{{ $product['weight'] }}">

                                                        <select name="products[{{ $key }}][unit]" id=""
                                                            class="form-control">
                                                            @foreach ($units as $item)
                                                                <option
                                                                    {{ $product['unit'] == $item ? 'selected' : null }}
                                                                    value="{{ $item }}">
                                                                    {{ \App\CPU\translate($item) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <input type="text" name="products[{{ $key }}][price]" class="form-control" data-toggle="calc"
                                                        value="{{ $product['price'] }}">
                                                </td>

                                                <td>
                                                    <div class="d-flex">
                                                        <button type="button" class="btn btn-sm btn-success mr-1"
                                                            id="add">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-danger" id="del">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else

                                    <tr>
                                        <td>
                                            <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                style="object-fit: cover" height="50px" width="50px" id="trigger-image">
                                            <input id="image"  class="d-none" type="hidden" name="products[0][image]"  value="" class="form-control">
                                            <input id="input-image" class="d-none" type="file"class="form-control"
                                                accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                        </td>
                                        <td>
                                            <input type="text" name="products[0][name]" class="form-control">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" name="products[0][length]" class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Length') }}">
                                                <input type="number" name="products[0][height]" class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Height') }}">
                                                <input type="number" name="products[0][width]" class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Width') }}">
                                                    <input type="number" name="products[0][weight]" class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Weight') }}" data-toggle="calc">

                                                <select name="products[0][unit]" id="" class="form-control">

                                                    @foreach ($units as $item)
                                                        <option value="{{ $item }}">
                                                            {{ \App\CPU\translate($item) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="number" name="products[0][price]"
                                            class="form-control" data-toggle="calc" placeholder="$"
                                            placeholder="{{ \App\CPU\translate('Price') }}">

                                           </td>

                                        <td>
                                            <div class="d-flex">
                                                <button type="button" class="btn btn-sm btn-success mr-1"
                                                    id="add">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger" id="del">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>


                        <div class="form-group express_form" id="delivery-form">

                            <div class="input-group mb-3 d-none">
                                @php
                                 $locations = $locations = App\CPU\Helpers::locations();

                                @endphp
                                <select name="from" class="form-control">
                                    @foreach ($locations as $item)
                                    <option value="{{ $item }}"
                                    {{ old('from','khmer') == $item ? 'selected' : null }}>
                                        {{ \App\CPU\translate($item) }}
                                    </option>
                                    @endforeach

                                </select>
                                <button type="button" class="bg-transparent border-0" data-toggle="switch-location">
                                    <span class="mr-2">
                                        <i class="text-primary">⇄</i>
                                    </span>
                                </button>
                                <select  name="to" class="form-control">
                                    @foreach ($locations as $item)
                                    <option value="{{ $item }}" {{ old('to','thai') == $item ? 'selected' :
                                        null }}>
                                        {{ \App\CPU\translate($item) }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <table class="table table-bordered" id="delivery">
                                <thead>

                                    <th>{{ \App\CPU\translate('Receiver') }}</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <table class="table table-bordered" id="receiver">
                                                <tr>

                                                    <td colspan="2">
                                                        <div class="form-group express_form" id="receiver-form">
                                                            <div class="form-group">
                                                                <select name="receiver[id]" class="form-control" data-placeholder="{{ \App\CPU\translate('Search by name, referral code') }}">
                                                                    @foreach (session('users',[]) as $user)
                                                                    <option value="{{ $user->id }}"
                                                                        {{ old('receiver.id') == $user->id ? 'selected' : null }}>
                                                                        {{ $user->name??$user->f_name.' '.$user->l_name }} - ({{ $user->refcode }})
                                                                    </option>
                                                                @endforeach</select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width=1>{{ \App\CPU\translate('Name') }} <span style="color:red">*</span> </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="receiver[name]" value="{{ old('receiver.name') }}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width=1>{{ \App\CPU\translate('Phone') }} <span style="color:red">*</span></td>
                                                    <td>
                                                        <input type="text" class="form-control" name="receiver[phone][main]" id="phone" value="{{ old('receiver.phone.full') }}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width=100>{{ \App\CPU\translate('Address') }} <span style="color:red">*</span></td>
                                                    <td>
                                                        <textarea class="form-control" name="receiver[address]" cols="30" rows="2">{{ old('receiver.address') }}</textarea>
                                                           <div class="mt-2">
                                                               <label for="" class="form-control-label">
                                                                {{ \App\CPU\translate('You can upload address as image') }}
                                                               </label>
                                                            <input id="input-image"  type="file" class="form-control"
                                                            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                            <img src="{{old('address_image') }}" class="border rounded" onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'"
                                                            style="object-fit: contain;width: 100%;height:150px" id="trigger-image">
                                                            <input id="image" class="d-none" type="hidden" name="address_image"
                                                                value="{{ old('address_image') }}" class="form-control">
                                                           </div>


                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>




                                <div>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>{{ \App\CPU\translate('Total weight') }}</td>
                                            <td> <span id="total_weight">0</span>  <span>{{ \App\CPU\translate( $units[0]) }}</span> </td>
                                        </tr>
                                        <tr>
                                            <td>{{ \App\CPU\translate('Total price') }}</td>
                                            <td id="total_price" class="font-size-xl"> {{ \App\CPU\Helpers::currency_converter(0) }} </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        @foreach ($payment_method as $k => $payment)
                                                            <tr>
                                                                <td width="100px">
                                                                    <div class="custom-control custom-radio">
                                                                        <input class="custom-control-input" type="radio"
                                                                            {{ $loop->first ? 'checked' : null }} name="payment"
                                                                            value="{{ $payment->id }}"
                                                                            id="k-{{ $k }}">
                                                                        <label for="k-{{ $k }}"
                                                                            class="custom-control-label">
                                                                            <img width="100%"
                                                                                src="{{ $payment['image'] }}">
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <label for="k-{{ $k }}">
                                                                        {{ $payment['name'] }}
                                                                        <br>
                                                                        {{ $payment['account_name'] }}
                                                                        <br>
                                                                        {{ $payment['account_number'] }}
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ \App\CPU\translate('Upload Payment') }} <span style="color:red">*</span></td>
                                            <td>
                                                <input id="input-image"  type="file" class="form-control"
                                                accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                <img src="{{  old('payment_image') }} " onerror="this.src='{{ asset('assets/front-end/img/image-place-holder.png') }}'" class="border rounded"
                                                style="object-fit: contain;width: 100%;height:250px" id="trigger-image">
                                                <input id="image" class="d-none" type="hidden" name="payment_image"
                                                    value="{{ old('payment_image') }}" class="form-control">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ \App\CPU\translate('Note') }}</td>
                                            <td>
                                                <textarea class="form-control" name="noted"
                                                    cols="30"
                                                    rows="2">{{ old('noted') }}</textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                 <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('submit') }}</button>


                    </form>
                </div>
            </div>
        </div>
    </div>
               </section>
        </div>
    </div>

@endsection

@push('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('js/compress.js') }}"></script>
<script>
    $(`[data-toggle="switch-location"]`).each(function() {
        $(this).click(function(e) {
            e.preventDefault();
            var fromVal = $(`[name="from"]`).val();
            var toVal = $(`[name="to"]`).val();
            $(`[name="from"]`).val(toVal).trigger('change');
            $(`[name="to"]`).val(fromVal).trigger('change');
        });
    });

var users = {};

   @if(session('users'))
        @foreach(session('users') as $user)
        users[`{{ $user->id }}`] = {!! $user !!};
        @endforeach
   @endif
   const phoneInput = [];
    var Express = {
        init: () => {
            Express.delivery.init();
            Express.product.init();
            Express.phonenumber();
        },
        phonenumber : function($el = $("input#phone")){
            $el.each(function(i, el) {
                     phoneInput[i] = window.intlTelInput(el, {
                        separateDialCode: true
                        , hiddenInput: "full"
                        , initialCountry: "auto"
                        , geoIpLookup: function(success) {
                            success('KH')
                        }
                        , utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js"
                    , });
                });
        },
        phonenumberset : function (id,phone) {
            $.each(phoneInput,(i,el)=>{

                if(id == el.id){
                    el.setNumber(phone);
                 }
            });
        }
        , delivery: {
            init: () => {

                $(`[name="sender[id]"],[name="receiver[id]"]`).each(function() {
                    $(this).select2({
                        minimumInputLength: 0
                        , ajax: {
                            url: `{{ route('express.searchuser') }}`
                            , dataType: 'json'
                            , type: 'GET'
                            , delay: 250
                            , data: function(params) {
                                var query = {
                                    search: params.term
                                    , page: params.page || 1
                                }
                                return query;
                            }
                            , processResults: function(res, params) {
                                params.page = params.page || 1;
                                return results = {
                                    results: res.data.map(function(data) {
                                        users[data.id] = data;
                                        return {
                                            id: data.id,
                                             text: `${data.name?? `${data.f_name} ${data.l_name}`} (${data.refcode})`,
                                             data : data

                                        , }
                                    })
                                    , pagination: {
                                        more: params.page < res.last_page
                                    }
                                };

                            }
                            , cache: true
                        , }
                    });
                    $(`[name="receiver[id]"]`)
                        .val(`0`)
                        .trigger('change');


                    $(this).on('change', function(e) {

                        var id = $(this).val();
                        var res =  users[id];
                        var address =  '';
                        if(res && res.shippings.length){
                            address =  res.shippings.map(function(d){
                            return `Address Type : ${d.address_type}\n Contact Name : ${d.contact_person_name}\n Contact Phone : ${d.phone}\n Contact Address : ${d.address}\n ------------------------\n`;
                        });
                        }

                        $(this).parents('tbody').eq(0).find(`tr`).eq(1).find(`[name]`)
                                        .val( `${res.name ?? `${res.f_name} ${res.l_name}`}`);
                                   $(this).parents('tbody').eq(0).find(`tr`).eq(2).find(`[name]`)
                                        .val(`${res.phone}`);
                                   $(this).parents('tbody').eq(0).find(`tr`).eq(3).find(`[name]:first`)
                                        .val(`${address??'N/A'}`);


                        Express.phonenumberset($(this).parents('tbody').eq(0).find(`#phone`).data('intlTelInputId'),res.phone);



                    });
                });




                if (`{{ old('receiver.id') }}`) {
                    $(`[name="receiver[id]"]`)
                        .val(`{{ old('receiver.id') }}`)
                        .trigger('change');
                }

            }
        },
        product: {
                $append: $(`table#product tbody`),
                $clone: $(`table#product tbody tr#clone`).clone().removeAttr('style').removeAttr('id'),
                $add: `table#product tbody #add`,
                $del: `table#product tbody #del`,
                datetime: () => {
                    return (new Date()).getTime();
                },
                init: () => {
                    Express.product.calc();
                    $( document).on('input',`[name="express_products[]"],[data-toggle="calc"]`, function() {
                        Express.product.calc();
                    });
                    $(document).on('click', Express.product.$add, function(e) {
                        e.preventDefault();
                        var index = Express.product.datetime();
                        var clone = Express.product.$clone.prop('outerHTML').replaceAll('__name__', 'name');
                        clone = clone.replaceAll('__index__', index);
                        $(this).parents('tr').after(clone);
                    });
                    $(document).on('click', Express.product.$del, function(e) {
                        e.preventDefault();
                        if (Express.product.$append.find(`tr:not(#clone)`).length > 1) {
                            $(this).parents('tr').remove();
                        }

                    });


                    $( document).on('input',`[data-toggle="search"]`, function() {
                        var search = $(this).val();
                        var $table = $(this).parents('table');
                        var value = $(this).val().toLowerCase();
                        $table.find("tbody>tr").filter(function() {
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                        });

                    });

                    $( document).on('input',`#ck-all`, function() {
                        var checked = $(this).prop('checked');
                        $(`[name="express_products[]"]`).prop('checked',checked);
                        Express.product.calc();
                    });
                    $( document).on('input',`[name="express_products[]"]`, function() {

                        Express.product.calc();

                    });

                    $(document).on('click', `img#trigger-image`, function() {
                        $(this).parent().find('input').trigger('click');
                    });
                    $(document).on('input', 'input#input-image', function(e) {
                        if(e.target.files.length){
                            $(e.target).parent().find('img').attr('src', `{{ asset('assets/front-end/img/loader_.gif') }}`);
                            var compressor = new window.Compress();
                            compressor.compress([...e.target.files], {
                                size: 4
                                , quality: 0.75
                            , }).then((output) => {
                                var v = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                                var formData = new FormData();
                                formData.append('_token', `{{ csrf_token() }}`)
                                formData.append('image', v)
                                 $.ajax({
                                    url: `{{ route('image') }}`
                                    , method: 'POST'
                                    , contentType: false
                                    , processData: false
                                    , data: formData
                                    , success: (res) => {
                                        $(e.target).parent().find('img').attr('src', res.url);
                                            if(res.status){
                                                $(e.target).parent().find('#image').val(res.url);
                                            }else{
                                                $(e.target).parent().find('#image').val('');
                                            }
                                            if(res.message){
                                                toastr.error(res.message);
                                            }
                                    }
                                });
                                $(e.target).val('');
                            });

                        }

                       // $(this).parent().find('img').attr('src', URL.createObjectURL(e.target.files[0]));
                    });

                },
                allchecked:  () =>{
                    var allchecked = false;
                    $(`[name="express_products[]"]`).each(function(){
                        var checked = $(this).prop('checked');
                        if(checked){
                            allchecked = true;
                        }else{
                            allchecked = false;
                            return false;

                        }
                    });
                    return allchecked;
                },
                calc: ()=>{
                    var total_price = 0;
                    var total_weight = 0;
                    $(`[name="express_products[]"]`).each(function() {
                        var product = $(this).data('product');
                        var checked = $(this).prop('checked');
                        if(checked){
                            total_price = eval( total_price+ parseFloat( product.price));
                            total_weight = eval(total_weight+ parseFloat(product.weight));
                        }

                    });
                    Express.product.$append.find(`tr:not(#clone)`).each(function(){
                        $(this).find(`[data-toggle="calc"]`).each(function(){
                            var val = $(this).val();
                            if(val){
                                if($(this).attr('name').includes("weight")){
                                    total_weight = eval(total_weight+ parseFloat(val));
                                }else if($(this).attr('name').includes("price")){
                                    total_price = eval( total_price+ parseFloat(val));
                                }
                            }
                        })
                    });

                    $.get(`{{ route('currency','') }}/${total_price}`).done(res=>{
                        $(`#total_price`).html(res);
                    })
                    $(`#total_weight`).html(total_weight);
                }
            }
    , };
    Express.init();

</script>
@endpush
