@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')

<link rel="stylesheet" href="/assets/back-end/vendor/icon-set/style.css">
<!-- CSS Front Template -->
<link rel="stylesheet" href="/assets/back-end/css/theme.minc619.css?v=1.0">
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .navbar-sticky{
            position: relative !important;
        }
    </style>
@endpush

@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('Express Products') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                {{ \App\CPU\translate('express_product_form') }}
                            </div>
                            <div class="card-body"
                                style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
                                <form action="{{ route('express.product.store') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf


                                    <div class="form-group express_form" id="product-form">
                                        <table class="table table-bordered" id="product">
                                            <thead>
                                                <th width="100">{{ \App\CPU\translate('Image') }}</th>
                                                <th>{{ \App\CPU\translate('Name') }}</th>
                                                <th width="400">{{ \App\CPU\translate('Dimensions') }}</th>
                                                <th width="1"></th>
                                            </thead>
                                            <tbody>
                                                <tr id="clone" style="display: none">
                                                    <td>
                                                        <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                            style="object-fit: cover" height="60px" width="60px" id="trigger-image">
                                                        <input id="image" class="d-none" type="hidden" __name__="products[__index__][image]" class="form-control">
                                                        <input id="input-image" class="d-none" type="file"
                                                            __name__="products[__index__][image]" class="form-control"
                                                            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    </td>
                                                    <td>
                                                        <input type="text" __name__="products[__index__][name]"
                                                            class="form-control">
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="number" __name__="products[__index__][length]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Length') }}">
                                                            <input type="number" __name__="products[__index__][height]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Height') }}">
                                                            <input type="number" __name__="products[__index__][width]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('weight') }}">
                                                                <input type="number" __name__="products[__index__][weight]"
                                                                class="form-control"
                                                                placeholder="{{ \App\CPU\translate('weight') }}">
                                                                @php
                                                                $units = App\CPU\Helpers::units();
                                                            @endphp
                                                            <select __name__="products[__index__][unit]" id=""
                                                                class="form-control">

                                                                @foreach ($units as $item)
                                                                <option value="{{ $item }}">
                                                                    {{ \App\CPU\translate($item) }}</option>
                                                            @endforeach
                                                            </select>
                                                        </div>
                                                    </td>



                                                    <td>
                                                        <div class="d-flex">
                                                            <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                                <i class="tio-add"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-danger" id="del">
                                                                <i class="tio-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @if (old('products'))
                                                    @foreach (old('products') as $key => $product)
                                                        <tr>
                                                            <td>
                                                                <img onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                                src="{{ $product['image'] }}"
                                                                style="object-fit: cover" height="60px" width="60px" id="trigger-image">
                                                                <input id="image"  class="d-none" type="hidden" name="products[{{ $key }}][image]"  value="{{ $product['image'] }}" class="form-control">
                                                                <input id="input-image" class="d-none" type="file" class="form-control"
                                                                    accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="products[{{ $key }}][name]" class="form-control"
                                                                    value="{{ $product['name'] }}">
                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="number"
                                                                        name="products[{{ $key }}][length]"
                                                                        class="form-control"
                                                                        placeholder="{{ \App\CPU\translate('Length') }}"
                                                                        value="{{ $product['length'] }}">
                                                                    <input type="number"
                                                                        name="products[{{ $key }}][height]"
                                                                        class="form-control"
                                                                        placeholder="{{ \App\CPU\translate('Height') }}"
                                                                        value="{{ $product['height'] }}">
                                                                    <input type="number"
                                                                        name="products[{{ $key }}][width]"
                                                                        class="form-control"
                                                                        placeholder="{{ \App\CPU\translate('Width') }}"
                                                                        value="{{ $product['width'] }}">

                                                                        <input type="number"
                                                                        name="products[{{ $key }}][weight]"
                                                                        class="form-control"
                                                                        placeholder="{{ \App\CPU\translate('Weight') }}"
                                                                        value="{{ $product['weight'] }}">
                                                                        @php
                                                                        $units = App\CPU\Helpers::units();
                                                                    @endphp
                                                                    <select name="products[{{ $key }}][unit]" id=""
                                                                        class="form-control">

                                                                        @foreach ($units as $item)
                                                                            <option {{  $product['unit']== $item  ? 'selected':null }} value="{{ $item }}">
                                                                                {{ \App\CPU\translate($item) }}</option>
                                                                        @endforeach
                                                                    </select>


                                                                </div>
                                                            </td>


                                                            <td>
                                                                <div class="d-flex">
                                                                    <button type="button" class="btn btn-sm btn-success mr-1"
                                                                        id="add">
                                                                        <i class="tio-add"></i>
                                                                    </button>
                                                                    <button type="button" class="btn btn-sm btn-danger" id="del">
                                                                        <i class="tio-delete"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else

                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                            style="object-fit: cover" height="60px" width="60px" id="trigger-image">
                                                        <input id="image"  class="d-none" type="hidden" name="products[0][image]"  value="" class="form-control">
                                                        <input id="input-image" class="d-none" type="file"class="form-control"
                                                            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="products[0][name]" class="form-control">
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="number" name="products[0][length]" class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Length') }}">
                                                            <input type="number" name="products[0][height]" class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Height') }}">
                                                            <input type="number" name="products[0][width]" class="form-control"
                                                                placeholder="{{ \App\CPU\translate('Width') }}">
                                                                <input type="number" name="products[0][weight]" class="form-control"
                                                                placeholder="{{ \App\CPU\translate('weight') }}">
                                                            @php
                                                                $units = App\CPU\Helpers::units();
                                                            @endphp
                                                            <select name="products[0][unit]" id="" class="form-control">

                                                                @foreach ($units as $item)
                                                                    <option value="{{ $item }}">
                                                                        {{ \App\CPU\translate($item) }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>


                                                    <td>
                                                        <div class="d-flex">
                                                            <button type="button" class="btn btn-sm btn-success mr-1" id="add">
                                                                <i class="tio-add"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-danger" id="del">
                                                                <i class="tio-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>

                                    <hr>
                                    <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('submit') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection
@push('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src="{{ asset('js/compress.js') }}"></script>
<script>
        $(`[data-toggle="switch-location"]`).each(function() {
            $(this).click(function(e) {
                e.preventDefault();
                var fromVal = $(`[name="from"]`).val();
                var toVal = $(`[name="to"]`).val();
                $(`[name="from"]`).val(toVal).trigger('change');
                $(`[name="to"]`).val(fromVal).trigger('change');
            });
        });




        var Express = {
            init: () => {
                Express.product.init();
            },
            product: {
                $append: $(`table#product tbody`),
                $clone: $(`table#product tbody tr#clone`).clone().removeAttr('style').removeAttr('id'),
                $add: `table#product tbody #add`,
                $del: `table#product tbody #del`,
                datetime: () => {
                    return (new Date()).getTime();
                },
                init: () => {

                    $(document).on('click', `img#trigger-image`, function() {
                        $(this).parent().find('input').trigger('click');
                    });
                    $(document).on('input', 'input#input-image', function(e) {
                        if(e.target.files.length){
                            $(e.target).parent().find('img').attr('src', `{{ asset('assets/front-end/img/loader_.gif') }}`);
                            var compressor = new window.Compress();
                            compressor.compress([...e.target.files], {
                                size: 4
                                , quality: 0.75
                            , }).then((output) => {
                                var v = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                                var formData = new FormData();
                                formData.append('_token', `{{ csrf_token() }}`)
                                formData.append('image', v)
                                 $.ajax({
                                    url: `{{ route('image') }}`
                                    , method: 'POST'
                                    , contentType: false
                                    , processData: false
                                    , data: formData
                                    , success: (res) => {
                                        $(e.target).parent().find('img').attr('src', res.url);
                                            if(res.status){
                                                $(e.target).parent().find('#image').val(res.url);
                                            }else{
                                                $(e.target).parent().find('#image').val('');
                                            }
                                            if(res.message){
                                                toastr.error(res.message);
                                            }
                                    }
                                });
                                $(e.target).val('');
                            });

                        }

                       // $(this).parent().find('img').attr('src', URL.createObjectURL(e.target.files[0]));
                    });

                    $(document).on('click', Express.product.$add, function(e) {
                        e.preventDefault();
                        var index = Express.product.datetime();
                        var clone = Express.product.$clone.prop('outerHTML').replaceAll('__name__', 'name');
                        clone = clone.replaceAll('__index__', index);
                        $(this).parents('tr').after(clone);
                    });
                    $(document).on('click', Express.product.$del, function(e) {
                        e.preventDefault();
                        if (Express.product.$append.find(`tr:not(#clone)`).length > 1) {
                            $(this).parents('tr').remove();
                        }

                    });
                }
            }
        };
        Express.init();
</script>
@endpush
