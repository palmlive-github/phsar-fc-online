@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .navbar-sticky{
            position: relative !important;
        }
    </style>
@endpush

@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('Express Products') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="card box-shadow-sm">
                  
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                      
                        <div class="my-2">
                            <a class="btn btn-sm btn-primary" href="{{ route('express.product.create') }}">{{ \App\CPU\translate('Create') }}</a>
                        </div>
                        @if(count($expressProducts)==0)
                        <div class="text-center p-4">
                            <img class="mb-3" src="{{asset('assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                            <p class="mb-0">{{\App\CPU\translate('no_data_found')}}</p>
                        </div>
                    @else
                            <div class="mb-3">
                                <form action="{{ url()->current() }}" method="GET">
                                    <div class="input-group input-group-merge input-group-flush">
                                        <input id="date" type="text" name="date" class="form-control" placeholder="{{ \App\CPU\translate('Date')}}"
                                        value="{{ request('date') }}" autocomplete="off">
                                      
                                        <button type="submit"
                                            class="btn btn-primary ml-2">{{ \App\CPU\translate('search') }}</button>
                                    </div>
                                </form>
                            </div>
                       
                            <div class="card-body" style="padding: 0">
                               
                                <div class="table-responsive">
                                 
                                    <table style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};"
                                        class="table  border table-nowrap table-align-middle card-table">
                                        <thead class="thead-light">
                                        <tr>
                                            <th style="width: 1">{{ \App\CPU\translate('ID')}}</th>
                                            <th>{{ \App\CPU\translate('name')}}</th>
                                            <th>{{ \App\CPU\translate('length')}}</th>
                                            <th>{{ \App\CPU\translate('height')}}</th>
                                            <th>{{ \App\CPU\translate('width')}}</th>
                                            <th>{{ \App\CPU\translate('weight')}}</th>
                                            <th>{{ \App\CPU\translate('unit')}}</th>
                                            <th>{{ \App\CPU\translate('Price')}}</th>
                                            <th>{{ \App\CPU\translate('Status')}}</th>
                                            <th>{{ \App\CPU\translate('Date')}}</th>
                                            <th>{{ \App\CPU\translate('Action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($expressProducts as $key=>$row)
                                            <tr>
                                                <td class="text-center">{{$key+ 1}}</td>
                                                <td>
                                                    
                                                        <img width="40px" height="40px" class="border"
                                                     onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                     src="{{ $row->image }}" alt="">
                                                    {{$row->name}}
                                                    
                                                </td>
                                                <td>{{$row->length}}</td>
                                                <td>{{$row->height}}</td>
                                                <td>{{$row->width}}</td>
                                                <td>{{$row->weight}}</td>
                                                <td>{{ \App\CPU\translate($row->unit)}}</td>
                                                <td>{{ \App\CPU\Helpers::currency_converter($row->price)}}</td>
                                               
                                                <td>{{ \App\CPU\translate($row->status)}}</td>
                                                <td>  {{ $row->created_at->translatedFormat('d-M-Y') }}</td>
                                                <td>
                                                    @if ($row->status == null)
                                                    <a class="text-primary" href="{{ route('express.product.edit',$row) }}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                   
                                                    <a class="text-primary delete" style="cursor: pointer;"
                                                            id="{{$row['id']}}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    @endif
                                                        
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
        
                            <div class="card-footer">
        
                                {!! $expressProducts->appends(request()->except('page'))->links() !!}
                            </div>
                           
                            @endif
      
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection
@push('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $('input[name="date"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: "YYYY/MM/DD",
            cancelLabel: 'Clear'
        }
    });
    $('input[name="date"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
    });

    $('input[name="date"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $(`button#more`).click(function(e){
        e.preventDefault();
        var target = $(this).data('target');
        if($(target).hasClass(`d-none`)){
            $(target).removeClass(`d-none`);
            $(this).text(`{{ \App\CPU\translate('Hide')}}`);
        }else{
            $(target).addClass(`d-none`);
            $(this).text(`{{ \App\CPU\translate('More')}}`);
        }

    });

    $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            Swal.fire({
                title: '{{\App\CPU\translate('Are_you_sure')}}?',
                text: "{{\App\CPU\translate('You_will_not_be_able_to_revert_this')}}!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{\App\CPU\translate('Yes')}}, {{\App\CPU\translate('delete_it')}}!'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{route('express.product.destroy','')}}/"+id,
                        method: 'DELETE',
                        data: {id: id},
                        success: function () {
                            //toastr.success('{{\App\CPU\translate('Express_deleted_Successfully.')}}');
                            location.reload();
                        }
                    });
                }
            })
        });
   

</script>
@endpush
