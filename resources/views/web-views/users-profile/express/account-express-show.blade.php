@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
<style>
    .parent {
        width: 100px;
        height: 100px;
        display: table;
        border: 1px solid #e9e9e9;
        border-radius: 50%;
    }

    .parent h5 {
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }

    .navbar-sticky {
        position: relative !important;
    }
</style>
@endpush

@section('content')



<!-- Page Title-->
<div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
    <div class="row">

        <div class="col-md-12 sidebar_heading">
            <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                {{ \App\CPU\translate('Express Detail') }}</h1>
        </div>
    </div>
</div>
<!-- Page Content-->
<div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
    style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
    <div class="row">
        <!-- Content  -->
        <section class="col-lg-12">
            <div class="card box-shadow-sm">
                <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                    <table class="table border-0">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-row mb-2 justify-content-between">
                                        <div class="col">
                                            <p class="mb-0">
                                                {{ \App\CPU\translate('Date') }} :
                                                {{ $express->created_at->translatedFormat('d-M-Y') }}
                                            </p>

                                            <p class="mb-0">
                                                {{ \App\CPU\translate('From') }} :
                                                {{ \App\CPU\translate($express->delivery->from) }}
                                                →
                                                {{ \App\CPU\translate($express->delivery->to) }}
                                            </p>
                                            <p class="mb-0">
                                                {{ \App\CPU\translate('Sender') }} :
                                                @if ($express->delivery->sender_id == auth('customer')->id())
                                                <span class="text-primary">{{ \App\CPU\translate('You') }}</span>
                                                @else
                                                {{ $express->delivery->sender_info['name']}}
                                                @endif
                                            </p>
                                            <p class="mb-0">
                                                {{ \App\CPU\translate('Receiver') }} :
                                                @if ($express->delivery->receiver_id == auth('customer')->id())
                                                <span class="text-primary">{{ \App\CPU\translate('You') }}</span>
                                                @else
                                                {{ $express->delivery->receiver_info['name']}}
                                                @endif
                                            </p>
                                            <p class="mb-0">
                                                {{ \App\CPU\translate('Receiver Phone') }} :
                                                {{ $express->delivery->receiver_info['phone']['full']}}
                                            </p>
                                            <p class="mb-0">
                                                {{ \App\CPU\translate('Receiver Address') }} :
                                                <div>
                                                    {{ $express->delivery->receiver_info['address']}}
                                                </div>
                                                <div>
                                                   <img src="{{  $express->delivery->address_image }}" alt="" style="width: 300px;
                                                   height: 200px;
                                                   object-fit: contain;" class="border">
                                                </div>
                                                
                                            </p>


                                            

                                        </div>
                                        <div class="col-auto">
                                            <p class="mb-0">
                                                {{\App\CPU\translate('Price') }} : {{
                                                \App\CPU\Helpers::currency_converter($express->price) }}
                                            </p>
                                            <p class="mb-0">
                                                {{\App\CPU\translate('Status') }} :
                                                {{\App\CPU\translate($express->status) }}
                                            </p>
                                            <p class="mb-0">
                                                {{ \App\CPU\translate('Payment Image') }} :
                                             
                                                <div>
                                                   <img src="{{  $express->payment_image }}" alt="" style="width: 300px;
                                                   height: 300px;
                                                   object-fit: contain;" class="border">
                                                </div>
                                                
                                            </p>
                                        </div>
                                    </div>

                                    <table class="table table-bordered">

                                        <head>
                                          
                                            <tr>
                                                <th width=1>{{ \App\CPU\translate('Id') }}</th>
                                                <th>{{ \App\CPU\translate('name')}}</th>
                                                <th>{{ \App\CPU\translate('length') }}</th>
                                                <th>{{ \App\CPU\translate('height') }}</th>
                                                <th>{{ \App\CPU\translate('width') }}</th>
                                                <th>{{ \App\CPU\translate('weight') }}</th>
                                                <th>{{ \App\CPU\translate('unit') }}</th>
                                                <th>{{ \App\CPU\translate('price') }}</th>
                                            </tr>
                                        </head>
                                        <tbody>

                                            @foreach( $express->products as $j=>$product)
                                            <tr>
                                                <td class="text-center">{{$j+ 1}}</td>
                                                <td class="">
                                                    <div>
                                                        <img class="border" style="width: 50px;height:50px"
                                                            onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                            src="{{ $product->image }}" alt="">
                                                   
                                                        {{$product->name}}
                                                    </div>
                                                </td>
                                                <td> {{ $product->length }}</td>
                                                <td> {{ $product->height }}</td>
                                                <td> {{ $product->width }}</td>
                                                <td> {{ $product->weight }}</td>
                                                <td>{{ \App\CPU\translate($product->unit) }} </td>
                                                <td>{{ \App\CPU\Helpers::currency_converter($product->price) }}</td>
                                              
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>

                    </table>

                </div>
            </div>
        </section>
    </div>
</div>
@endsection
