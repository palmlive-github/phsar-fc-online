@extends('layouts.front-end.app')

@section('title',auth('customer')->user()->f_name.' '.auth('customer')->user()->l_name)


@section('content')
    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{Session::get('direction') === "rtl" ? 'right' : 'left'}} headerTitle">{{\App\CPU\translate('Bank Info')}}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
         style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
        <div class="row">
            <!-- Sidebar-->
        @include('web-views.partials._profile-aside')
        <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="card box-shadow-sm">
                    <div class="card-header">
                        <form class="mt-3" action="{{route('account-bankinfo')}}" method="post"
                              enctype="multipart/form-data">
                            <div class="row photoHeader">
                                @csrf

                                <div class="card-body {{Session::get('direction') === "rtl" ? 'mr-3' : 'ml-3'}}">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="payment_method_id">{{\App\CPU\translate('Payment method')}} </label>
                                            <select name="payment_method_id" id="payment_method_id" class="form-control">
                                                @foreach ($payment_method as $item)
                                                <option {{ $item->id == $customerDetail['payment_method_id']?'selected':null }} value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('payment_method_id')
                                                <div class="text-danger text-sm">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="firstName">{{\App\CPU\translate('account_name')}} </label>

                                            <input type="text" class="form-control" id="holder_name" name="holder_name" autocomplete="off"
                                                   value="{{old('holder_name',$customerDetail['holder_name'] )}}" required>
                                                @error('period')
                                                    <div class="text-danger text-sm">{{ $message }}</div>
                                                @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="firstName">{{\App\CPU\translate('account_number')}} </label>

                                            <input type="number" class="form-control" id="account_no" name="account_no" autocomplete="off"
                                                   value="{{old('account_no',$customerDetail['account_no'] )}}" required>
                                                @error('period')
                                                    <div class="text-danger text-sm">{{ $message }}</div>
                                                @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary float-{{Session::get('direction') === "rtl" ? 'left' : 'right'}}">{{\App\CPU\translate('Update')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@push('script')

<script>
    $('input[name="period"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: "YYYY-MM-DD",
            cancelLabel: 'Clear'
        },

    });
    $('input[name="period"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('input[name="period"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
</script>
@endpush
