@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

    </style>
@endpush

@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('withdrawal') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="card box-shadow-sm">
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <div class="parent">
                            <h5>
                                @php
                                    $balance = $customerDetail->balances()->sum('balance') -  $customerDetail->withdrawals()->sum('balance');
                                @endphp
                                {{ \App\CPU\Helpers::currency_converter($balance) }}
                            </h5>
                        </div>

                       {{-- <form action="" method="post" class="my-2">
                            @csrf
                            <div class="input-group">
                                <input id="amount" name="amount" type="number" max="{{ $balance }}" class="form-control" placeholder="{{ \App\CPU\translate('Amount') }}">
                                <button type="submit" class="btn btn-primary float-{{Session::get('direction') === "rtl" ? 'left' : 'right'}}">{{\App\CPU\translate('Request')}}</button>
                            </div>
                       </form> --}}
                    </div>
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <h4 class="h4 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                            {{ \App\CPU\translate('Transactions') }}</h4>
                        <table class="table table-bordered">
                            <thead>
                                <th width=1>{{ \App\CPU\translate('Id') }}</th>
                                <th>{{ \App\CPU\translate('Date') }}</th>
                                <th>{{ \App\CPU\translate('Balance') }}</th>
                                <th>{{ \App\CPU\translate('Image payment') }}</th>
                            </thead>
                            <tbody>
                                @foreach ($withdrawals as $key => $row)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                            {{ $row->created_at->translatedFormat('d-M-Y') }}
                                        </td>
                                        <td>
                                            {{ \App\CPU\Helpers::currency_converter($row->balance)  }}
                                        </td>
                                        <td>
                                            <a href="{{ $row->image }}"><img class="border" width="100px" height="200px" src="{{ $row->image }}" alt="" style="object-fit: cover"></a>
                                        </td>
                                         {{-- <td>
                                             @if ($row->status == 'requesting')
                                                <form action="{{ route('account-withdrawal-cancel',$row->id) }}" method="POST">
                                                    @csrf
                                                    <button class="btn">
                                                        {{ \App\CPU\translate('Cancel') }}
                                                    </button>
                                                </form>

                                             @endif
                                         </td> --}}
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                        {!! $withdrawals->links() !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@push('script')
<script>
    $(`#amount`).on('input',function(){
        var v = $(this).val();
        var max = parseFloat($(this).attr('max'));
        if(v  > max){
            $(this).val(max);
        }
    });
</script>
@endpush
