@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

    </style>
@endpush

@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('balance') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                <div class="card box-shadow-sm">
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <div class="parent">
                            <h5>
                                @php
                                     $balance = $customerDetail->balances()->sum('balance') -  $customerDetail->withdrawals()->sum('balance');
                                @endphp
                                {{ \App\CPU\Helpers::currency_converter($balance) }}
                            </h5>
                        </div>


                    </div>
                    <div class="card-body {{ Session::get('direction') === 'rtl' ? 'mr-3' : 'ml-3' }}">
                        <h4 class="h4 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                            {{ \App\CPU\translate('Referral History') }}</h4>
                        <table class="table table-bordered">
                            <thead>
                                <th width=1>{{ \App\CPU\translate('Id') }}</th>
                                <th>{{ \App\CPU\translate('Type') }}</th>
                                <th>{{ \App\CPU\translate('Commission') }}</th>
                                <th>{{ \App\CPU\translate('Date') }}</th>
                            </thead>
                            <tbody>
                                @foreach ($history_balance as $key => $row)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                           {{ collect(explode('\\',$row->model))->last() }}
                                           @php
                                               $model = (new $row->model)->find($row->model_id);
                                           @endphp
                                           @if ($model && collect(explode('\\',$row->model))->last() == 'User')
                                                 ({{ $model->f_name }} {{ $model->l_name }} {{ \App\CPU\translate('Become VIP') }})
                                           @elseif($model && collect(explode('\\',$row->model))->last() == 'Express')

                                                 ( {{ \App\CPU\translate($model->delivery->from) }} &rightarrow; {{ \App\CPU\translate($model->delivery->from) }})
                                            @elseif($model && collect(explode('\\',$row->model))->last() == 'Order')

                                                 ( {{ \App\CPU\translate('Purchased products') }} : {{ \App\CPU\translate($model->details->count()) }})
                                           @endif

                                        </td>
                                        <td class="text-green"> {{ \App\CPU\Helpers::currency_converter($row->balance)  }}</td>
                                        <td>{{ $row->created_at->translatedFormat('d-M-Y') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                        {!! $history_balance->links() !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
