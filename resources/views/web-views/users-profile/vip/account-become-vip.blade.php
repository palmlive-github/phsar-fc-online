@extends('layouts.front-end.app')

@section('title', auth('customer')->user()->f_name . ' ' . auth('customer')->user()->l_name)


@push('css_or_js')
    <style>
        .parent {
            width: 100px;
            height: 100px;
            display: table;
            border: 1px solid #e9e9e9;
            border-radius: 50%;
        }

        .parent h5 {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

    </style>
@endpush

@section('content')



    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9 sidebar_heading">
                <h1 class="h3  mb-0 float-{{ Session::get('direction') === 'rtl' ? 'right' : 'left' }} headerTitle">
                    {{ \App\CPU\translate('To Become a VIP') }}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
        style="text-align: {{ Session::get('direction') === 'rtl' ? 'right' : 'left' }};">
        <div class="row">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')
            <!-- Content  -->
            <section class="col-lg-9 col-md-9">
                @if ($customerDetail->is_vip || $customerDetail->vipRequests->count())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card border-0 box-shadow mt-2">
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <th> {{ \App\CPU\translate('Date') }}</th>
                                            <th> {{ \App\CPU\translate('Total Fee') }}</th>
                                            <th> {{ \App\CPU\translate('Status') }}</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($customerDetail->vipRequests as $row)
                                                <tr>
                                                    <td>{{ $row->created_at->translatedFormat('d-m-Y') }}
                                                        @if ($row->status == 'approved')
                                                            -
                                                            {{ $row->updated_at->translatedFormat('d-m-Y') }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="/storage/upload-payment/{{ $row->upload_payment }}" target="_blank">
                                                            <img width="50px"
                                                                src="/storage/upload-payment/{{ $row->upload_payment }}"
                                                                alt="">
                                                        </a>
                                                        {{ \App\CPU\Helpers::currency_converter($row->total_fee) }}
                                                    </td>
                                                    <td>{{ \App\CPU\translate($row->status) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-danger" id="firebase-error" style="display: none"></div>
                        <div class="alert alert-success" id="firebase-success" style="display: none"></div>
                        <div class="card border-0 box-shadow mt-2">
                            <div class="card-body">
                                <form class="needs-validation" action="{{ route('account-become-vip') }}" method="post"
                                    id="sign-up-form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">

                                        <div class="col-sm-8">
                                            <div class="mb-3">
                                                <label
                                                    for="payment_method_id">{{ \App\CPU\translate('Payment Method') }}</label>
                                                <select name="payment_method_id" class="form-control"
                                                    id="payment_method_id" required>
                                                    <option value="">{{ \App\CPU\translate('Please select') }}</option>
                                                    @foreach ($payment_method as $item)
                                                        <option
                                                            {{ $customerDetail->payment_method_id == $item->id ? 'selected' : null }}
                                                            value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="mb-3">
                                                <label
                                                    for="payment_account_name">{{ \App\CPU\translate('Account Name') }}</label>
                                                <input type="text" value="{{ $customerDetail->holder_name }}"
                                                    name="holder_name" id="payment_account_name" class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Account Name') }}" required>
                                            </div>
                                            <div class="mb-3">
                                                <label
                                                    for="payment_account_number">{{ \App\CPU\translate('Account Number') }}</label>
                                                <input type="text" value="{{ $customerDetail->account_no }}"
                                                    name="account_no" id="payment_account_number" class="form-control"
                                                    placeholder="{{ \App\CPU\translate('Account Number') }}" required>
                                            </div>
                                            {{-- <div class="form-group mb-3">
                                                <div class="custom-file" style="text-align: left">
                                                    <input type="file" name="attach_file" id="attach_file"
                                                        class="custom-file-input" style="overflow: hidden; padding: 2%">
                                                    <label class="custom-file-label"
                                                        for="attach_file">{{ \App\CPU\translate('Upload') }}
                                                        {{ \App\CPU\translate('Attach file') }}</label>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="col-sm-4 mb-3 mb-sm-0">
                                            <div class="pb-2 text-center">
                                                <img class="w-100"
                                                    style="width: auto;border: 1px solid; border-radius: 10px;"
                                                    id="viewerNationIDCard"
                                                    src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                    alt="Nation ID Card" />
                                            </div>

                                            <div class="form-group mt-2">
                                                <div class="custom-file" style="text-align: left">
                                                    <input type="file" name="id_card_image" id="uploadNationIDCard"
                                                        data-target="#viewerNationIDCard" class="custom-file-input" required
                                                        accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    <label class="custom-file-label"
                                                        for="uploadNationIDCard">{{ \App\CPU\translate('Upload') }}
                                                        {{ \App\CPU\translate('Nation ID card') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-8 mb-3 mb-sm-0">
                                            <div>
                                                <h4>{{ \App\CPU\translate('Register fee') }}</h4>
                                            </div>
                                            <div class="mb-3">{{ \App\CPU\translate('Total Price') }}:
                                                {{ \App\CPU\Helpers::currency_converter($vip->fee) }}</div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    @foreach ($payment_method as $k => $payment)
                                                        <tr>
                                                            <td width="100px">
                                                                <div class="custom-control custom-radio">
                                                                    <input class="custom-control-input" type="radio"
                                                                        {{ $loop->first ? 'checked' : null }} name="payment"
                                                                        value="{{ $payment->id }}"
                                                                        id="k-{{ $k }}">
                                                                    <label for="k-{{ $k }}"
                                                                        class="custom-control-label">
                                                                        <img width="100%"
                                                                            src="{{ $payment['image'] }}">
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <label for="k-{{ $k }}">
                                                                    {{ $payment['name'] }}
                                                                    <br>
                                                                    {{ $payment['account_name'] }}
                                                                    <br>
                                                                    {{ $payment['account_number'] }}
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-4 mb-3 mb-sm-0">
                                            <div>
                                                <h5>{{ \App\CPU\translate('Pay Slip') }}</h5>
                                            </div>

                                            <div class="pb-2 text-center">
                                                <img class="w-100"
                                                    style="width: auto;border: 1px solid; border-radius: 10px;"
                                                    id="viewerPayment"
                                                    src="{{ asset('assets\back-end\img\400x400\img2.jpg') }}"
                                                    alt="Image Payment" />
                                            </div>

                                            <div class="form-group mt-2">
                                                <div class="custom-file" style="text-align: left">
                                                    <input type="file" name="upload_payment" id="uploadPayment"
                                                        data-target="#viewerPayment" class="custom-file-input" required
                                                        accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                                    <label class="custom-file-label"
                                                        for="uploadPayment">{{ \App\CPU\translate('Upload') }}
                                                        {{ \App\CPU\translate('Image Payment') }}</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group d-flex flex-wrap justify-content-between">

                                        <div class="form-group mb-1">
                                            <strong>
                                                <input type="checkbox" class="mr-1" name="remember" id="agree">
                                            </strong>
                                            <label for="agree">{{ \App\CPU\translate('i_agree_to_Your_terms') }}<a
                                                    class="font-size-sm" target="_blank" href="{{ route('terms') }}">
                                                    <span
                                                        class="text-primary">{{ \App\CPU\translate('terms_and_condition') }}</span>
                                                </a></label>
                                        </div>

                                    </div>
                                    <div class="flex-between row" style="direction: {{ Session::get('direction') }}">
                                        <div class="mx-1">
                                            <div class="text-right">
                                                <button class="btn btn-primary" id="sign-up" type="submit" disabled>
                                                    <i
                                                        class="czi-user {{ Session::get('direction') === 'rtl' ? 'ml-2 mr-n1' : 'mr-2 ml-n1' }}"></i>
                                                    {{ \App\CPU\translate('Request') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </section>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#agree').change(function() {
            // console.log('jell');
            if ($(this).is(':checked')) {
                $('#sign-up').removeAttr('disabled');
            } else {
                $('#sign-up').attr('disabled', 'disabled');
            }
        });

        $("#uploadNationIDCard,#uploadPayment").change(function() {
            var target = $(this).data(`target`);
            if (this.files.length) {
                $(target).attr(`src`, URL.createObjectURL(this.files[0]));
            }
        });
    </script>
@endpush
