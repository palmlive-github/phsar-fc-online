@extends('layouts.front-end.app')

@section('title','')

@push('css_or_js')
    <style>
        .headerTitle {
            font-size: 24px;
            font-weight: 600;
            margin-top: 1rem;
        }

        .sidebar {
            max-width: 20rem;
        }

        .custom-control-label {
            cursor: pointer;
        }

        .btnF {
            cursor: pointer;
        }

        .widget-categories .accordion-heading > a:hover {
            color: #FFD5A4 !important;
        }

        .widget-categories .accordion-heading > a {
            color: #FFD5A4;
        }

        .list-link:hover {
            color: #030303 !important;
        }

        .border:hover {
            border: 3px solid #4884ea;
            margin-bottom: 5px;
            margin-top: -6px;
        }

        body {
            font-family: 'Titillium Web', sans-serif
        }

        .card {
            border: none
        }

        .totals tr td {
            font-size: 13px
        }


        .product-qty span {
            font-size: 14px;
            color: #6A6A6A;
        }

        .spanTr {
            color: #FFFFFF;
            font-weight: 600;
            font-size: 13px;

        }

        .spandHead {
            color: #FFFFFF;
            font-weight: 600;
            font-size: 20px;
            margin-left: 25px;
        }

        .spandHeadO {
            color: #FFFFFF;
            font-weight: 600;
            font-size: 14px;

        }

        .font-name {
            font-weight: 600;
            font-size: 12px;
            color: #030303;
        }

        .amount {
            font-size: 15px;
            color: #030303;
            font-weight: 600;


        }

        .tdBorder {
            border-right: 1px solid #f7f0f0;
            text-align: center;
        }

        .bodytr {
            border: 1px solid #dadada;
        }

        .sellerName {
            font-size: 15px;
            font-weight: 600;
        }

        /* a{
            color: #030303;
            cursor: pointer;
        }
        a:hover{
            color: #4884ea;
            cursor: pointer;
        } */
        .divider-role {
            border-bottom: 1px solid whitesmoke;
        }

        .sidebarL h3:hover + .divider-role {
            border-bottom: 3px solid #1b7fed !important;
            transition: .2s ease-in-out;
        }

        /*//for -message*/
        .container {
            max-width: 1170px;
            margin: auto;
        }

        img {
            max-width: 100%;
        }

        .inbox_people {
            background: #ffffff none repeat scroll 0 0;
            float: left;
            overflow: hidden;
            /*width:;*/
            border-right: 1px;
        }

        .inbox_msg {
            border: none;
            clear: both;
            overflow: hidden;
        }

        .top_spac {
            margin: 20px 0 0;
        }


        .recent_heading {
            float: left;
            width: 40%;
        }

        .srch_bar {
            display: inline-block;
            color: #92C6FF;
            width: 100%;
            /*padding:;*/
        }

        input {
            border: none;

        }

        .headind_srch {
            padding: {{Session::get('direction') === "rtl" ? '10px 20px 10px 29px' : '10px 29px 10px 20px'}};
            overflow: hidden;
            border-bottom: none;
        }

        .recent_heading h4 {
            color: #05728f;
            font-size: 21px;
            margin: auto;
        }

        /* .form-control {

            border: none;
            box-shadow: none;
        } */

        .chat_ib h5 {
            font-size: 15px;
            font-size: 13px;
            color: #030303;
            cursor: pointer;
            margin: 0 0 8px 0;
        }

        .chat_ib h5 span {
            font-size: 80%;
            float: right;
            padding: 10px;
            background: #4884ea;
            color: white;
            border-radius: 100%;
        }

        .chat_ib p {
            font-size: 14px;
            color: #989898;
            margin: auto;
        }

        .chat_img {
            float: left;
            width: 12%;
            cursor: pointer;
        }

        .chat_ib {
            float: left;
            padding: {{Session::get('direction') === "rtl" ? '0 15px 6px 0' : '0 0 6px 6px'}};
            width: 88%;
            margin-top: 0.56rem;
        }

        .chat_people {
            overflow: hidden;
            clear: both;
        }

        .chat_list {
            border-bottom: none;
            margin: 0;
            padding: 18px 16px 10px;
        }

        .inbox_chat {
            height: 510px;
            overflow-y: auto;
        }

        .active_chat {
            background: #ebebeb;
        }

        .received_msg {
            display: inline-block;
            padding: {{Session::get('direction') === "rtl" ? '0 10px 0 0' : '0 0 0 10px'}};
            vertical-align: top;
            width: 92%;
        }

        .received_withd_msg p {
            background: #E2F0FF none repeat scroll 0 0;
            border-radius: 10px;
            color: #030303;
            font-size: 14px;
            margin: 0;
            padding: {{Session::get('direction') === "rtl" ? '4px 10px 3px 8px' : '4px 8px 3px 10px'}};
            width: fit-content;
            max-width: 100%;
            word-break: break-word;

        }
        .send_msg  .time_date{
            float: right;
        }
        .time_date {
            color: #747474;
            display: block;
            font-size: 12px;
            margin: 8px 0 0;
        }

        .received_withd_msg {
            width: 80%;
        }

        .send_msg p {
            background: #4884ea none repeat scroll 0 0;
            border-radius: 8px;
            font-size: 14px;
            margin: 0;
            color: #fff;
            padding: {{Session::get('direction') === "rtl" ? '5px 12px 5px 10px' : '5px 10px 5px 12px'}};
            width: 100%;
        }

        .outgoing_msg {
            overflow: hidden;
            margin: 26px 0 26px;
        }

        .send_msg {
            float: right;
            width: fit-content;
            max-width: 80%;
            margin-{{Session::get('direction') === "rtl" ? 'left' : 'right'}}: 10px;
        }

        .input_msg_write input {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
            color: #4c4c4c;
            font-size: 15px;
            min-height: 48px;
            width: 100%;
        }

        .type_msg {
            border-top: 1px solid #c4c4c4;
            position: relative;
        }

        .msg_send_btn {
            background: none repeat scroll 0 0;
            border: medium none;
            border-radius: 50%;
            color: #4884ea;
            cursor: pointer;
            font-size: 17px;
            height: 33px;
            position: absolute;
            right: 0;
            top: 11px;
            width: 33px;
        }

        .msg_history {
            height: 516px;
            padding: {{Session::get('direction') === "rtl" ? '30px 25px 0 15px' : '30px 15px 0 25px'}};
            overflow-y: auto;
        }

        .aSend {
            padding: 10px;
            color: #4884ea;
            font-size: 16px;
            font-weight: 600;
        }

        .price_sidebar {
            padding: 20px;
        }


        .active {
            background: #1B7FED;
        }

        .active h5 {
            color: white;
        }

        .incoming_msg {
            display: flex;
        }

        .incoming_msg_img img {
            border-radius: 50%;
            background-image: url(/assets/front-end/img/image-place-holder.png);
            width: 40px;
            height: 40px;
            background-size: contain;
        }

        .chat_ib h5.active-text {
            color: #fff!important;
            font-weight: 900 !important;
        }

        @media (max-width: 600px) {
            .sidebar_heading {
                background: #1B7FED;
            }

            .sidebar_heading h1 {
                text-align: center;
                color: aliceblue;
                padding-bottom: 17px;
                font-size: 19px;
            }

            .Chat {
                margin-top: 2rem;
            }

            .sidebarR {
                padding: 24px;
            }

            .price_sidebar {
                padding: 20px;
            }

            .mr-3 {
                /*margin-right: none !important;*/
            }

            .send_msg {
                margin- {{Session::get('direction') === "rtl" ? 'left' : 'right'}}: 7px;
            }
        }

        /* @media(max-width:992px)
        {
            .price_sidebar {
            padding: 20px;
            max-width: 230px;
        }
        .chatSel{
            max-width: 262px;
            display: flex;
            margin-top: 1rem;
        }
        } */

    </style>

@endpush

@section('content')

    <!-- Page Title-->
    <div class="container rtl" style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="sidebar_heading col-md-9">
                <h1 class="h3  mb-0 folot-left headerTitle">{{\App\CPU\translate('chat_with_seller')}}</h1>
            </div>
        </div>
    </div>

    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4 mt-3 rtl"
         style="text-align: {{Session::get('direction') === "rtl" ? 'right' : 'left'}};">
        <div class="row mt-3">
            <!-- Sidebar-->
            @include('web-views.partials._profile-aside')

            {{-- Seller List start --}}
            @if ($last_chat)


            @if (isset($unique_shops))

                <div class="col-lg-3 chatSel">
                    <div class="card box-shadow-sm">
                        <div class="inbox_people">
                            <div class="headind_srch">
                                <form
                                    class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
                                    <input
                                        class="form-control form-control-sm {{Session::get('direction') === "rtl" ? 'ml-3' : 'mr-3'}} w-75"
                                        id="myInput" type="text"
                                        placeholder="{{\App\CPU\translate('Search')}}"
                                        aria-label="Search">
                                    <i class="fa fa-search" style="color: #92C6FF" aria-hidden="true"></i>
                                </form>
                                <hr>
                            </div>
                            <div class="inbox_chat">
                                @if (isset($unique_shops))
                                    @foreach($unique_shops as $key=>$shop)
                                        <div class="chat_list @if ($key == 0) btn-primary @endif"
                                             id="user_{{$shop->shop_id}}">
                                            <div class="chat_people" id="chat_people">
                                                <div class="chat_img">
                                                    <img
                                                        onerror="this.src='{{asset('assets/front-end/img/image-place-holder.png')}}'"
                                                        src="{{asset('storage/shop/'.$shop->image)}}"
                                                        style="border-radius: 10px">
                                                </div>
                                                <div class="chat_ib">
                                                    <h5 class="seller @if($shop->shop_id == ($last_chat?$last_chat->shop_id:null))active-text @endif"
                                                        id="{{$shop->shop_id}}">{{$shop->name}}</h5>
                                                        @if ($shop->unseen)
                                                            <span class="text-danger unseen" data-unseen="{{ $shop->unseen }}"> ({{ $shop->unseen }})</span>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endForeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <section class="col-lg-6">
                    <div class="card box-shadow-sm Chat">
                        <div class="messaging">
                            <div class="inbox_msg">

                                <div class="mesgs">
                                    <div class="msg_history" id="show_msg" data-id="{{ $last_chat ? $last_chat->shop_id : null }}">
                                        @if (isset($chattings))

                                            @foreach($chattings as $key => $chat)
                                                @if ($chat->sent_by_seller)
                                                    <div class="incoming_msg">
                                                        <div class="incoming_msg_img"><img
                                                                src="@if($chat->image == 'def.png'){{asset('storage/'.$chat->image)}} @else {{asset('storage/shop/'.$chat->image)}} @endif"
                                                                alt="sunil"></div>
                                                        <div class="received_msg">
                                                            <div class="received_withd_msg">
                                                                <p>
                                                                    {{$chat->message}}
                                                                </p>
                                                                <span class="time_date" data-toggle="time-ago" data-date="{{ $chat->created_at}}">{{App\CPU\Helpers::timeAgo( $chat->created_at) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                @else

                                                    <div class="outgoing_msg">
                                                        <div class="send_msg">
                                                            <p class="btn-primary">
                                                                {{$chat->message}}
                                                            </p>
                                                            <span class="time_date" data-toggle="time-ago" data-date="{{ $chat->created_at}}">{{App\CPU\Helpers::timeAgo( $chat->created_at) }}</span>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endForeach
                                            {{-- for scroll down --}}
                                            <div id="down"></div>

                                        @endif
                                    </div>
                                    <div class="type_msg">
                                        <div class="input_msg_write">
                                            <form
                                                class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 my-2"
                                                id="myForm">
                                                @csrf

                                                <input type="text" id="hidden_value" hidden
                                                       value="{{$last_chat->shop_id}}" name="">
                                                <input type="text" id="seller_value" hidden
                                                       value="{{$last_chat->shop->seller_id}}" name="">

                                                <textarea
                                                    class="form-control form-control-sm {{Session::get('direction') === "rtl" ? 'ml-3' : 'mr-3'}} w-75"
                                                    id="msgInputValue"
                                                    type="text" placeholder="{{\App\CPU\translate('Send a message')}}" aria-label="Search"></textarea>
                                                <input class="aSend" type="submit" id="msgSendBtn" style="width: 45px;"
                                                       value="Send">
                                                {{-- <a class="aSend" id="msgSendBtn">Send</a> --}}
                                                {{-- <i class="fa fa-send" style="color: #92C6FF" aria-hidden="true"></i> --}}

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @else
                <div class="col-md-8" style="display: flex; justify-content: center;align-items: center;">
                    <p>{{\App\CPU\translate('No conversation found')}}</p>
                </div>
            @endif
            @endif
        </div>
    </div>

@endsection

@push('script')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
        var pusher = new Pusher(`{{ env('PUSHER_APP_KEY') }}`, {
            cluster: `{{ env('PUSHER_APP_CLUSTER') }}`
        });
        var channelName = `chat-channel`;
        var channel = pusher.subscribe(channelName);
            channel.bind('chat-event', function(data) {
                var chat = data.message;
                var t = ` <div class="incoming_msg">
                    <div class="incoming_msg_img">
                        <img src="${chat.image}">
                    </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                <p>
                                ${chat.message}
                                </p>
                                <span class="time_date" data-toggle="time-ago" data-date="${chat.created_at}"> now </span>
                            </div>
                        </div>
                    </div>`;
                if(chat.sent_by_seller == 1 && $(".msg_history").attr('data-id') == chat.shop_id && chat.user_id == `{{ auth('customer')->id() }}`){
                    $(".msg_history").append(t);
                }
                if(chat.sent_by_customer == 1 && $(".msg_history").attr('data-id') == chat.shop_id && chat.user_id == `{{ auth('customer')->id() }}`){

                    $(".msg_history").append(`
                        <div class="outgoing_msg" id="outgoing_msg">
                        <div class='send_msg'>
                            <p class="btn-primary">${chat.message}</p>
                            <span class='time_date' data-toggle="time-ago" data-date="${new Date}">${time(new Date)}</span>
                        </div>
                        </div>`
                        );
                }

                if(chat.sent_by_seller && chat.user_id == `{{ auth('customer')->id() }}`){
                    var unseen = $(`.seller[id="${chat.shop_id}"]`).parent().find(`.unseen`).data('unseen') ?? 0;
                    $(`.seller[id="${chat.shop_id}"]`).parent().find(`.unseen`).remove();
                    $(`.seller[id="${chat.shop_id}"]`).parent().append(`<span class="text-danger unseen" data-unseen="${unseen+ 1}"> (${unseen + 1})</span>`);

                }

                $('img').each(function(i,img){
                    img.addEventListener("error", function (event) {
                        event.target.src = '{{asset('assets/front-end/img/image-place-holder.png')}}';
                        event.onerror = null
                    })
                });
                timeAgo();
                $(".msg_history").stop().animate({scrollTop: $(".msg_history")[0].scrollHeight}, 1000);
            });

            channel.bind('chat-seen-event', function(data) {
                var chat = data.message;
                if(chat.seen_by_customer == 1 && chat.user_id == `{{ auth('customer')->id() }}`){
                    $(`.seller[id="${chat.shop_id}"]`).parent().find(`.unseen`).remove();
                }

            });
    </script>

    <script>
        $(document).ready(function () {
            var shop_id;
            $(".msg_history").scrollTop($(".msg_history")[0].scrollHeight);
            // var perams_url = window.location.search.substring(1);
            // var perams_url_split = perams_url.split('&');

            $(".seller").click(function (e) {
                e.stopPropagation();
                shop_id = e.target.id;
                //active when click on seller
                $('.chat_list.btn-primary').removeClass('btn-primary');
                $(`#user_${shop_id}`).addClass("btn-primary");
                $('.seller').css('color', 'black');
                $(`#user_${shop_id} h5`).css('color', 'white');
                $('.inbox_chat').find('h5.active-text').removeClass("active-text");
                $.ajax({
                    type: "get",
                    url: "messages?shop_id=" + shop_id,
                    success: function (res) {
                        $(".msg_history").attr('data-id',shop_id).html('');
                        $(".msg_history").attr('data-page',1);
                        $('.chat_ib').find('#' + shop_id).removeClass('active-text');

                        if (res && res.data.length > 0) {

                            $.each(res.data, (index,element) => {
                                if (element.sent_by_customer) {

                                    $(".msg_history").append(`
                                    <div class="outgoing_msg">
                                    <div class='send_msg'>
                                        <p  class="btn-primary">${element.message}</p>
                                        <span class='time_date' data-toggle="time-ago" data-date="${element.created_at}">${time(element.created_at)}</span>
                                    </div>
                                    </div>`);

                                } else {
                                    let img_path = element.image == 'def.png' ? `${window.location.origin}/storage/${element.image}` : `${window.location.origin}/storage/shop/${element.image}`;

                                    $(".msg_history").append(`
                                    <div class="incoming_msg" style="display: flex;" id="incoming_msg">
                                    <div class="incoming_msg_img" id="">
                                        <img src="${img_path}" alt="">
                                    </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                        <p id="receive_msg">${element.message}</p>
                                        <span class="time_date" data-toggle="time-ago" data-date="${element.created_at}">${time(element.created_at)}</span></div>
                                    </div>
                                    </div>`);
                                }
                                $('#hidden_value').attr("value", shop_id);
                            });
                        } else {
                            $(".msg_history").html(`<p> No Message available </p>`);
                            data = [];
                        }
                        $('img').each(function(i,img){
                            img.addEventListener("error", function (event) {
                                event.target.src = '{{asset('assets/front-end/img/image-place-holder.png')}}';
                                event.onerror = null
                            })
                        });
                        timeAgo();
                        $(".msg_history").scrollTop($(".msg_history")[0].scrollHeight);
                    }
                });

                $('.type_msg').css('display', 'block');


            });
            var ajax = null;
            $(".msg_history").scroll(function() {
                var pos = $(this).scrollTop();
                var shop_id = $(this).attr('data-id');
                var page = parseInt(`${$(this).attr('data-page')??1}`);
                if (pos == 0) {
                    if(ajax){
                        ajax.abort();
                    }
                    if($(this).find(`.no-more`).length){
                        return;
                    }
                    ajax = $.ajax({
                    type: "get",
                    url: "oldmessages?shop_id=" + shop_id+"&page="+(eval(page + 1)),
                    success: function (res) {
                        if (res && res.data.length > 0) {
                            $(".msg_history").scrollTop(1);
                            $(".msg_history").attr('data-page',res.page);
                            $(".msg_history").prepend(`<div class="border my-3"></div>`);
                            $.each(res.data, (index,element) => {
                                var $t =$(`
                                    <div class="outgoing_msg">
                                    <div class='send_msg'>
                                        <p  class="btn-primary">${element.message}</p>
                                        <span class='time_date' data-toggle="time-ago" data-date="${element.created_at}">${time(element.created_at)}</span>
                                    </div>
                                    </div>`);
                                if (element.sent_by_customer) {
                                    $(".msg_history").prepend($t);

                                } else {
                                    let img_path = element.image == 'def.png' ? `${window.location.origin}/storage/${element.image}` : `${window.location.origin}/storage/shop/${element.image}`;
                                    $t = $(`
                                        <div class="incoming_msg" style="display: flex;" id="incoming_msg">
                                        <div class="incoming_msg_img" id="">
                                            <img src="${img_path}" alt="">
                                        </div>
                                        <div class="received_msg">
                                            <div class="received_withd_msg">
                                            <p id="receive_msg">${element.message}</p>
                                            <span class="time_date" data-toggle="time-ago" data-date="${element.created_at}">${time(element.created_at)}</span></div>
                                        </div>
                                    </div>`);
                                    $(".msg_history").prepend($t);
                                }
                            });

                        }else{
                            $(".msg_history").prepend(`<h5 class="text-center no-more">No more messages</h5>`);
                        }
                        timeAgo();

                        $('img').each(function(i,img){
                            img.addEventListener("error", function (event) {
                                event.target.src = '{{asset('assets/front-end/img/image-place-holder.png')}}';
                                event.onerror = null
                            })
                        });
                    }
                });
                }
            });

            $("#myInput").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $(".chat_list").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#msgSendBtn").click(function (e) {
                e.preventDefault();
                // get all the inputs into an array.
                var inputs = $('#myForm').find('#msgInputValue').val();
                var new_shop_id = $('#myForm').find('#hidden_value').val();
                var new_seller_id = $('#myForm').find('#seller_value').val();
                if(!inputs.trim()){
                    toastr.error('Type something!');
                    return;
                }

                let data = {
                    message: inputs,
                    shop_id: new_shop_id,
                    seller_id: new_seller_id
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{route('messages_store')}}',
                    data: data,
                    success: function (respons) {
                        $(".msg_history").stop().animate({scrollTop: $(".msg_history")[0].scrollHeight}, 1000);
                        timeAgo();
                    }
                });
                $('#myForm').find('#msgInputValue').val('');

            });
        });
        timeAgo();
        function timeAgo(){
            $(`[data-toggle="time-ago"]`).each(function () {
                var t = $(this).data('date');
                var x = setInterval(() => {
                    $(this).text(time(t));
                }, 10000);
            });
        }

        function time(time){
            switch (typeof time) {
            case 'number':
                break;
            case 'string':
                time = +new Date(time);
                break;
            case 'object':
                if (time.constructor === Date) time = time.getTime();
                break;
            default:
                time = +new Date();
            }
            var time_formats = [
            [60, 'seconds', 1], // 60
            [120, '1 minute ago', '1 minute from now'], // 60*2
            [3600, 'minutes', 60], // 60*60, 60
            [7200, '1 hour ago', '1 hour from now'], // 60*60*2
            [86400, 'hours', 3600], // 60*60*24, 60*60
            [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
            [604800, 'days', 86400], // 60*60*24*7, 60*60*24
            [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
            [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
            [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
            [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
            [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
            [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
            [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
            [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
            ];
            var seconds = (+new Date() - time) / 1000,
            token = 'ago',
            list_choice = 1;

            if (seconds == 0) {
            return 'Just now'
            }
            if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = 'from now';
            list_choice = 2;
            }
            var i = 0,
            format;
            while (format = time_formats[i++])
            if (seconds < format[0]) {
                if (typeof format[2] == 'string')
                return format[list_choice];
                else
                return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
            }
            return time;
        }

    </script>

@endpush

