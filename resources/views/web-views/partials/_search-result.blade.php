<style>
    .list-group-item li, a {
        color: {{$web_config['primary_color']}};
    }

    .list-group-item li, a:hover {
        color: {{$web_config['secondary_color']}};
    }
</style>
<ul class="list-group list-group-flush">
    @foreach($products as $i)

        <li class="list-group-item" onclick="$('.search_form').submit()">
            <a href="javascript:" onmouseover="$('.search-bar-input-mobile').val('{{$i['name']}}');$('.search-bar-input').val('{{$i['name']}}');">
                <div class="d-flex">
                    <div>
                        <img width="50px" src="/storage/product/thumbnail/{{$i['thumbnail']}}" alt="{{$i['name']}}">
                    </div>
                    <div class="ml-2">
                        <div>{{$i['name']}}</div>
                        <div>{{\App\CPU\Helpers::currency_converter($i['unit_price'])}}</div>
                        </div>
                </div>
            </a>
        </li>
    @endforeach
</ul>
