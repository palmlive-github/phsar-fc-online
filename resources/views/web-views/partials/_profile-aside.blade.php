<style>
    body {
        font-family: 'Titillium Web', sans-serif
    }

    .footer span {
        font-size: 12px
    }

    .product-qty span {
        font-size: 12px;
        color: #6A6A6A;
    }

    label {
        font-size: 16px;
    }

    .divider-role {
        border-bottom: 1px solid whitesmoke;
    }

    .sidebarL h3:hover + .divider-role {
        border-bottom: 3px solid {{$web_config['secondary_color']}}    !important;
        transition: .2s ease-in-out;
    }

    .price_sidebar {
        padding: 20px;
    }

    @media (max-width: 600px) {

        .sidebar_heading h1 {
            text-align: center;
            color: aliceblue;
            padding-bottom: 17px;
            font-size: 19px;
        }

        .sidebarR {
            padding: 24px;
        }

        .price_sidebar {
            padding: 20px;
        }
    }

</style>

<div class="sidebarR col-lg-3 col-md-3">
    <!--Price Sidebar-->
    <div class="price_sidebar rounded-lg box-shadow-sm" id="shop-sidebar" style="margin-bottom: -10px;background: white">
        <div class="box-shadow-sm">

        </div>
        <div class="pb-0">
            <!-- Filter by price-->
            <div class=" sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('user-account*')?'active-menu':''}}" href="{{route('user-account')}}">
                        {{\App\CPU\translate('profile_info')}}
                    </a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>

        <div class="pb-0">
            <!-- Filter by price-->
            <div class=" sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-address*')?'active-menu':''}}"
                       href="{{ route('account-address') }}">{{\App\CPU\translate('address')}} </a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0" style="padding-top: 12px;">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-refcode*') ? 'active-menu' :''}}" href="{{route('account-refcode') }} ">{{\App\CPU\translate('Referral Code')}}</a>
                </h3>
                <div class="divider-role"
                    style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0" style="padding-top: 12px;">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-bankinfo*') ? 'active-menu' :''}}" href="{{route('account-bankinfo') }} ">{{\App\CPU\translate('Bank Info')}}</a>
                </h3>
                <div class="divider-role"
                    style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        @if (auth('customer')->user()->is_vip)

        <div class="pb-0" style="padding-top: 12px;">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-balance*') ? 'active-menu' :''}}" href="{{route('account-balance') }} ">{{\App\CPU\translate('Balance')}}</a>
                </h3>
                <div class="divider-role"
                    style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0" style="padding-top: 12px;">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-withdrawal*') ? 'active-menu' :''}}" href="{{route('account-withdrawal') }} ">{{\App\CPU\translate('Withdrawal')}}</a>
                </h3>
                <div class="divider-role"
                    style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>

            {{-- <div class="pb-0" style="padding-top: 12px;">
                <!-- Filter by price-->
                <div class="sidebarL">
                    <h3 class="widget-title btnF" style="font-weight: 700;">
                        <a class="{{Request::is('account-period*') ? 'active-menu' :''}}" href="{{route('account-period') }} ">{{\App\CPU\translate('Referral Period')}}</a>
                    </h3>
                    <div class="divider-role"
                        style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                    </div>
                </div>
            </div> --}}

        @else
            <div class="pb-0" style="padding-top: 12px;">
                <div class="sidebarL">
                    <h3 class="widget-title btnF" style="font-weight: 700;">
                        <a class="{{Request::is('account-become-vip*') ? 'active-menu' :''}}" href="{{route('account-become-vip') }} ">{{\App\CPU\translate('Become a VIP')}}</a>
                    </h3>
                    <div class="divider-role"
                        style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                    </div>
                </div>
            </div>
        @endif

        <div class="pb-0" style="padding-top: 12px;">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-oder*') || Request::is('account-order-details*') ? 'active-menu' :''}}" href="{{route('account-oder') }} ">{{\App\CPU\translate('my_order')}}</a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('track-order*')?'active-menu':''}}" href="{{route('track-order.index') }} ">{{\App\CPU\translate('track_your_order')}}</a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF " style="font-weight: 700;">
                    <a class="{{Request::is('wishlists*')?'active-menu':''}}" href="{{route('wishlists')}}"> {{\App\CPU\translate('wish_list')}}  </a></h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>

        {{--to do--}}
        <div class="pb-0">
            <!-- Filter by price-->
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('chat*')?'active-menu':''}}" href="{{route('chat-with-seller')}}">{{\App\CPU\translate('chat_with_seller')}}</a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        @if (auth('customer')->user()->is_vip)
        <div class="pb-0">
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('express/delivery')?'active-menu':''}}" href="{{route('express.delivery.index')}}">
                        {{\App\CPU\translate('Express delivery')}}
                        @if ($notify = auth('customer')->user()->notify()->where('model','App\Model\Express')->where('seen',0)->count())
                        <div class="navbar-tool" style="display: inline-flex;">
                            <span class="navbar-tool-label" style="position: initial;">
                            {{ $notify }}
                            </span>
                        </div>
                        @endif

                    </a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0">
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('express/delivery/create/khmer*')?'active-menu':''}}" href="{{route('express.delivery.create.khmer-thai')}}">
                        {{\App\CPU\translate('Express') }} ({{\App\CPU\translate('khmer') }} &rightarrow; {{\App\CPU\translate('thai') }})

                    </a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        <div class="pb-0">
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('express/delivery/create/thai*')?'active-menu':''}}" href="{{route('express.delivery.create.thai-khmer')}}">
                        {{\App\CPU\translate('Express') }} ({{\App\CPU\translate('thai') }} &rightarrow; {{\App\CPU\translate('khmer') }})
                        @if ($notify = auth('customer')->user()->notify()->where('model','App\Model\ExpressProduct')->where('seen',0)->count())
                        <div class="navbar-tool" style="display: inline-flex;">
                            <span class="navbar-tool-label" style="position: initial;">
                            {{ $notify }}
                            </span>
                        </div>
                        @endif
                    </a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>
        @endif

        <div class="pb-0">
            <div class="sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="" href="{{$web_config['telegram']}}">{{\App\CPU\translate('Contact on Telegram')}}</a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div>



        {{-- <div class="pb-0">
            <!-- Filter by price-->
            <div class=" sidebarL">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{(Request::is('account-ticket*') || Request::is('support-ticket*'))?'active-menu':''}}"
                       href="{{ route('account-tickets') }}">{{\App\CPU\translate('support_ticket')}}</a></h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;">
                </div>
            </div>
        </div> --}}
        {{--<div class="pb-0" style="padding-top: 12px;">
            <!-- Filter by price-->
            <div class="sidebarL ">
                <h3 class="widget-title btnF" style="font-weight: 700;">
                    <a class="{{Request::is('account-transaction*')?'active-menu':''}}"
                       href="{{ route('account-transaction') }}">
                       {{\App\CPU\translate('tansction_history')}}
                    </a>
                </h3>
                <div class="divider-role"
                     style="border: 1px solid whitesmoke; margin-bottom: 14px;  margin-top: -6px;"></div>
            </div>
        </div>--}}
    </div>
</div>


















