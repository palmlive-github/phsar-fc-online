<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'email'    => 'อีเมลไม่ถูกต้อง',
    'status_verify' => 'บัญชีของคุณยังไม่เปิดใช้งาน โปรดระบุที่อยู่อีเมลของคุณ',
    'status_disabled' => 'บัญชีของคุณถูกปิด',
    'failed'   => 'ข้อมูลที่ใช้ในการยืนยันตัวตนไม่ถูกต้อง',
    'password' => 'รหัสผ่านที่ให้มาไม่ถูกต้อง',
    'throttle' => 'คุณได้พยายามเข้าระบบหลายครั้งเกินไป กรุณาลองใหม่ใน :seconds วินาทีข้างหน้า.',
];
